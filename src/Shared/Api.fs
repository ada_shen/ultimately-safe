module UltimatelySafe.Shared.Api

type Api = {
    joinRoom : string -> Async<Result<int, string>>
    leaveRoom : string -> Async<Result<int, string>>
}

let routeBuilder (_typeName : string) (methodName : string) = $"/api/{methodName}"
