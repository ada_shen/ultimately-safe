module UltimatelySafe.Shared.Utils.Options

/// Convert a (success, result) pair from a "tryX"-style function to an Option
let inline optionOfTryPair (success : bool, result : 'T) = if success then Some result else None
