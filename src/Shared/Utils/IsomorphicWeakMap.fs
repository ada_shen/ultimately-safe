namespace UltimatelySafe.Shared.Utils

open UltimatelySafe.Shared.Utils.Options

///An abstraction over either ConditionalWeakTable or WeakMap, usable from either client or server
type IsomorphicWeakMap<'K, 'V when 'K : not struct and 'V : not struct>(?init : ('K * 'V) seq) =
    #if FABLE_COMPILER
    let mutable weakMap = Fable.Core.JS.Constructors.WeakMap.Create(?iterable = init)

    member _.Set (key : 'K) (value : 'V) = weakMap.set(key, value) |> ignore

    member _.Delete (key : 'K) = weakMap.delete(key)

    member _.Has (key : 'K) = weakMap.has(key)

    member _.Get (key : 'K) = if weakMap.has(key) then Some (weakMap.get(key)) else None

    member _.Clear () = weakMap <- Fable.Core.JS.Constructors.WeakMap.Create()
    #else
    let cwt = System.Runtime.CompilerServices.ConditionalWeakTable()
    do
        match init with
        | Some init -> for (k, v) in init do cwt.Add(k, v)
        | None -> ()

    member _.Set (key : 'K) (value : 'V) = cwt.Add(key, value)

    member _.Delete (key : 'K) = cwt.Remove(key)

    member _.Has (key : 'K) = cwt.TryGetValue(key) |> fst

    member _.Get (key : 'K) = cwt.TryGetValue(key) |> optionOfTryPair

    member _.Clear () = cwt.Clear()
    #endif

    member this.GetOrCompute (key : 'K) (computation : unit -> 'V) =
        match this.Get key with
        | Some value -> value
        | None ->
            let newValue = computation ()
            this.Set key newValue
            newValue
