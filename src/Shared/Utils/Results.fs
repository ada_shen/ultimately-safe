module UltimatelySafe.Shared.Utils.Results

/// Raised by Result.get in the error case
exception ResultError of error : obj with
    override this.Message =
        match this.error with
        | :? string as message -> message
        | error -> sprintf "%A" error

type Result<'T, 'Err> with
    /// Unsafely gets the Ok value of a Result. Raises a ResultError in the error case.
    static member get<'T, 'Err> (r : Result<'T, 'Err>) =
        match r with
        | Ok value -> value
        | Error error -> raise <| ResultError error

    /// Appends one Result to another using provided functions for appending their constituent types
    static member append<'T, 'Err>
        (appendT : 'T -> 'T -> 'T)
        (appendErr : 'Err -> 'Err -> 'Err)
        (a : Result<'T, 'Err>)
        (b : Result<'T, 'Err>)
        =
        match (a, b) with
        | (Ok x, Ok y) -> Ok (appendT x y)
        | (Error err, Ok _) | (Ok _, Error err) -> Error err
        | (Error err1, Error err2) -> Error (appendErr err1 err2)

    /// Equivalent to Result.defaultValue, which for some reason is not implemented in Fable
    static member orValue<'T, 'Err> (defaultVal : 'T) (r : Result<'T, 'Err>) =
        match r with
        | Ok value -> value
        | Error _ -> defaultVal

    /// Equivalent to Result.defaultWith, which for some reason is not implemented in Fable
    static member orElse<'T, 'Err> (onError : 'Err -> 'T) (r : Result<'T, 'Err>) =
        match r with
        | Ok value -> value
        | Error err -> onError err

    /// Equivalent to Result.toOption, which for some reason is not implemented in Fable
    static member asOption<'T, 'Err> (r : Result<'T, 'Err>) =
        match r with
        | Ok value -> Some value
        | Error _ -> None

    /// Equivalent to Result.isOk, which for some reason is not implemented in Fable
    static member isOkay<'T, 'Err> (r : Result<'T, 'Err>) =
        match r with
        | Ok _ -> true
        | Error _ -> false

    /// Equivalent to Result.isError, which for some reason is not implemented in Fable
    static member isErrored<'T, 'Err> (r : Result<'T, 'Err>) =
        match r with
        | Ok _ -> false
        | Error _ -> true
