module UltimatelySafe.Shared.Utils.Functions

//Why is <| left-associative?
//The operator name is because $ is reserved and associativity is determined by the first character,
//with ^ being the first usable right-associative option.
/// A right-associative version of the pipe-left operator.
/// As close as you can get in F# to Haskell's `$`.
/// Note that this operator has higher precedence than both the normal F# pipe operators.
let inline ( ^<| ) f x = f x

/// Composes a function of two curried arguments with a function of one argument
let inline ( >>+ ) f1 f2 a b = f2 (f1 a b)

/// Composes a function of two curried arguments with a function of one argument (right to left)
let inline ( <<+ ) f2 f1 a b = f2 (f1 a b)

/// Applies a side effect to a value
let inline tap f x =
    f x
    x

/// Pipes a value through a side effect
let inline ( |>! ) x f = tap f x
