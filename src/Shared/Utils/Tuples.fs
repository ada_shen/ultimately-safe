module UltimatelySafe.Shared.Utils.Tuples

/// Turns two arguments into a two-element tuple
let inline pair a b = (a, b)

/// Utilities for two-element tuples
module Pair =
    /// Applies the given function to both elements of the tuple and returns a new tuple
    let inline map<'T, 'U> (f : 'T -> 'U) (a : 'T, b : 'T) = (f a, f b)

    /// Adds a pair of deltas to a pair of numbers and returns a new pair
    let inline add (a, b) (dA, dB) = (a + dA, b + dB)

    /// Subtracts a pair of deltas from a pair of numbers and returns a new pair
    let inline sub (a, b) (dA, dB) = (a - dA, b - dB)

/// Composes a pair-returning function with a function of two curried arguments.
let inline ( ||>> ) f1 f2 x = x |> f1 ||> f2

/// Composes a pair-returning function with a function of two curried arguments (right to left).
let inline ( <<|| ) f2 f1 x = x |> f1 ||> f2
