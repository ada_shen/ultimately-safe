module UltimatelySafe.Shared.Utils.Dictionaries

open UltimatelySafe.Shared.Utils.Options

open System.Collections.Generic

type Dictionary<'K, 'V> with
    /// If the key is present in the Dictionary, returns the associated value.
    /// If the key is not present, uses the computation to construct a value which is assigned to the key and returned.
    member this.GetOrCompute(key : 'K, computation : unit -> 'V) =
        match this.TryGetValue(key) |> optionOfTryPair with
        | Some value -> value
        | None ->
            let newValue = computation ()
            this.Add(key, newValue)
            newValue
