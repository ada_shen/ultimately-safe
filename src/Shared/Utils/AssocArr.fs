/// Module containing functions for working with association arrays
[<RequireQualifiedAccess>]
module UltimatelySafe.Shared.Utils.AssocArr

/// Gets the index of the specified key in the array.
/// Raises a KeyNotFoundException if it is not found.
let findIndex key arr = arr |> Array.findIndex (fst >> (( = ) key))

/// Gets the index of the specified key in the array, or None if it is not found
let tryFindIndex key arr = arr |> Array.tryFindIndex (fst >> (( = ) key))

/// Gets the element from the association array with the specified key.
/// Raises a KeyNotFoundException if it is not found.
let get key arr = arr |> Array.find (fst >> (( = ) key)) |> snd

/// Returns a new array with the specified key removed
let remove key arr = arr |> tryFindIndex key |> Option.map (fun i -> arr |> Array.removeAt i) |> Option.defaultValue arr

/// Searches for an element in the array whose key-value pair matches a predicate
let tryFind pred arr = arr |> Array.tryFind pred |> Option.map snd

/// Returns a seq of the keys in the array
let keys arr = arr |> Seq.map fst

/// Returns a seq of the values in the array
let values arr = arr |> Seq.map snd
