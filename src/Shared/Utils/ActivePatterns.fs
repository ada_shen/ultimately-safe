module UltimatelySafe.Shared.Utils.ActivePatterns

/// Allows you to check a value for equality as part of a pattern match
let (|Equals|_|) arg x = if arg = x then Some () else None

/// Matches a string against a prefix and returns the remainder
let (|Prefix|_|) (prefix : string) (s : string) =
    if s.StartsWith(prefix) then Some (s.Substring(prefix.Length)) else None
