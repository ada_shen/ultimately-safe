module UltimatelySafe.Shared.Utils.Booleans

let inline nand a b = not (a && b)

let inline nor a b = not (a || b)
