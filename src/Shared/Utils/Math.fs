module UltimatelySafe.Shared.Utils.Math

/// Wraps a given number within a range such that
/// min - 1 = max
/// max + 1 = min
let wrap min max n =
    if n < min then
        max - ((min - n - 1) % (max - min + 1))
    else
        min + ((n - min) % (max - min + 1))
