module UltimatelySafe.Shared.Exceptions

open System

//This file serves as a central place to define custom exceptions for the app

/// An isomorphic replacement for System.ArgumentException,
/// which for some reason is not mapped to a unique runtime class in Fable
exception ArgumentsInvalidException of message : string with
    override this.Message = this.message

/// Active pattern that matches on all custom exceptions in the app
let (|AppException|_|) (ex : Exception) =
    match ex with
    | ArgumentsInvalidException _ -> Some ex
    | _ -> None

[<AutoOpen>]
type Helpers =
    static member argInvalid(argName : string, ?message : string) =
        let infoMessage =
            match message with
            | Some message -> $"is invalid: {message}"
            | None -> "is invalid"
        raise <| ArgumentsInvalidException $"Argument `{argName}` {infoMessage}"

    static member argsInvalid (message : string) = raise <| ArgumentsInvalidException message
