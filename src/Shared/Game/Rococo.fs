[<RequireQualifiedAccess>]
module UltimatelySafe.Shared.Game.Rococo

open System.Collections.Generic

open UltimatelySafe.Shared.Exceptions
open UltimatelySafe.Shared.Game.Common
open UltimatelySafe.Shared.Utils
open UltimatelySafe.Shared.Utils.ActivePatterns
open UltimatelySafe.Shared.Utils.Dictionaries
open UltimatelySafe.Shared.Utils.Results
open UltimatelySafe.Shared.Utils.Tuples

/// The set of pieces used in Rococo
/// For an explanation of Rococo pieces and how they capture, see:
/// https://www.chessvariants.com/other.dir/rococo.dir/rococo-moves.html
type PieceType =
    | Advancer
    | LongLeaper
    | Swapper
    | Withdrawer
    | Immobilizer
    | Chameleon
    | CannonPawn
    | King

    interface IPieceType with
        override this.Name =
            match this with
            | LongLeaper -> "Long Leaper"
            | CannonPawn -> "Cannon Pawn"
            | _ -> sprintf "%A" this

        override this.Abbreviation =
            match this with
            | Advancer -> "A"
            | LongLeaper -> "L"
            | Swapper -> "S"
            | Withdrawer -> "W"
            | Immobilizer -> "I"
            | Chameleon -> "C"
            | CannonPawn -> "P"
            | King -> "K"

/// The size of the (square) Rococo board, including the outer ring
let boardSize = 10
let private lastIndex = boardSize - 1

/// Returns true if a coordinate is inside the outer ring in the main board
let coordInMainBoard (coord : int) = coord > 0 && coord < lastIndex

/// Returns true if a coordinate is positioned in the outer ring
let coordInRing = not << coordInMainBoard

/// A position on the Rococo game board
type Position(rank : int, file : int) =
    inherit BoardPosition(rank, file)

    do Position.ValidateCoords rank file

    static member ValidateCoords (rank : int) (file : int) =
        if rank < 0 || rank >= boardSize then
            argInvalid(nameof rank, $"{rank} is an invalid Rococo rank value. Must be within [0, {boardSize}].")

        if file < 0 || file >= boardSize then
            argInvalid(nameof file, $"{file} is an invalid Rococo file value. Must be within [0, {boardSize}].")

    /// Attempts to construct a Rococo Position with the given coords. Returns Error if the coords are invalid.
    static member TryCreate(rank : int, file : int) =
        try Ok (Position(rank, file))
        with ArgumentsInvalidException message -> Error message

    member this.WithCoords'(?rank : int, ?file : int) = Position(defaultArg rank this.Rank, defaultArg file this.File)

    override this.WithCoords(?rank : int, ?file : int) = this.WithCoords'(?rank = rank, ?file = file)

    override this.WithIndexes(?rank : int, ?file : int) = Position(defaultArg rank this.Rank, defaultArg file this.File)

/// Destructures the Coords of an Ultima Position
let (|Position|) (pos : Position) = pos.Coords

type Position with
    /// Returns true if a position is inside the outer ring in the main board
    static member IsInMainBoard (Position (rank, file)) = coordInMainBoard rank && coordInMainBoard file

    /// Returns true if a position is in a space belonging to the outer ring
    static member IsInRing (pos : Position) = not (Position.IsInMainBoard pos)

    /// True if the position is inside the outer ring in the main board
    member this.InMainBoard = Position.IsInMainBoard this

    /// True if the position is in a space belonging to the outer ring
    member this.InRing = Position.IsInRing this

type State = GameState<PieceType>

/// Returns the initial state for a new Rococo game
let startingState () =
    {GameState.empty () with
        Key = GameStateKey()
        Pieces = makePieces [
            (CannonPawn, 8)
            (LongLeaper, 2)
            (Advancer, 1)
            (Swapper, 1)
            (Withdrawer, 1)
            (Immobilizer, 1)
            (Chameleon, 1)
            (King, 1)
        ]
        Board = makeBoard [
            [ "" ;  "" ;  "" ;  "" ;  "" ;  "" ;  "" ;  "" ;  "" ; "" ]
            [ "" ; "BI"; "BW"; "BL"; "BK"; "BC"; "BL"; "BA"; "BS"; "" ]
            [ "" ; "BP"; "BP"; "BP"; "BP"; "BP"; "BP"; "BP"; "BP"; "" ]
            [ "" ;  "" ;  "" ;  "" ;  "" ;  "" ;  "" ;  "" ;  "" ; "" ]
            [ "" ;  "" ;  "" ;  "" ;  "" ;  "" ;  "" ;  "" ;  "" ; "" ]
            [ "" ;  "" ;  "" ;  "" ;  "" ;  "" ;  "" ;  "" ;  "" ; "" ]
            [ "" ;  "" ;  "" ;  "" ;  "" ;  "" ;  "" ;  "" ;  "" ; "" ]
            [ "" ; "WP"; "WP"; "WP"; "WP"; "WP"; "WP"; "WP"; "WP"; "" ]
            [ "" ; "WI"; "WW"; "WL"; "WK"; "WC"; "WL"; "WA"; "WS"; "" ]
            [ "" ;  "" ;  "" ;  "" ;  "" ;  "" ;  "" ;  "" ;  "" ; "" ]
        ]
    }

/// Given a position on a Rococo board, returns all positions adjacent to it
let adjacentSpaces (Position (rank, file)) = seq {
    for r in max 0 (rank - 1) .. min lastIndex (rank + 1) do
        for f in max 0 (file - 1) .. min lastIndex (file + 1) do
            if r <> rank || f <> file then Position(r, f)
}

/// Given a position on a Rococo board, returns all pieces in adjacent occupied spaces
let adjacentPieces (game : State) (pos : Position) = seq {
    for Position (r, f) in adjacentSpaces pos do
        match game.Board[r][f] with
        | Some piece -> game.Pieces[piece]
        | None -> ()
}

/// Returns true if an Immobilizer (or a Chameleon acting as an Immobilizer) owned by the given player
/// is adjacent to the given position
let immobilizerAdjacent (game : State) (player : Player) (position : Position) =
    position
    |> adjacentPieces game
    |> Seq.exists (function
        | {Type = Immobilizer; Player = Equals player} -> true
        | {Type = Chameleon; Player = Equals player} ->
            match GameState.pieceAtPosition game position with
            | Some {Type = Immobilizer} -> true
            | _ -> false
        | _ -> false
    )

module private Movement =
    let private lastMoveIfSwap (game : State) =
        game.Moves
        |> Array.tryLast
        |> Option.bind (fun move ->
            match move.Components with
            | [|Relocate (_, fromA, toA); Relocate (_, fromB, toB)|] when fromA = toB && toA = fromB ->
                Some (fromA :?> Position, toA :?> Position)
            | _ -> None
        )

    let private plus' (filterRay : int * int -> Position seq -> Position seq) (Position (rank, file)) = seq {
        //White's Up
        yield! seq {for r in rank + 1 .. lastIndex -> Position(r, file)} |> filterRay (1, 0)

        //Down
        yield! seq {for r in rank - 1 .. -1 .. 0 -> Position(r, file)} |> filterRay (-1, 0)

        //Right
        yield! seq {for f in file + 1 .. lastIndex -> Position(rank, f)} |> filterRay (0, 1)

        //Left
        yield! seq {for f in file - 1 .. -1 .. 0 -> Position(rank, f)} |> filterRay (0, -1)
    }

    let private queen' (filterRay : int * int -> Position seq -> Position seq) (Position (rank, file) as pos) = seq {
        yield! plus' filterRay pos

        //White's Up-left
        yield! seq {for d in 1 .. min (lastIndex - rank) file -> Position(rank + d, file - d)}
        |> filterRay (1, -1)

        //Up-right
        yield! seq {for d in 1 .. min (lastIndex - rank) (lastIndex - file) -> Position(rank + d, file + d)}
        |> filterRay (1, 1)

        //Down-left
        yield! seq {for d in 1 .. min rank file -> Position(rank - d, file - d)}
        |> filterRay (-1, -1)

        //Down-right
        yield! seq {for d in 1 .. min rank (lastIndex - file) -> Position(rank - d, file + d)}
        |> filterRay (-1, 1)
    }

    //Due to the rules regarding the outer ring and how they interact, each piece needs its own implementation.
    //To sum up, a piece can only land in an edge square if it is the shortest possible move to make a given capture.

    let king (game : State) (player : Player) (fromPos : Position) =
        fromPos
        |> adjacentSpaces
        |> Seq.filter (fun pos ->
            let spaceIsOccupiedByCurrentPlayer = pos |> GameState.spaceIsOccupiedByPlayer game player
            let spaceIsEmpty = pos |> GameState.spaceIsEmpty game
            let inRing = pos.InRing
            not (spaceIsOccupiedByCurrentPlayer || spaceIsEmpty && inRing)
        )

    let longLeaper (game : State) (player : Player) (fromPos : Position) =
        fromPos
        |> queen' (fun _ ray ->
            //Long Leapers cannot jump over friendly pieces or two adjacent pieces.
            //They can make multiple subsequent captures.
            //The rule with the outer ring is that they cannot land more than one ring space past the last capture.
            //Dump the ray seq to an array so we can have lookahead/behind when filtering.
            let array = ray |> Seq.toArray
            array
            |> Seq.indexed
            |> Seq.takeWhile (fun (i, pos) ->
                let spaceIsOccupiedByCurrentPlayer = pos |> GameState.spaceIsOccupiedByPlayer game player
                let spaceIsOccupied = pos |> GameState.spaceIsOccupied game
                let nextSpaceIsOccupied = i >= array.Length - 1 || array[i + 1] |> GameState.spaceIsOccupied game
                not (spaceIsOccupiedByCurrentPlayer || spaceIsOccupied && nextSpaceIsOccupied)
            )
            |> Seq.filter (fun (i, pos) ->
                let spaceIsEmpty = pos |> GameState.spaceIsEmpty game
                let inRing = pos.InRing
                let lastSpaceHasEnemy =
                    i > 0 && array[i - 1] |> GameState.spaceIsOccupiedByPlayer game (Player.oppositeTo player)
                spaceIsEmpty && (not inRing || lastSpaceHasEnemy)
            )
            |> Seq.map snd
        )

    let advancer (game : State) (player : Player) (fromPos : Position) =
        fromPos
        |> queen' (fun _ ray ->
            //Advancers capture pieces in the space after the one they land on
            let array = ray |> Seq.toArray
            array
            |> Seq.takeWhile (GameState.spaceIsEmpty game)
            |> Seq.indexed
            |> Seq.filter (fun (i, pos) ->
                let inRing = pos.InRing
                let nextSpaceHasEnemy =
                    i < array.Length - 1
                        && array[i + 1] |> GameState.spaceIsOccupiedByPlayer game (Player.oppositeTo player)
                not inRing || nextSpaceHasEnemy
            )
            |> Seq.map snd
        )

    let withdrawer (game : State) (player : Player) (fromPos : Position) =
        fromPos
        |> queen' (fun delta ray ->
            //Withdrawers capture pieces in the space immediately before their starting space.
            //This means that we need to allow a one-space movement into the ring
            //only when we are moving directly away from an adjacent enemy piece.
            let isCapture =
                Pair.sub fromPos.Coords delta
                |> Position.TryCreate
                |> Result.map (GameState.spaceIsOccupiedByPlayer game (Player.oppositeTo player))
                |> Result.orValue false
            ray
            |> Seq.indexed
            |> Seq.takeWhile (fun (i, pos) ->
                let spaceIsEmpty = pos |> GameState.spaceIsEmpty game
                let inRing = pos.InRing
                spaceIsEmpty && (not inRing || i = 0 && isCapture)
            )
            |> Seq.map snd
        )

    let swapper (game : State) (_ : Player) (fromPos : Position) =
        let maybeLastSwap = game |> lastMoveIfSwap
        fromPos
        |> queen' (fun _ ray ->
            //Swappers can move passively like Queens.
            //They can also "swap" with *any* piece a Queen's move away.
            //They can swap with pieces in the ring, but cannot otherwise move in the ring.
            //If two Swappers/Chameleons swap, they cannot swap back on the next turn.
            let array = ray |> Seq.toArray
            array
            |> Seq.indexed
            |> Seq.takeWhile (fun (i, pos) ->
                let lastSpaceIsEmpty = i = 0 || array[i - 1] |> GameState.spaceIsEmpty game
                let reswap =
                    match maybeLastSwap with
                    | Some (swappedFrom, swappedTo) ->
                        (fromPos, pos) = (swappedFrom, swappedTo) || (fromPos, pos) = (swappedTo, swappedFrom)
                    | None -> false
                lastSpaceIsEmpty && not reswap
            )
            |> Seq.map snd
            |> Seq.filter (fun pos ->
                let inRing = pos.InRing
                let spaceIsOccupied = pos |> GameState.spaceIsOccupied game
                not inRing || spaceIsOccupied
            )
        )

    let immobilizer (game : State) (_ : Player) (fromPos : Position) =
        fromPos
        |> queen' (fun _ ray ->
            ray
            |> Seq.takeWhile (fun pos ->
                let spaceIsEmpty = pos |> GameState.spaceIsEmpty game
                let inRing = pos.InRing
                spaceIsEmpty && not inRing
            )
        )

    let cannonPawn (game : State) (player : Player) (fromPos : Position) = seq {
        //The Cannon Pawn, on its own, can move one direction in any space.
        //If an adjacent piece is occupied, however, it "leaps" over that piece and lands one space beyond.
        //Leaps (and only leaps) are allowed to capture (by displacement).
        for dRank in [-1; 0; 1] do
            for dFile in [-1; 0; 1] do
                let delta = (dRank, dFile)
                if delta <> (0, 0) then
                    match Pair.add fromPos.Coords delta |> Position.TryCreate with
                    | Ok pos ->
                        match GameState.pieceAtPosition game pos with
                        | Some _ ->
                            match Pair.add pos.Coords delta |> Position.TryCreate with
                            | Ok afterPos ->
                                match GameState.pieceAtPosition game afterPos with
                                | Some {Player = Equals (Player.oppositeTo player)} -> afterPos
                                | None when not afterPos.InRing -> afterPos
                                | _ -> ()
                            | Error _ -> ()
                        | None when not pos.InRing -> pos
                        | _ -> ()
                    | Error _ -> ()
    }

    let chameleon (game : State) (player : Player) (fromPos : Position) =
        let maybeLastSwap = game |> lastMoveIfSwap
        fromPos
        |> queen' (fun delta ray ->
            let opponent = Player.oppositeTo player
            let moveStartedInRing = fromPos.InRing
            let isCapturingWithdrawer =
                Pair.sub fromPos.Coords delta
                |> Position.TryCreate
                |> Result.asOption
                |> Option.bind (GameState.pieceAtPosition game)
                |> (function
                    | Some {Type = Withdrawer; Player = Equals opponent} -> true
                    | _ -> false
                )
            let array = ray |> Seq.toArray
            array
            |> Seq.indexed
            |> Seq.takeWhile (fun (i, pos) ->
                let spaceIsEmpty = pos |> GameState.spaceIsEmpty game
                let inRing = pos.InRing
                let piece = pos |> GameState.pieceAtPosition game
                let lastPiece = if i = 0 then None else array[i - 1] |> GameState.pieceAtPosition game
                let nextPiece = if i = array.Length - 1 then None else array[i + 1] |> GameState.pieceAtPosition game

                let isCapturingKing =
                    i = 0 && match piece with Some {Type = King; Player = Equals opponent} -> true | _ -> false

                let isSwappingWithSwapper =
                    match piece with
                    | Some {Type = Swapper} ->
                        match maybeLastSwap with
                        | Some (swappedFrom, swappedTo) ->
                            (fromPos, pos) <> (swappedFrom, swappedTo) && (fromPos, pos) <> (swappedTo, swappedFrom)
                        | None -> true
                    | _ -> false

                let isLeapingToCapturePawn =
                    i = 0
                        && piece.IsSome
                        && match nextPiece with Some {Type = CannonPawn; Player = Equals opponent} -> true | _ -> false
                let isCapturingPawn =
                    i = 1
                        && lastPiece.IsSome
                        && match piece with Some {Type = CannonPawn; Player = Equals opponent} -> true | _ -> false

                let isCapturingLongLeaper =
                    match piece with Some {Type = LongLeaper; Player = Equals opponent} -> true | _ -> false
                let didCaptureLongLeaper =
                    match lastPiece with Some {Type = LongLeaper; Player = Equals opponent} -> true | _ -> false

                let canMoveIntoSpace =
                    //In order to find all possible captures when moving along the edge of the board,
                    //allow all ring moves if the move started in the ring.
                    //We'll then filter out the disallowed spaces in the next pass.
                    spaceIsEmpty && (not inRing || moveStartedInRing)
                        || isCapturingKing
                        || i = 0 && spaceIsEmpty && isCapturingWithdrawer
                        || isSwappingWithSwapper
                        || isLeapingToCapturePawn
                        || isCapturingPawn
                        || isCapturingLongLeaper
                        || didCaptureLongLeaper

                let stoppedByCapture =
                    match lastPiece with
                    | Some {Type = LongLeaper} -> false
                    | Some _ -> not isCapturingPawn
                    | None -> false

                canMoveIntoSpace && not stoppedByCapture
            )
            |> Seq.filter (fun (i, pos) ->
                let inRing = pos.InRing
                let spaceIsEmpty = pos |> GameState.spaceIsEmpty game
                let piece = pos |> GameState.pieceAtPosition game
                let lastPiece = if i = 0 then None else array[i - 1] |> GameState.pieceAtPosition game
                let nextPiece = if i = array.Length - 1 then None else array[i + 1] |> GameState.pieceAtPosition game

                //Filter out occupied spaces that we're only leaping over and not displacing
                let jumpedPiece =
                    match piece with
                    //You can never displace a Long Leaper
                    | Some {Type = LongLeaper} -> true
                    //The only way we pass through a King is if it's adjacent, in which case we can displace it.
                    //Likewise, we can always land on a Swapper to swap with it.
                    | Some {Type = King} | Some {Type = Swapper} -> false
                    //Otherwise, we're leaping to capture a Cannon Pawn. This could be the mount or the actual capture.
                    //Since this is the only possibility left, we can just check the index:
                    //if this is the first space of the move, it's the mount we're leaping over;
                    //otherwise, we're looking at the captured Pawn itself, which we allow to be displaced
                    | Some _ -> i = 0
                    | None -> false

                let didCaptureLongLeaper = match lastPiece with Some {Type = LongLeaper} -> true | _ -> false

                let isCapturingAdvancer =
                    match nextPiece with Some {Type = Advancer; Player = Equals opponent} -> true | _ -> false

                let isSwappingWithSwapper = match piece with Some {Type = Swapper} -> true | _ -> false

                //Filter out non-capturing spaces when moving along the edge of the board
                let illegalRingSpace =
                    inRing
                        && spaceIsEmpty
                        && not (
                            didCaptureLongLeaper
                                || isCapturingAdvancer
                                || i = 0 && isCapturingWithdrawer
                                || isSwappingWithSwapper
                        )

                not (jumpedPiece || illegalRingSpace)
            )
            |> Seq.map snd
        )

let private movementCache = IsomorphicWeakMap<GameStateKey, Dictionary<Position, Set<Position>>>()

/// Given a Rococo game state and a starting position, returns the set of all spaces to which that piece can move
let pieceMovement (game : State) (fromPos : Position) =
    let gameMovementCache = movementCache.GetOrCompute game.Key Dictionary
    gameMovementCache.GetOrCompute(fromPos, fun () ->
        match GameState.pieceAtPosition game fromPos with
        | Some piece ->
            match piece.Type with
            | Advancer -> Set (Movement.advancer game piece.Player fromPos)
            | LongLeaper -> Set (Movement.longLeaper game piece.Player fromPos)
            | Swapper -> Set (Movement.swapper game piece.Player fromPos)
            | Withdrawer -> Set (Movement.withdrawer game piece.Player fromPos)
            | Immobilizer -> Set (Movement.immobilizer game piece.Player fromPos)
            | Chameleon -> Set (Movement.chameleon game piece.Player fromPos)
            | CannonPawn -> Set (Movement.cannonPawn game piece.Player fromPos)
            | King -> Set (Movement.king game piece.Player fromPos)
        | None -> Set.empty
    )

/// Returns true if a move is legal between the given start and end positions in the given game state
let pieceCanMoveToSpace (game : State) (fromPos : Position) (toPos : Position) =
    match GameState.pieceAtPosition game fromPos with
    | Some piece ->
        not (immobilizerAdjacent game (Player.oppositeTo piece.Player) fromPos)
            && pieceMovement game fromPos |> Set.contains toPos
    | None -> false

module private Capturing =
    let private opposingPieceAtPosition (game : State) (pos : Position) =
        GameState.pieceAtPosition game pos
        |> Option.filter (fun p -> p.Player = Player.oppositeTo game.Player)

    let king'
        (capturablePieceAtPosition : Position -> Piece<PieceType> option)
        (_game : State)
        (_fromPos : Position)
        (toPos : Position)
        = seq {
            //The king is unchanged from normal Chess and captures by displacement
            match capturablePieceAtPosition toPos with
            | Some targetPiece -> Capture (targetPiece.Id, toPos)
            | None -> ()
        }

    let king (game : State) (fromPos : Position) (toPos : Position) =
        king' (opposingPieceAtPosition game) game fromPos toPos

    let cannonPawn'
        (capturablePieceAtPosition : Position -> Piece<PieceType> option)
        (_game : State)
        (fromPos : Position)
        (toPos : Position)
        = seq {
            //The Cannon Pawn can hop one space over adjacent pieces.
            //When doing so, it can capture any piece it lands on.
            //The Pawn can only move one space at a time otherwise, so we can just look at the distance.
            let distance = Pair.sub toPos.Coords fromPos.Coords |> Pair.map abs ||> max
            if distance = 2 then
                match capturablePieceAtPosition toPos with
                | Some targetPiece -> Capture (targetPiece.Id, toPos)
                | None -> ()
        }

    let cannonPawn (game : State) (fromPos : Position) (toPos : Position) =
        cannonPawn' (opposingPieceAtPosition game) game fromPos toPos

    let advancer'
        (capturablePieceAtPosition : Position -> Piece<PieceType> option)
        (_game : State)
        (fromPos : Position)
        (toPos : Position)
        = seq {
            //The Advancer captures a piece one space beyond where it ends its move
            let step = Pair.sub toPos.Coords fromPos.Coords |> Pair.map sign
            let targetPos = Pair.add toPos.Coords step |> Position
            match capturablePieceAtPosition targetPos with
            | Some targetPiece -> Capture (targetPiece.Id, targetPos)
            | None -> ()
        }

    let advancer (game : State) (fromPos : Position) (toPos : Position) =
        advancer' (opposingPieceAtPosition game) game fromPos toPos

    let longLeaper'
        (capturablePieceAtPosition : Position -> Piece<PieceType> option)
        (_game : State)
        (Position (fromRank, fromFile) as fromPos)
        (toPos : Position)
        = seq {
            //The Long Leaper captures by jumping over enemy pieces, similar to in Checkers.
            //It can make multiple hops, but cannot make a turn (can only capture in a straight line).
            let delta = Pair.sub toPos.Coords fromPos.Coords
            let distance = delta |> Pair.map abs ||> max
            let (rankStep, fileStep) = delta |> Pair.map sign
            for n in 1 .. distance - 1 do
                let targetPos = Position(fromRank + rankStep * n, fromFile + fileStep * n)
                match capturablePieceAtPosition targetPos with
                | Some targetPiece -> Capture (targetPiece.Id, targetPos)
                | None -> ()
        }

    let longLeaper (game : State) (fromPos : Position) (toPos : Position) =
        longLeaper' (opposingPieceAtPosition game) game fromPos toPos

    let withdrawer'
        (capturablePieceAtPosition : Position -> Piece<PieceType> option)
        (_game : State)
        (fromPos : Position)
        (toPos : Position)
        = seq {
            //The Withdrawer captures a piece by moving directly away from it
            let step = Pair.sub toPos.Coords fromPos.Coords |> Pair.map sign
            let targetPos = Pair.sub fromPos.Coords step |> Position.TryCreate
            match targetPos with
            | Ok targetPos ->
                match capturablePieceAtPosition targetPos with
                | Some targetPiece -> Capture (targetPiece.Id, targetPos)
                | None -> ()
            | Error _ -> ()
        }

    let withdrawer (game : State) (fromPos : Position) (toPos : Position) =
        withdrawer' (opposingPieceAtPosition game) game fromPos toPos

    let swapper'
        (capturablePieceAtPosition : Position -> Piece<PieceType> option)
        (_game : State)
        (fromPos : Position)
        (toPos : Position)
        = seq {
            //The Swapper "captures" by displacement,
            //except it doesn't actually capture and instead moves the piece it lands on to where it moved from
            match capturablePieceAtPosition toPos with
            | Some targetPiece -> Relocate (targetPiece.Id, toPos, fromPos)
            | None -> ()
        }

    let swapper (game : State) (fromPos : Position) (toPos : Position) =
        swapper' (GameState.pieceAtPosition game) game fromPos toPos

    let chameleon (game : State) (fromPos : Position) (toPos : Position) = seq {
        //Chameleons capture a piece by using that piece's manner of capturing *and movement*.
        //This means that they can only capture kings with a one-space move, pawns by vaulting,
        //and cannot capture Immobilizers or other Chameleons at all.
        //Chameleons can swap with Swappers (of either side).

        let capturablePieceOfType pieceType =
            opposingPieceAtPosition game >> Option.filter (fun p -> p.Type = pieceType)

        let delta = Pair.sub toPos.Coords fromPos.Coords
        let distance = delta |> Pair.map abs ||> max
        let step = delta |> Pair.map sign
        let firstPos = Pair.add fromPos.Coords step |> Position

        let moveIsAdjacent = distance = 1
        let moveIsPawnLeap = distance = 2 && GameState.spaceIsOccupied game firstPos

        if moveIsAdjacent then yield! king' (capturablePieceOfType King) game fromPos toPos
        if moveIsPawnLeap then yield! cannonPawn' (capturablePieceOfType CannonPawn) game fromPos toPos
        yield! advancer' (capturablePieceOfType Advancer) game fromPos toPos
        yield! longLeaper' (capturablePieceOfType LongLeaper) game fromPos toPos
        yield! withdrawer' (capturablePieceOfType Withdrawer) game fromPos toPos
        yield! swapper' (GameState.pieceAtPosition game >> Option.filter (fun p -> p.Type = Swapper)) game fromPos toPos
    }

/// Returns all the Capture MoveComponents
/// resulting from a move of the given piece ID between the given start and end positions
let private capturesForMove (game : State) (pieceId : string) (fromPos : Position) (toPos : Position) =
    match game.Pieces[pieceId].Type with
    | Advancer -> Capturing.advancer game fromPos toPos
    | LongLeaper -> Capturing.longLeaper game fromPos toPos
    | Swapper -> Capturing.swapper game fromPos toPos
    | Withdrawer -> Capturing.withdrawer game fromPos toPos
    | Chameleon -> Capturing.chameleon game fromPos toPos
    | CannonPawn -> Capturing.cannonPawn game fromPos toPos
    | King -> Capturing.king game fromPos toPos
    | Immobilizer -> Seq.empty

/// Applies a Move to a Rococo GameState and returns the resulting state
let applyMove (game : State) (move : Move) =
    let newState = GameState.applyMove game move

    //Check win condition
    let kingCaptured =
        move.Components
        |> Seq.exists (function Capture (id, _) when game.Pieces[id].Type = King -> true | _ -> false)
    if kingCaptured then {newState with Key = GameStateKey(); Winner = Some move.Player} else newState

/// Requests for additional input when making Rococo moves
type InputRequest =
    /// Select whether to promote a Pawn, and to which type
    | Promotion of (PieceType option -> MoveWithInputs)

/// Given a starting Rococo GameState, makes a move between the given start and end positions
let makeMove (game : State) (fromPos : Position) (Position (toRank, _) as toPos) =
    match GameState.pieceAtPosition game fromPos with
    | Some piece ->
        if piece.Player <> game.Player then failwith $"%A{game.Player} cannot move %A{piece.Player}'s piece"

        if not (pieceCanMoveToSpace game fromPos toPos) then failwith $"Move from %A{fromPos} to %A{toPos} is invalid"

        let movedToBackRanks =
            match piece.Player with
            | White -> toRank >= boardSize - 2
            | Black -> toRank < 2
        match piece.Type with
        | CannonPawn when movedToBackRanks ->
            Incomplete (Promotion (fun selection ->
                let move = GameState.createMove game fromPos (fun pieceId -> [|
                    match selection with
                    | Some CannonPawn -> failwith "Cannot promote Pawn to Pawn"
                    | Some pieceType ->
                        let revivedPiece =
                            game.CapturedPieces
                            |> AssocArr.tryFind (fun (_, p) -> p.Type = pieceType && p.Player = piece.Player)
                        match revivedPiece with
                        | Some revivedPiece ->
                            Remove (pieceId, fromPos)
                            Revive (revivedPiece.Id, toPos)
                        | None -> failwith $"There is no captured %A{pieceType} to promote to for %A{piece.Player}"
                    | None -> Relocate (pieceId, fromPos, toPos)
                    yield! capturesForMove game pieceId fromPos toPos
                |])
                MoveWithInputs (move, fromPos, toPos, [selection])
            ))
        | _ ->
            let move = GameState.createMove game fromPos (fun pieceId -> [|
                Relocate (pieceId, fromPos, toPos)
                yield! capturesForMove game pieceId fromPos toPos
            |])
            Complete (MoveWithInputs (move, fromPos, toPos, []))
    | None -> failwith $"There is no piece to move at position %A{fromPos}"

/// Given a starting Rococo GameState, performs a self-capture of an immobilized piece at the given position
/// and returns the resulting state
let selfCaptureImmobilizedPiece (game : State) (pos : Position) =
    match GameState.pieceAtPosition game pos with
    | Some piece ->
        if piece.Player <> game.Player then
            failwith $"%A{piece.Player} cannot self-capture piece on %A{game.Player}'s turn"
        if not (immobilizerAdjacent game (Player.oppositeTo piece.Player) pos) then
            failwith "Cannot self-capture piece that is not immobilized"

        GameState.createMove game pos (fun pieceId -> [|Capture (pieceId, pos)|])
        |> applyMove game
    | None -> failwith $"No piece at position %A{pos}"

/// Given a starting Rococo GameState, performs a mutual capture of a Swapper (or Chameleon) and an adjacent piece
/// and returns the resulting state
let mutualCapture (game : State) (sourcePos : Position) (targetPos : Position) =
    let distance = Pair.sub targetPos.Coords sourcePos.Coords |> Pair.map abs ||> max
    if distance <> 1 then failwith $"Cannot perform mutual capture between pieces {distance} spaces away"

    match (sourcePos, targetPos) |> Pair.map (GameState.pieceAtPosition game) with
    | (Some ({Type = Swapper} as source), Some target)
    | (Some ({Type = Chameleon} as source), Some ({Type = Swapper} as target)) ->
        if source.Player <> game.Player then failwith $"%A{game.Player} cannot move %A{source.Player}'s piece"
        if target.Player = source.Player then failwith $"%A{source.Player} cannot mutual-capture their own piece"
        if immobilizerAdjacent game (Player.oppositeTo source.Player) sourcePos then
            failwith "Cannot perform mutual-capture while immobilized"

        GameState.createMove game sourcePos (fun _ -> [|
            Capture (source.Id, sourcePos)
            Capture (target.Id, targetPos)
        |])
        |> applyMove game
    | (Some {Type = Chameleon}, Some target) ->
        failwith $"A Chameleon can only mutual-capture a Swapper. Was given a %A{target.Type}"
    | (None, _) -> failwithf "No piece at source position %A" sourcePos
    | (_, None) -> failwithf "No piece at target position %A" targetPos
    | (Some source, _) -> failwith $"A %A{source.Type} cannot perform mutual capture"

let undoMove (game : State) = GameState.undoMove (startingState ()) game

let redoMove = GameState.redoMove
