[<RequireQualifiedAccess>]
module UltimatelySafe.Shared.Game.Maxima

open System.Collections.Generic

open UltimatelySafe.Shared.Exceptions
open UltimatelySafe.Shared.Game.Common
open UltimatelySafe.Shared.Utils
open UltimatelySafe.Shared.Utils.ActivePatterns
open UltimatelySafe.Shared.Utils.Dictionaries
open UltimatelySafe.Shared.Utils.Math
open UltimatelySafe.Shared.Utils.Results
open UltimatelySafe.Shared.Utils.Tuples

/// The set of pieces used in Maxima
/// For an explanation of Maxima pieces and how they capture, see:
/// https://www.chessvariants.com/dpieces.dir/maxima/maxima.html
type PieceType =
    | Pawn
    | Guard
    | LongLeaper
    | Mage
    | Chameleon
    | Immobilizer
    | Coordinator
    | Withdrawer
    | King

    interface IPieceType with
        override this.Name =
            match this with
            | LongLeaper -> "Long Leaper"
            | _ -> sprintf "%A" this

        override this.Abbreviation =
            match this with
            | Pawn -> "P"
            | Guard -> "G"
            | LongLeaper -> "L"
            | Mage -> "M"
            | Chameleon -> "X"
            | Immobilizer -> "I"
            | Coordinator -> "C"
            | Withdrawer -> "W"
            | King -> "K"

//The Maxima board is not a perfect rectangle. It has two "goals" at the top and bottom.
//Each goal is an extra two-square rank in the center of the board.

/// The maximum value of a Maxima rank value
let maxRankValue = 10

/// The maximum value of a Maxima file value
let maxFileValue = 7

/// Checks whether the given value is within the outer bounds for Maxima rank indexes
let private rankIsValid (r : int) =
    if r < 0 || r > maxRankValue then Error $"{r} is an invalid Maxima rank index. Must be within [0, 10]." else Ok ()

/// Raises an exception if the given value is outside the outer bounds for Maxima rank indexes
let private validateRank (r : int) = rankIsValid r |> Result.get

/// Checks whether the given value is within the outer bounds for Maxima file indexes
let private fileIsValid (f : int) =
    if f < 0 || f > maxFileValue then Error $"{f} is an invalid Maxima file index. Must be within [0, 7]." else Ok ()

/// Raises an exception if the given value is outside the outer bounds for Maxima file indexes
let private validateFile (f : int) = fileIsValid f |> Result.get

/// Given the index of a rank, returns its length
let rankSize (r : int) =
    validateRank r
    match r with
    | 0 | 10 -> 2
    | _ -> 8

/// Given the index of a file, returns its height
let fileSize (f : int) =
    validateFile f
    match f with
    | 3 | 4 -> 11
    | _ -> 9

/// Returns the first natural file number for a given rank
let firstFile (rank : int) =
    validateRank rank
    match rank with
    | 0 | 10 -> 3
    | _ -> 0

/// Returns the last natural file number for a given rank
let lastFile (rank : int) =
    validateRank rank
    match rank with
    | 0 | 10 -> 4
    | _ -> 7

/// Returns the first natural rank number for a given file
let firstRank (file : int) =
    validateFile file
    match file with
    | 3 | 4 -> 0
    | _ -> 1

/// Returns the last natural rank number for a given file
let lastRank (file : int) =
    validateFile file
    match file with
    | 3 | 4 -> 10
    | _ -> 9

/// A position on the Maxima game board
type Position(rank : int, file : int) =
    inherit BoardPosition(rank, file)

    do Position.ValidateCoords rank file |> Result.orElse (raise << ArgumentsInvalidException)

    /// Given a rank, converts a file index to its corresponding natural coordinate
    static member private FileIndexToCoord (rank : int) (fileIndex : int) =
        match rank with
        | 0 | 10 -> fileIndex + 3
        | _ -> fileIndex

    /// Checks whether a given rank and file form a valid pair of Maxima (natural) coordinates
    static member ValidateCoords (rank : int) (file : int) =
        (rankIsValid rank, fileIsValid file)
        ||> Result.append (fun () () -> ()) (fun msg1 msg2 -> $"{msg1}; {msg2}")
        |> Result.bind (fun () ->
            let inVoid =
                if file < firstFile rank then Some "left"
                elif file > lastFile rank then Some "right"
                else None
            match inVoid with
            | Some dir -> Error $"({rank}, {file}) is a non-space in the void to the {dir} of the goal"
            | None -> Ok ()
        )

    /// Attempts to construct a Maxima Position with the given coords. Returns Error if the coords are invalid.
    static member TryCreate(rank : int, file : int) =
        Position.ValidateCoords rank file |> Result.map (fun () -> Position(rank, file))

    /// Constructs a Maxima Position from array indexes.
    /// Raises an ArgumentsInvalidException if the indexes are invalid.
    static member FromIndexes(rankIndex : int, fileIndex : int) =
        Position(rankIndex, Position.FileIndexToCoord rankIndex fileIndex)

    /// Attempts to construct a Maxima Position with the given array indexes. Returns Error if the indexes are invalid.
    static member TryFromIndexes(rankIndex : int, fileIndex : int) =
        let coords = (rankIndex, Position.FileIndexToCoord rankIndex fileIndex)
        coords
        ||> Position.ValidateCoords
        |> Result.map (fun () -> coords |> Position)

    override this.FileIndex =
        match this.Rank with
        | 0 | 10 -> this.File - 3
        | _ -> this.File

    member this.WithCoords'(?rank : int, ?file : int) = Position(defaultArg rank this.Rank, defaultArg file this.File)

    override this.WithCoords(?rank : int, ?file : int) = this.WithCoords'(?rank = rank, ?file = file)

    member this.WithIndexes'(?rank : int, ?file : int) =
        let newRank = defaultArg rank this.RankIndex
        let newFile =
            file
            |> Option.map (Position.FileIndexToCoord newRank)
            |> Option.defaultValue this.FileIndex
        Position(newRank, newFile)

    override this.WithIndexes(?rank : int, ?file : int) = this.WithIndexes'(?rank = rank, ?file = file)

let (|Position|) (pos : Position) = pos.Coords

let (|IndexedPosition|) (pos : Position) = pos.Indexes

type Position with
    /// Returns true if the given position is in the given player's goal
    static member IsInGoal (player : Player) (Position (rank, _)) =
        match player with
        | White -> rank = 0
        | Black -> rank = 10

    /// True if the position is in the given player's goal
    member this.InGoal (player : Player) = Position.IsInGoal player this

type State = GameState<PieceType>

/// Returns the initial state for a new Maxima game
let startingState () =
    {GameState.empty () with
        Key = GameStateKey()
        Pieces = makePieces [
            (Pawn, 6)
            (Guard, 2)
            (LongLeaper, 2)
            (Mage, 2)
            (Chameleon, 2)
            (Immobilizer, 1)
            (Coordinator, 1)
            (Withdrawer, 1)
            (King, 1)
        ]
        Board = makeBoard [
            [                   "" ;  ""                   ]
            [ "" ; "BM";  "" ; "BW"; "BK";  "" ; "BM";  "" ]
            [ "" ; "BX"; "BL"; "BI"; "BC"; "BL"; "BX";  "" ]
            ["BG"; "BP"; "BP"; "BP"; "BP"; "BP"; "BP"; "BG"]
            [ "" ;  "" ;  "" ;  "" ;  "" ;  "" ;  "" ;  "" ]
            [ "" ;  "" ;  "" ;  "" ;  "" ;  "" ;  "" ;  "" ]
            [ "" ;  "" ;  "" ;  "" ;  "" ;  "" ;  "" ;  "" ]
            ["WG"; "WP"; "WP"; "WP"; "WP"; "WP"; "WP"; "WG"]
            [ "" ; "WX"; "WL"; "WC"; "WI"; "WL"; "WX";  "" ]
            [ "" ; "WM";  "" ; "WK"; "WW";  "" ; "WM";  "" ]
            [                   "" ;  ""                   ]
        ]
    }

/// Given a position on a Maxima board, returns all positions adjacent to it
let adjacentSpaces (Position (rank, file)) = seq {
    for r in max 0 (rank - 1) .. min (lastRank file) (rank + 1) do
        for f in max 0 (file - 1) .. min (lastFile rank) (file + 1) do
            if r <> rank || f <> file then
                match Position.TryCreate(r, f) with
                | Ok pos -> pos
                | Error _ -> ()
}

/// Given a position on a Maxima board, returns all pieces in adjacent occupied spaces
let adjacentPieces (game : State) (pos : Position) = seq {
    for IndexedPosition (rankIndex, fileIndex) in adjacentSpaces pos do
        match game.Board[rankIndex].[fileIndex] with
        | Some piece -> game.Pieces[piece]
        | None -> ()
}

let pieceIsImmobilized (game : State) (pos : Position) =
    match GameState.pieceAtPosition game pos with
    | None | Some {Type = Mage} -> false
    | Some piece ->
        pos
        |> adjacentPieces game
        |> Seq.exists (fun otherPiece ->
            otherPiece.Player = Player.oppositeTo piece.Player
                && (otherPiece.Type = Immobilizer || otherPiece.Type = Chameleon && piece.Type = Immobilizer)
        )

module private Movement =
    let private plus' (filterRay : Position seq -> Position seq) (Position (rank, file)) = seq {
        let filterRay' = Seq.takeWhile Result.isOkay >> Seq.map Result.get >> filterRay

        //White's Up
        yield! seq {for r in rank + 1 .. (lastRank file) -> Position.TryCreate(r, file)} |> filterRay'

        //Down
        yield! seq {for r in rank - 1 .. -1 .. (firstRank file) -> Position.TryCreate(r, file)} |> filterRay'

        //Right
        yield! seq {for f in file + 1 .. (lastFile rank) -> Position.TryCreate(rank, f)} |> filterRay'

        //Left
        yield! seq {for f in file - 1 .. -1 .. (firstFile rank) -> Position.TryCreate(rank, f)} |> filterRay'
    }

    let plus (game : State) (fromPos : Position) = plus' (Seq.takeWhile (GameState.spaceIsEmpty game)) fromPos

    let private queen' (filterRay : Position seq -> Position seq) (Position (rank, file) as pos) = seq {
        yield! plus' filterRay pos

        let filterRay' = Seq.takeWhile Result.isOkay >> Seq.map Result.get >> filterRay

        //White's Up-left
        yield! seq {for d in 1 .. min (maxRankValue - rank) file -> Position.TryCreate(rank + d, file - d)}
        |> filterRay'

        //Up-right
        yield! seq {
            for d in 1 .. min (maxRankValue - rank) (maxFileValue - file) -> Position.TryCreate(rank + d, file + d)
        }
        |> filterRay'

        //Down-left
        yield! seq {for d in 1 .. min rank file -> Position.TryCreate(rank - d, file - d)}
        |> filterRay'

        //Down-right
        yield! seq {for d in 1 .. min rank (maxFileValue - file) -> Position.TryCreate(rank - d, file + d)}
        |> filterRay'
    }

    let queen (game : State) (fromPos : Position) =
        queen' (Seq.takeWhile (GameState.spaceIsEmpty game)) fromPos

    let guard (game : State) (player : Player) (fromPos : Position) =
        fromPos
        |> adjacentSpaces
        |> Seq.filter (not << GameState.spaceIsOccupiedByPlayer game player)

    let longLeaper (game : State) (player : Player) (fromPos : Position) =
        fromPos
        |> queen' (fun ray ->
            //Long Leapers cannot jump over friendly pieces or two adjacent pieces.
            //Dump the ray seq to an array so we can have lookahead when filtering.
            let array = ray |> Seq.toArray
            array
            |> Seq.indexed
            |> Seq.takeWhile (fun (i, pos) ->
                let spaceIsOccupiedByCurrentPlayer = pos |> GameState.spaceIsOccupiedByPlayer game player
                let spaceIsOccupied = pos |> GameState.spaceIsOccupied game
                let nextSpaceIsOccupied = i >= array.Length - 1 || array[i + 1] |> GameState.spaceIsOccupied game
                not (spaceIsOccupiedByCurrentPlayer || spaceIsOccupied && nextSpaceIsOccupied)
            )
            |> Seq.map snd
            |> Seq.filter (GameState.spaceIsEmpty game)
        )

    let mage (game : State) (player : Player) (Position (rank, file)) = seq {
        //Mages have a very unique way of moving:
        //First, they move a single space diagonally.
        //Then, as part of the *same move*, they continue orthogonally in one of the directions they moved.
        //(This essentially looks like the outline of a Rook's move.)
        //Since the move is continuous with no jumps, if the initial diagonal space is occupied it will block the entire
        //pair of orthogonal directions. Mages capture opposing pieces by displacement.
        for (dRank, dFile) in Seq.allPairs [-1; 1] [-1; 1] do
            match Pair.add (rank, file) (dRank, dFile) |> Position.TryCreate with
            | Ok startingPoint when not (startingPoint |> GameState.spaceIsOccupiedByPlayer game player) ->
                yield startingPoint

                let endRank = if dRank = 1 then lastRank startingPoint.File else firstRank startingPoint.File
                yield! seq {
                    for r in startingPoint.Rank + dRank .. dRank .. endRank -> Position.TryCreate(r, startingPoint.File)
                }
                |> Seq.takeWhile Result.isOkay
                |> Seq.map Result.get
                |> Seq.takeWhile (fun pos ->
                    let spaceIsOccupiedByCurrentPlayer = pos |> GameState.spaceIsOccupiedByPlayer game player
                    let lastSpaceIsOccupied = pos.WithCoords'(rank = pos.Rank - dRank) |> GameState.spaceIsOccupied game
                    not (spaceIsOccupiedByCurrentPlayer || lastSpaceIsOccupied)
                )

                let endFile = if dFile = 1 then lastFile startingPoint.Rank else firstFile startingPoint.Rank
                yield! seq {
                    for f in startingPoint.File + dFile .. dFile .. endFile -> Position.TryCreate(startingPoint.Rank, f)
                }
                |> Seq.takeWhile Result.isOkay
                |> Seq.map Result.get
                |> Seq.takeWhile (fun pos ->
                    let spaceIsOccupiedByCurrentPlayer = pos |> GameState.spaceIsOccupiedByPlayer game player
                    let lastSpaceIsOccupied = pos.WithCoords'(file = pos.File - dFile) |> GameState.spaceIsOccupied game
                    not (spaceIsOccupiedByCurrentPlayer || lastSpaceIsOccupied)
                )
            | _ -> ()
    }

    let king (game : State) (player : Player) (Position (rank, file)) =
        //The Maxima King moves and captures like a Chess Knight,
        //with the added ability to wrap horizontally around the board
        let deltas = [-2; -1; 1; 2]
        Seq.allPairs deltas deltas
        |> Seq.filter (fun (dRank, dFile) -> abs dRank <> abs dFile)
        |> Seq.map (fun (dRank, dFile) ->
            let rank' = rank + dRank
            //The rules define wrapping as being from/to the first and last two files,
            //so we only need to worry about wrapping between the min and max file values.
            //(Wrapping is not defined for the goal ranks)
            let file' = file + dFile |> wrap 0 maxFileValue
            Position.TryCreate(rank', file')
        )
        |> Seq.filter Result.isOkay
        |> Seq.map Result.get
        |> Seq.filter (not << GameState.spaceIsOccupiedByPlayer game player)

    let chameleon (game : State) (player : Player) (fromPos : Position) =
        fromPos
        |> queen' (fun ray ->
            //Chameleons can leap over enemy Long Leapers, as long as there's an empty space behind them.
            //They can advance into further empty spaces, but cannot leap over non-Long Leapers.
            //They can also capture Guards, Mages, and Kings by displacement, but only from one space away.
            let array = ray |> Seq.toArray
            array
            |> Seq.indexed
            |> Seq.takeWhile (fun (i, pos) ->
                let spaceIsEmpty = pos |> GameState.spaceIsEmpty game
                let nextSpaceIsEmpty = i < array.Length - 1 && array[i + 1] |> GameState.spaceIsEmpty game
                let (spaceHasEnemyLongLeaper, spaceHasDisplacementCapture) =
                    match GameState.pieceAtPosition game pos with
                    | Some {Player = Equals (Player.oppositeTo player); Type = pieceType} ->
                        (pieceType = LongLeaper, [Guard; Mage; King] |> Seq.contains pieceType)
                    | _ -> (false, false)
                let spaceIsOneSpaceAway = i = 0
                let lastSpaceHasDisplacementCapture =
                    i = 1 //You can only capture by displacement from one space away, so we only need to check this here
                        && match GameState.pieceAtPosition game array[0] with
                           | Some {Player = Equals (Player.oppositeTo player); Type = pieceType} ->
                               [Guard; Mage; King] |> Seq.contains pieceType
                           | _ -> false
                not lastSpaceHasDisplacementCapture //We can land on a King but not jump over it
                    && (spaceIsEmpty
                        || spaceHasEnemyLongLeaper && nextSpaceIsEmpty
                        || spaceHasDisplacementCapture && spaceIsOneSpaceAway)
            )
            |> Seq.map snd
            |> Seq.filter (fun pos ->
                let spaceHasDisplacementCapture =
                    match GameState.pieceAtPosition game pos with
                    | Some {Player = Equals (Player.oppositeTo player); Type = pieceType} ->
                        [Guard; Mage; King] |> Seq.contains pieceType
                    | _ -> false
                GameState.spaceIsEmpty game pos || spaceHasDisplacementCapture
            )
        )

let private movementCache = IsomorphicWeakMap<GameStateKey, Dictionary<BoardPosition, Set<Position>>>()

/// Given a Maxima game state and a starting position, returns the set of all spaces to which that piece can move
let pieceMovement (game : State) (fromPos : Position) =
    let gameMovementCache = movementCache.GetOrCompute game.Key Dictionary
    gameMovementCache.GetOrCompute(fromPos, fun () ->
        match GameState.pieceAtPosition game fromPos with
        | Some piece ->
            match piece.Type with
            | Pawn -> Set (Movement.plus game fromPos)
            | Guard -> Set (Movement.guard game piece.Player fromPos)
            | LongLeaper -> Set (Movement.longLeaper game piece.Player fromPos)
            | Mage -> Set (Movement.mage game piece.Player fromPos)
            | King -> Set (Movement.king game piece.Player fromPos)
            | Chameleon -> Set (Movement.chameleon game piece.Player fromPos)
            | _ -> Set (Movement.queen game fromPos)
        | None -> Set.empty
    )

/// Returns true if a move is legal between the given start and end positions in the given game state
let pieceCanMoveToSpace (game : State) (fromPos : Position) (toPos : Position) =
    not (pieceIsImmobilized game fromPos) && pieceMovement game fromPos |> Set.contains toPos

module private Capturing =
    let private opposingPieceAtPosition (game : State) (pos : Position) =
        GameState.pieceAtPosition game pos
        |> Option.filter (fun p -> p.Player = Player.oppositeTo game.Player)

    /// Function used to process captures for all pieces that capture by displacement
    let displacement'
        (capturablePieceAtPosition : Position -> Piece<PieceType> option)
        (_game : State)
        (_fromPos : Position)
        (toPos : Position)
        = seq {
            match capturablePieceAtPosition toPos with
            | Some targetPiece -> Capture (targetPiece.Id, toPos)
            | None -> ()
        }

    let displacement (game : State) (fromPos : Position) (toPos : Position) =
        displacement' (opposingPieceAtPosition game) game fromPos toPos

    let pawn'
        (capturablePieceAtPosition : Position -> Piece<PieceType> option)
        (game : State)
        (_fromPos : Position)
        (toPos : Position)
        = seq {
            //Pawns capture by "pinching":
            //An enemy piece is captured if the pawn ends its move
            //with that piece sandwiched orthogonally between it and another allied piece.
            //The "pinching" does not need to be in the direction of movement.
            for delta in [(-1, 0); (1, 0); (0, -1); (0, 1)] do
                let targetPos = Pair.add toPos.Coords delta |> Position.TryCreate
                let afterTargetPos =
                    targetPos
                    |> Result.bind (fun pos -> Pair.add pos.Coords delta |> Position.TryCreate)
                match (targetPos, afterTargetPos) with
                | (Ok targetPos, Ok afterTargetPos) ->
                    match (capturablePieceAtPosition targetPos, GameState.pieceAtPosition game afterTargetPos) with
                    | (Some targetPiece, Some {Player = Equals game.Player}) -> Capture (targetPiece.Id, targetPos)
                    | _ -> ()
                | _ -> ()
        }

    let pawn (game : State) (fromPos : Position) (toPos : Position) =
        pawn' (opposingPieceAtPosition game) game fromPos toPos

    let longLeaper'
        (capturablePieceAtPosition : Position -> Piece<PieceType> option)
        (_game : State)
        (Position (fromRank, fromFile) as fromPos)
        (toPos : Position)
        = seq {
            //The Long Leaper captures by jumping over enemy pieces, similar to in Checkers.
            //It can make multiple hops, but cannot make a turn (can only capture in a straight line).
            let delta = Pair.sub toPos.Coords fromPos.Coords
            //Assuming straight-line movement, so the abs of both deltas is either equal or zero
            let distance = delta |> Pair.map abs ||> max
            let (rankStep, fileStep) = delta |> Pair.map sign
            for n in 1 .. distance - 1 do
                let targetPos = Position(fromRank + rankStep * n, fromFile + fileStep * n)
                match capturablePieceAtPosition targetPos with
                | Some targetPiece -> Capture (targetPiece.Id, targetPos)
                | None -> ()
        }

    let longLeaper (game : State) (fromPos : Position) (toPos : Position) =
        longLeaper' (opposingPieceAtPosition game) game fromPos toPos

    let coordinator'
        (capturablePieceAtPosition : Position -> Piece<PieceType> option)
        (game : State)
        (_fromPos : Position)
        (toPos : Position)
        = seq {
            //Coordinators capture by "coordinating" with their King:
            //After a Coordinator moves, it captures any enemy piece in its rank and the King's file,
            //or vice-versa

            let (Position (kingRank, kingFile)) =
                game
                |> GameState.findPieceByPlayerAndType Position game.Player King
                |> Option.get
                |> snd

            let targetA = toPos.WithCoords'(file = kingFile)
            match capturablePieceAtPosition targetA with
            | Some targetPiece -> Capture (targetPiece.Id, targetA)
            | None -> ()

            let targetB = toPos.WithCoords'(rank = kingRank)
            match capturablePieceAtPosition targetB with
            | Some targetPiece -> Capture (targetPiece.Id, targetB)
            | None -> ()
        }

    let coordinator (game : State) (fromPos : Position) (toPos : Position) =
        coordinator' (opposingPieceAtPosition game) game fromPos toPos

    let withdrawer'
        (capturablePieceAtPosition : Position -> Piece<PieceType> option)
        (_game : State)
        (Position (fromRank, fromFile) as fromPos)
        (toPos : Position)
        = seq {
            //The Withdrawer captures a piece by moving directly away from it
            let (rankStep, fileStep) = Pair.sub toPos.Coords fromPos.Coords |> Pair.map sign
            let targetPos = Position.TryCreate(fromRank - rankStep, fromFile - fileStep)
            match targetPos with
            | Ok targetPos ->
                match capturablePieceAtPosition targetPos with
                | Some targetPiece -> Capture (targetPiece.Id, targetPos)
                | None -> ()
            | Error _ -> ()
        }

    let withdrawer (game : State) (fromPos : Position) (toPos : Position) =
        withdrawer' (opposingPieceAtPosition game) game fromPos toPos

    let chameleon (game : State) (fromPos : Position) (toPos : Position) = seq {
        //Chameleons capture a piece by using that piece's manner of capturing *and movement*.
        //This means that they can only capture Pawns orthogonally and Guards with a one-space move.
        //They can also capture Mages and Kings with a one-space move, despite not being able to mimic their movement.
        //Chameleons cannot capture Immobilizers or other Chameleons at all.

        let capturablePieceOfType pieceType =
            opposingPieceAtPosition game >> Option.filter (fun p -> p.Type = pieceType)

        let (dRank, dFile) = Pair.sub toPos.Coords fromPos.Coords
        let moveIsOrthogonal = dRank = 0 || dFile = 0
        let moveIsAdjacent = (abs dRank) <= 1 && (abs dFile) <= 1

        if moveIsAdjacent then
            let capturablePieceAtPosition =
                opposingPieceAtPosition game >> Option.filter (fun p -> [Guard; Mage; King] |> Seq.contains p.Type)
            yield! displacement' capturablePieceAtPosition game fromPos toPos
        if moveIsOrthogonal then yield! pawn' (capturablePieceOfType Pawn) game fromPos toPos
        yield! coordinator' (capturablePieceOfType Coordinator) game fromPos toPos
        yield! longLeaper' (capturablePieceOfType LongLeaper) game fromPos toPos
        yield! withdrawer' (capturablePieceOfType Withdrawer) game fromPos toPos
    }

/// Returns all the Capture MoveComponents
/// resulting from a move of the given piece ID between the given start and end positions
let private capturesForMove (game : State) (pieceId : string) (fromPos : Position) (toPos : Position) =
    match game.Pieces[pieceId].Type with
    | Pawn -> Capturing.pawn game fromPos toPos
    | Guard | Mage | King -> Capturing.displacement game fromPos toPos
    | LongLeaper -> Capturing.longLeaper game fromPos toPos
    | Coordinator -> Capturing.coordinator game fromPos toPos
    | Withdrawer -> Capturing.withdrawer game fromPos toPos
    | Chameleon -> Capturing.chameleon game fromPos toPos
    | Immobilizer -> Seq.empty

/// Applies a Move to a Maxima GameState and returns the resulting state
let applyMove (game : State) (move : Move) =
    let newState = GameState.applyMove game move

    //Check win conditions
    //Note that since check isn't currently implemented we need to check for the lose condition of scoring an "own goal"
    let opponent = Player.oppositeTo move.Player

    let kingCaptured =
        move.Components
        |> Seq.exists (function Capture (id, _) when game.Pieces[id].Type = King -> true | _ -> false)

    let (ownGoalRank, opponentGoalRank) =
        match move.Player with
        | White -> (0, 10)
        | Black -> (10, 0)
    let goalScored =
        newState.Board[opponentGoalRank]
        |> Array.forall (function
            | Some id -> newState.Pieces[id].Player = move.Player
            | None -> false
        )
    let ownGoalScored =
        newState.Board[ownGoalRank]
        |> Array.forall (function
            | Some id -> newState.Pieces[id].Player = move.Player
            | None -> false
        )

    let kingSoloed =
        newState
        |> GameState.findPieces Position.FromIndexes (fun (piece, _) -> piece.Player = opponent)
        |> Seq.length = 1

    let winState = {newState with Key = GameStateKey(); Winner = Some move.Player}
    let loseState = {newState with Key = GameStateKey(); Winner = Some opponent}

    //There is an order of priority to the win conditions.
    //Capturing the enemy King takes priority over everything else. You can score an own-goal to do it and still win.
    if kingCaptured then winState
    //Next, goals are checked. Order here doesn't matter since you can't land in both goals in one move.
    elif goalScored then winState
    elif ownGoalScored then loseState
    //Finally, soloing the enemy King is checked. Notably you *can't* score an own-goal to do this.
    elif kingSoloed then winState
    else newState

/// Given a starting Maxima GameState, makes a move between the given start and end positions
let makeMove (game : State) (fromPos : Position) (toPos : Position) =
    match GameState.pieceAtPosition game fromPos with
    | Some piece ->
        if piece.Player <> game.Player then failwith $"%A{game.Player} cannot move %A{piece.Player}'s piece"
    | None -> failwith $"There is no piece to move at position %A{fromPos}"

    if not (pieceCanMoveToSpace game fromPos toPos) then failwith $"Move from %A{fromPos} to %A{toPos} is invalid"

    let move = GameState.createMove game fromPos (fun pieceId -> [|
        Relocate (pieceId, fromPos, toPos)
        yield! capturesForMove game pieceId fromPos toPos
    |])
    MoveWithInputs (move, fromPos, toPos, [])

/// Given a starting Maxima GameState, performs a self-capture of an immobilized piece at the given position
/// and returns the resulting state
let selfCaptureImmobilizedPiece (game : State) (pos : Position) =
    match GameState.pieceAtPosition game pos with
    | Some piece ->
        if piece.Player <> game.Player then
            failwith $"%A{piece.Player} cannot self-capture piece on %A{game.Player}'s turn"
        if not (pieceIsImmobilized game pos) then
            failwith "Cannot self-capture piece that is not immobilized"

        GameState.createMove game pos (fun pieceId -> [|Capture (pieceId, pos)|])
        |> applyMove game
    | None -> failwith $"No piece at position %A{pos}"

let undoMove (game : State) = GameState.undoMove (startingState ()) game

let redoMove = GameState.redoMove
