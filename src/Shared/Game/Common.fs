[<AutoOpen>]
module UltimatelySafe.Shared.Game.Common

open System

open UltimatelySafe.Shared.Exceptions
open UltimatelySafe.Shared.Utils
open UltimatelySafe.Shared.Utils.ActivePatterns

/// Union of the available games
type GameType = Ultima | Rococo | Maxima

/// Union of the player types in a chess-like game
type Player = Black | White

module Player =
    /// Gets the opposite of the given Player
    let oppositeTo (player : Player) =
        match player with
        | White -> Black
        | Black -> White

/// An interface for piece types to conform to
type IPieceType =
    //Is there a way to have an interface satisfy the `equality` constraint?
    //The only way I found was <'This when 'This :> IPieceType<'This> and 'This : equality>
    //and that required repeating the equality constraint at each use site, which kind of defeats the point.
    //For now I'm just tacking on the extra constraint where it's needed and assuming it will always be met
    //(this is pretty much always going to be implemented by a union type anyway).

    /// The long-form name of the piece
    abstract member Name : string

    /// The standard abbreviation for the piece
    abstract member Abbreviation : string

/// Represents a piece on the game board
type Piece<'Type when 'Type :> IPieceType> =
    {
        /// A unique ID for this piece within the game
        Id : string

        /// A value indicating the type of this piece within the game.
        /// This is a generic property so that different game types can be typed independently.
        Type : 'Type

        /// The player to whom this piece belongs
        Player : Player
    }

/// A coordinate pair of rank/file on a game board
[<AbstractClass>]
type BoardPosition(rank : int, file : int) =
    /// The rank of the position, in its most natural form (not guaranteed to be a valid array index)
    member val Rank = rank

    /// The rank of the position, adjusted so it is guaranteed to be usable as an array index
    abstract member RankIndex : int
    default this.RankIndex = this.Rank

    /// The file of the position, in its most natural form (not guaranteed to be a valid array index)
    member val File = file

    /// The file of the position, adjusted so it is guaranteed to be usable as an array index
    abstract member FileIndex : int
    default this.FileIndex = this.File

    /// The coordinates of this position, in their most natural form (not guaranteed to be usable as array indexes)
    member this.Coords = (this.Rank, this.File)

    /// The rank and file of this position as array indexes
    member this.Indexes = (this.RankIndex, this.FileIndex)

    override this.ToString() = $"({this.Rank}, {this.File})"

    /// Copy-on-update method taking new natural coordinate values.
    /// Raises ArgumensInvalidtException if the given values are invalid/out-of-bounds.
    abstract member WithCoords : ?rank : int * ?file : int -> BoardPosition

    /// Copy-on-update method taking new natural coordinate values.
    /// Returns a Result instead of raising an exception on invalid/out-of-bounds values.
    member this.TryWithCoords(?rank : int, ?file : int) =
        try Ok (this.WithCoords(?rank = rank, ?file = file))
        with ArgumentsInvalidException message -> Error message

    /// Copy-on-update method taking new array index values.
    /// Raises ArgumentsInvalidException if the given values are invalid/out-of-bounds.
    abstract member WithIndexes : ?rank : int * ?file : int -> BoardPosition

    /// Copy-on-update method taking new array index values.
    /// Returns a Result instead of raising an exception on invalid/out-of-bounds values.
    member this.TryWithIndexes(?rank : int, ?file : int) =
        try Ok (this.WithIndexes(?rank = rank, ?file = file))
        with ArgumentsInvalidException message -> Error message

    override this.Equals(other : obj) = this.Coords = (unbox<BoardPosition> other).Coords

    override this.GetHashCode() = this.Coords.GetHashCode()

    //There's not really a direct need to compare positions, but it's necessary for Set membership
    interface IComparable<BoardPosition> with
        override this.CompareTo(other : BoardPosition) = compare this.Coords other.Coords

    interface IComparable with
        override this.CompareTo(other : obj) = (this :> IComparable<BoardPosition>).CompareTo(unbox other)

/// Destructures the Coords of a BoardPosition
let (|BoardPosition|) (pos : BoardPosition) = pos.Coords

/// Destructures the Indexes of a BoardPosition
let (|IndexedBoardPosition|) (pos : BoardPosition) = pos.Indexes

/// An action taken as part of a move
type MoveComponent =
    /// Relocates a piece from one space to another
    | Relocate of id : string * fromPos : BoardPosition * toPos : BoardPosition

    /// Captures a piece and removes it from play
    | Capture of id : string * fromPos : BoardPosition

    /// Removes a piece from play without saving it in the map of captured pieces. Useful for promotion.
    | Remove of id : string * fromPos : BoardPosition

    /// Revives a captured piece to the specified position
    | Revive of id : string * pos : BoardPosition

/// A move in a game
type Move =
    {
        /// The player making the move
        Player : Player

        /// The ID of the piece that is "moving"
        InitiatingPieceId : string

        /// The actions taken as part of the move
        Components : MoveComponent[]
    }

/// A Move, tupled with the inputs it resulted from. Useful for server-side move validation.
type MoveWithInputs =
    MoveWithInputs of move : Move * fromPos : BoardPosition * toPos : BoardPosition * additionalInputs : obj list

/// The result of moving a piece along the board.
/// Either a complete move, tupled with the original inputs,
/// or an incomplete move with a game-specific request for more information,
/// such as making a selection for pawn promotion.
type MovementResult<'Request> =
    | Complete of MoveWithInputs
    | Incomplete of 'Request

/// A 2D array of the board position. Elements contain the ID of the piece in that space.
/// This array is intentionally jagged, as some games use non-rectangular board shapes.
type Board = (string option)[][]

module Board =
    /// Returns true if the space at the given position on the board is empty
    let spaceIsEmpty (board : Board) (IndexedBoardPosition (rankIndex, fileIndex)) = board[rankIndex].[fileIndex].IsNone

    /// Returns true if the space at the given position on the board contains a piece
    let spaceIsOccupied (board : Board) (IndexedBoardPosition (rankIndex, fileIndex)) =
        board[rankIndex].[fileIndex].IsSome

/// Used to replace object identity for GameStates since SolidJS merges updates onto the original object.
/// Used as a cache key. Can't just use obj because, despite Fable emitting a JS "System.Object" class
/// (which records and unions extend), just writing obj() or System.Object() produces an empty object literal.
/// SolidJS only treats named classes atomically.
type GameStateKey() = class end

/// The current state of a game
type GameState<'PieceType when 'PieceType :> IPieceType> =
    {
        /// An object used as an identity for this state. Used as a cache key.
        /// THIS MUST BE REPLACED WITH A NEW KEY FOR EACH NEW STATE.
        /// FIXME: I don't like doing this, but since `member val` isn't allowed on records, IDK what else to do...
        Key : GameStateKey

        /// A map from piece IDs to their details
        Pieces : Map<string, Piece<'PieceType>>

        /// An association array of pieces that have been captured and are no longer on the board.
        /// Unlike the main Pieces map, where the most common operation is, by far, lookup,
        /// this collection is most frequently iterated. More importantly, we'd like to be able to maintain
        /// insertion order for display purposes, and F# Maps sort their entries by their keys.
        /// We could also use a Dictionary for this, but Dictionaries are very much built for mutability.
        /// F# arrays are also mutable, but it's easy to pretend that they aren't and that just felt better here.
        CapturedPieces : (string * Piece<'PieceType>)[]

        /// The current board position
        Board : Board

        /// An ordered array of the moves taken up to this point in the game
        Moves : Move[]

        /// The index at which the next move will be added to the array.
        /// This may be less than the length of the array if moves have been undone.
        NextMoveIndex : int

        /// The player whose turn it is to move
        Player : Player

        /// Set if a player has won the game
        Winner : Player option
    }

module GameState =
    /// Returns an empty state for use as a base for starting states.
    /// For some reason when I added the Key property, it forced me to wrap this in a function. I do not know why.
    let empty () = {
        Key = GameStateKey()
        Pieces = Map.empty
        CapturedPieces = [||]
        Board = [||]
        Moves = [||]
        NextMoveIndex = 0
        Player = White
        Winner = None
    }

    /// Returns the details of the piece at the given position on the board, if there is one
    let pieceAtPosition<'PieceType when 'PieceType :> IPieceType>
        (game : GameState<'PieceType>)
        (IndexedBoardPosition (rankIndex, fileIndex))
        =
        game.Board[rankIndex][fileIndex] |> Option.map (fun pieceId -> game.Pieces[pieceId])

    /// Finds all the pieces on the board that match a predicate (given the piece and its position)
    let findPieces<'PieceType, 'Pos when 'PieceType :> IPieceType and 'Pos :> BoardPosition>
        (makeIndexedPosition : int * int -> 'Pos)
        (predicate : Piece<'PieceType> * 'Pos -> bool)
        (game : GameState<'PieceType>)
        = seq {
            for (r, rank) in game.Board |> Seq.indexed do
                for (f, space) in rank |> Seq.indexed do
                    match space with
                    | Some pieceId ->
                        let pieceTuple = (game.Pieces[pieceId], makeIndexedPosition (r, f))
                        if predicate pieceTuple then pieceTuple
                    | None -> ()
        }

    /// Looks for a single piece on the board that matches a predicate (given the piece and its position).
    /// Returns Some if there is exactly one such piece, and None otherwise.
    let findPiece<'PieceType, 'Pos when 'PieceType :> IPieceType and 'Pos :> BoardPosition>
        (makeIndexedPosition : int * int -> 'Pos)
        (predicate : Piece<'PieceType> * BoardPosition -> bool)
        (game : GameState<'PieceType>)
        =
        game |> findPieces makeIndexedPosition predicate |> Seq.tryExactlyOne

    /// Finds all the pieces on the board of the given type
    let findPiecesOfType<
        'PieceType, 'Pos
            when 'PieceType :> IPieceType and 'PieceType : equality and 'Pos :> BoardPosition
    > (makeIndexedPosition : int * int -> 'Pos) (pieceType : 'PieceType) (game : GameState<'PieceType>) =
        game |> findPieces makeIndexedPosition (fun (piece, _) -> piece.Type = pieceType)

    /// Looks for a single piece on the board of the given type and owned by the given player.
    /// Returns Some if there is exactly one such piece, and None otherwise.
    let findPieceByPlayerAndType<
        'PieceType, 'Pos
            when 'PieceType :> IPieceType and 'PieceType : equality and 'Pos :> BoardPosition
    >
        (makeIndexedPosition : int * int -> 'Pos)
        (player : Player)
        (pieceType : 'PieceType)
        (game : GameState<'PieceType>)
        =
        game |> findPiece makeIndexedPosition (fun (piece, _) -> piece.Type = pieceType && piece.Player = player)

    /// Returns true if the space at the given position on the board is empty
    let spaceIsEmpty (game : GameState<_>) (pos : BoardPosition) = Board.spaceIsEmpty game.Board pos

    /// Returns true if the space at the given position on the board contains a piece
    let spaceIsOccupied (game : GameState<_>) (pos : BoardPosition) = Board.spaceIsOccupied game.Board pos

    /// Returns true if the space at the given position on the board contains a piece owned by the given player
    let spaceIsOccupiedByPlayer (game : GameState<_>) (player : Player) (pos : BoardPosition) =
        match pieceAtPosition game pos with
        | Some piece -> piece.Player = player
        | None -> false

    /// Convenience function for constructing a Move for given game and starting position.
    /// The callback function is passed the ID of the piece at the starting position
    /// and should return a seq of MoveComponents.
    let createMove<'PieceType when 'PieceType :> IPieceType>
        (game : GameState<'PieceType>)
        (IndexedBoardPosition (rankIndex, fileIndex) as pos)
        (makeComponents : string -> MoveComponent[])
        =
        let pieceId =
            game.Board[rankIndex][fileIndex]
            |> Option.defaultWith (fun () -> failwith $"No piece to move at position {pos}")
        {Player = game.Player; InitiatingPieceId = pieceId; Components = makeComponents pieceId}

    /// Applies a Move to a given GameState and returns the resulting state
    let applyMove<'PieceType when 'PieceType :> IPieceType> (game : GameState<'PieceType>) (move : Move) =
        if move.Player <> game.Player then failwith $"It is not %A{move.Player}'s turn to move"

        let removePiece id (IndexedBoardPosition (rankIndex, fileIndex)) g =
            {g with
                Key = GameStateKey()
                Pieces = g.Pieces |> Map.remove id
                Board =
                    match g.Board[rankIndex][fileIndex] with
                    | Some (Equals id) ->
                        g.Board |> Array.updateAt rankIndex (g.Board[rankIndex] |> Array.updateAt fileIndex None)
                    | _ -> g.Board
            }

        (
            {game with
                Key = GameStateKey()
                Moves = [|yield! game.Moves |> Seq.take game.NextMoveIndex; move|]
                NextMoveIndex = game.NextMoveIndex + 1
                Player = Player.oppositeTo game.Player
            },
            move.Components
        ) ||> Seq.fold (fun g mc ->
            match mc with
            | Relocate (
                  id,
                  IndexedBoardPosition (fromRankIndex, fromFileIndex),
                  IndexedBoardPosition (toRankIndex, toFileIndex)
              ) ->
                let board = g.Board |> Array.copy

                //Only null out the from space if it contains the ID being moved.
                //This avoids breaking moves that swap two pieces.
                match board[fromRankIndex][fromFileIndex] with
                | Some (Equals id) -> board[fromRankIndex] <- board[fromRankIndex] |> Array.updateAt fromFileIndex None
                | _ -> ()

                board[toRankIndex] <- board[toRankIndex] |> Array.updateAt toFileIndex (Some id)

                {g with Key = GameStateKey(); Board = board}
            | Capture (id, fromPos) ->
                let capturedPiece = g.Pieces[id]
                {(g |> removePiece id fromPos) with
                    Key = GameStateKey()
                    CapturedPieces = [|yield! g.CapturedPieces; (id, capturedPiece)|]
                }
            | Remove (id, fromPos) -> g |> removePiece id fromPos
            | Revive (id, IndexedBoardPosition (rankIndex, fileIndex)) ->
                let revivedPiece = g.CapturedPieces |> AssocArr.get id
                {g with
                    Key = GameStateKey()
                    Pieces = g.Pieces |> Map.add id revivedPiece
                    CapturedPieces = g.CapturedPieces |> AssocArr.remove id
                    Board =
                        g.Board |> Array.updateAt rankIndex (g.Board[rankIndex] |> Array.updateAt fileIndex (Some id))
                }
        )

    /// Undoes a move by replaying all but the most recent move onto a given initial state
    let undoMove<'PieceType when 'PieceType :> IPieceType>
        (initialState : GameState<'PieceType>)
        (game : GameState<'PieceType>)
        =
        //I'll be nice: if there are no moves to undo, just return without raising an error
        if game.Moves |> Array.isEmpty then game else

        let numMovesToApply = game.NextMoveIndex - 1
        let replayedState = (initialState, game.Moves |> Seq.take numMovesToApply) ||> Seq.fold applyMove
        {replayedState with Key = GameStateKey(); Moves = game.Moves}

    /// Re-applies one previously undone move in the given game
    let redoMove<'PieceType when 'PieceType :> IPieceType> (game : GameState<'PieceType>) =
        if game.NextMoveIndex = game.Moves.Length then game else

        let advancedState = applyMove game game.Moves[game.NextMoveIndex]
        {advancedState with Key = GameStateKey(); Moves = game.Moves}

/// Convenience function for making a starting set of pieces for a game.
/// Takes a list of piece counts and produces a full Map with pieces for both players.
let internal makePieces<'PieceType when 'PieceType :> IPieceType> (counts : ('PieceType * int) list) =
    Map [
        for player in [Black; White] do
            let playerId = (sprintf "%A" player)[0]
            for (pieceType, count) in counts do
                for i in 1 .. count do
                    let pieceId = $"{playerId}{pieceType.Abbreviation}{i}"
                    (pieceId, {Id = pieceId; Type = pieceType; Player = player})
    ]

/// Convenience function for making a starting board state.
/// The board is stored in a 2D array indexed (rank, file) so that A1 = (0, 0).
/// This makes indexing intuitive but results in a literal representation of the array looking "upside-down".
/// This function:<br/>
/// * Reverses the outer array to let you write a natural-looking image (from White's perspective)<br/>
/// * Maps the empty string to None so you don't have to type out options<br/>
/// * Automatically appends indexes to piece IDs
let internal makeBoard (init : string list list) =
    let mutable indexes = Map.empty<string, int>
    init
    |> Seq.rev
    |> Seq.map (fun row ->
        row
        |> Seq.map (fun idOrEmpty ->
            match idOrEmpty with
            | "" -> None
            | id ->
                let index = indexes |> Map.tryFind id |> Option.defaultValue 1
                indexes <- indexes |> Map.add id (index + 1)
                Some $"{id}{index}"
        )
        |> Seq.toArray
    )
    |> Seq.toArray
