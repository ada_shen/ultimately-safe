[<RequireQualifiedAccess>]
module UltimatelySafe.Shared.Game.Ultima

open System.Collections.Generic

open UltimatelySafe.Shared.Exceptions
open UltimatelySafe.Shared.Game.Common
open UltimatelySafe.Shared.Utils
open UltimatelySafe.Shared.Utils.ActivePatterns
open UltimatelySafe.Shared.Utils.Dictionaries
open UltimatelySafe.Shared.Utils.Tuples

/// The set of pieces used in Ultima.
/// For an explanation of Ultima pieces and how they capture, see:
/// https://www.chessvariants.com/other.dir/ultimapieces.html
type PieceType =
    | King
    | Withdrawer
    | Chameleon
    | LongLeaper
    | Coordinator
    | Immobilizer
    | Pawn

    interface IPieceType with
        override this.Name =
            match this with
            | LongLeaper -> "Long Leaper"
            | _ -> sprintf "%A" this

        override this.Abbreviation =
            match this with
            | King -> "K"
            | Withdrawer -> "W"
            | Chameleon -> "X"
            | LongLeaper -> "L"
            | Coordinator -> "C"
            | Immobilizer -> "I"
            | Pawn -> "P"

/// The size of the (square) Ultima game board
let boardSize = 8
let private lastIndex = boardSize - 1

/// A position on the Ultima game board
type Position(rank : int, file : int) =
    inherit BoardPosition(rank, file)

    do Position.ValidateCoords rank file

    static member ValidateCoords (rank : int) (file : int) =
        if rank < 0 || rank >= boardSize then
            argInvalid(nameof rank, $"{rank} is an invalid Ultima rank value. Must be within [0, {boardSize}].")

        if file < 0 || file >= boardSize then
            argInvalid(nameof file, $"{file} is an invalid Ultima file value. Must be within [0, {boardSize}].")

    /// Attempts to construct an Ultima Position with the given coords. Returns Error if the coords are invalid.
    static member TryCreate(rank : int, file : int) =
        try Ok (Position(rank, file))
        with ArgumentsInvalidException message -> Error message

    member this.WithCoords'(?rank : int, ?file : int) = Position(defaultArg rank this.Rank, defaultArg file this.File)

    override this.WithCoords(?rank : int, ?file : int) = this.WithCoords'(?rank = rank, ?file = file)

    override this.WithIndexes(?rank : int, ?file : int) = Position(defaultArg rank this.Rank, defaultArg file this.File)

/// Destructures the Coords of an Ultima Position
let (|Position|) (pos : Position) = pos.Coords

type State = GameState<PieceType>

/// Returns the initial state for a new Ultima game
let startingState () =
    {GameState.empty () with
        Key = GameStateKey()
        Pieces = makePieces [
            (King, 1)
            (Withdrawer, 1)
            (Chameleon, 2)
            (LongLeaper, 2)
            (Coordinator, 1)
            (Immobilizer, 1)
            (Pawn, 8)
        ]
        Board = makeBoard [
            ["BC"; "BL"; "BX"; "BW"; "BK"; "BX"; "BL"; "BI"]
            [for _ in 1 .. 8 -> "BP"]
            for _ in 1 .. 4 do [for _ in 1 .. 8 -> ""]
            [for _ in 1 .. 8 -> "WP"]
            ["WI"; "WL"; "WX"; "WK"; "WW"; "WX"; "WL"; "WC"]
        ]
    }

/// Given a position on an Ultima board, returns all positions adjacent to it
let adjacentSpaces (Position (rank, file)) = seq {
    for r in max 0 (rank - 1) .. min lastIndex (rank + 1) do
        for f in max 0 (file - 1) .. min lastIndex (file + 1) do
            if r <> rank || f <> file then Position(r, f)
}

/// Given a position on an Ultima board, returns all pieces in adjacent occupied spaces
let adjacentPieces (game : State) (pos : Position) = seq {
    for Position (r, f) in adjacentSpaces pos do
        match game.Board[r][f] with
        | Some piece -> game.Pieces[piece]
        | None -> ()
}

/// Returns true if an Immobilizer (or a Chameleon acting as an Immobilizer) owned by the given player
/// is adjacent to the given position
let immobilizerAdjacent (game : State) (player : Player) (position : Position) =
    position
    |> adjacentPieces game
    |> Seq.exists (function
        | {Type = Immobilizer; Player = Equals player} -> true
        | {Type = Chameleon; Player = Equals player} ->
            match GameState.pieceAtPosition game position with
            | Some {Type = Immobilizer} -> true
            | _ -> false
        | _ -> false
    )

module private Movement =
    let private plus' (filterRay : Position seq -> Position seq) (Position (rank, file)) = seq {
        //White's Up
        yield! seq {for r in rank + 1 .. lastIndex -> Position(r, file)} |> filterRay

        //Down
        yield! seq {for r in rank - 1 .. -1 .. 0 -> Position(r, file)} |> filterRay

        //Right
        yield! seq {for f in file + 1 .. lastIndex -> Position(rank, f)} |> filterRay

        //Left
        yield! seq {for f in file - 1 .. -1 .. 0 -> Position(rank, f)} |> filterRay
    }

    let plus (game : State) (fromPos : Position) = plus' (Seq.takeWhile (GameState.spaceIsEmpty game)) fromPos

    let private queen' (filterRay : Position seq -> Position seq) (Position (rank, file) as pos) = seq {
        yield! plus' filterRay pos

        //White's Up-left
        yield! seq {for d in 1 .. min (lastIndex - rank) file -> Position(rank + d, file - d)}
        |> filterRay

        //Up-right
        yield! seq {for d in 1 .. min (lastIndex - rank) (lastIndex - file) -> Position(rank + d, file + d)}
        |> filterRay

        //Down-left
        yield! seq {for d in 1 .. min rank file -> Position(rank - d, file - d)}
        |> filterRay

        //Down-right
        yield! seq {for d in 1 .. min rank (lastIndex - file) -> Position(rank - d, file + d)}
        |> filterRay
    }

    let queen (game : State) (fromPos : Position) =
        queen' (Seq.takeWhile (GameState.spaceIsEmpty game)) fromPos

    let king (game : State) (player : Player) (fromPos : Position) =
        fromPos
        |> adjacentSpaces
        |> Seq.filter (not << GameState.spaceIsOccupiedByPlayer game player)

    let longLeaper (game : State) (player : Player) (fromPos : Position) =
        fromPos
        |> queen' (fun ray ->
            //Long Leapers cannot jump over friendly pieces or two adjacent pieces.
            //Dump the ray seq to an array so we can have lookahead when filtering.
            let array = ray |> Seq.toArray
            array
            |> Seq.indexed
            |> Seq.takeWhile (fun (i, pos) ->
                let spaceIsOccupiedByCurrentPlayer = pos |> GameState.spaceIsOccupiedByPlayer game player
                let spaceIsOccupied = pos |> GameState.spaceIsOccupied game
                let nextSpaceIsOccupied = i >= array.Length - 1 || array[i + 1] |> GameState.spaceIsOccupied game
                not (spaceIsOccupiedByCurrentPlayer || spaceIsOccupied && nextSpaceIsOccupied)
            )
            |> Seq.map snd
            |> Seq.filter (GameState.spaceIsEmpty game)
        )

    let chameleon (game : State) (player : Player) (fromPos : Position) =
        fromPos
        |> queen' (fun ray ->
            //Chameleons can leap over enemy Long Leapers, as long as there's an empty space behind them.
            //They can advance into further empty spaces, but cannot leap over non-Long Leapers.
            //They can also capture Kings by displacement, but only from one space away.
            let array = ray |> Seq.toArray
            array
            |> Seq.indexed
            |> Seq.takeWhile (fun (i, pos) ->
                let spaceIsEmpty = pos |> GameState.spaceIsEmpty game
                let nextSpaceIsEmpty = i < array.Length - 1 && array[i + 1] |> GameState.spaceIsEmpty game
                let (spaceHasEnemyLongLeaper, spaceHasEnemyKing) =
                    match GameState.pieceAtPosition game pos with
                    | Some {Type = LongLeaper; Player = Equals (Player.oppositeTo player)} -> (true, false)
                    | Some {Type = King; Player = Equals (Player.oppositeTo player)} -> (false, true)
                    | _ -> (false, false)
                let spaceIsOneSpaceAway = i = 0
                let lastSpaceHasEnemyKing =
                    i = 1 //You can only capture a King from one space away, so we only need to check this here
                        && match GameState.pieceAtPosition game array[0] with
                           | Some {Type = King; Player = Equals (Player.oppositeTo player)} -> true
                           | _ -> false
                not lastSpaceHasEnemyKing //We can land on a King but not jump over it
                    && (spaceIsEmpty
                        || spaceHasEnemyLongLeaper && nextSpaceIsEmpty
                        || spaceHasEnemyKing && spaceIsOneSpaceAway)
            )
            |> Seq.map snd
            |> Seq.filter (fun pos ->
                let spaceHasEnemyKing =
                    match GameState.pieceAtPosition game pos with
                    | Some {Type = King; Player = Equals (Player.oppositeTo player)} -> true
                    | _ -> false
                GameState.spaceIsEmpty game pos || spaceHasEnemyKing
            )
        )

let private movementCache = IsomorphicWeakMap<GameStateKey, Dictionary<BoardPosition, Set<Position>>>()

/// Given an Ultima game state and a starting position, returns the set of all spaces to which that piece can move
let pieceMovement (game : State) (fromPos : Position) =
    let gameMovementCache = movementCache.GetOrCompute game.Key Dictionary
    gameMovementCache.GetOrCompute(fromPos, fun () ->
        match GameState.pieceAtPosition game fromPos with
        | Some piece ->
            match piece.Type with
            | King -> Set (Movement.king game piece.Player fromPos)
            | Pawn -> Set (Movement.plus game fromPos)
            | LongLeaper -> Set (Movement.longLeaper game piece.Player fromPos)
            | Chameleon -> Set (Movement.chameleon game piece.Player fromPos)
            | _ -> Set (Movement.queen game fromPos)
        | None -> Set.empty
    )

/// Returns true if a move is legal between the given start and end positions in the given game state
let pieceCanMoveToSpace (game : State) (fromPos : Position) (toPos : Position) =
    match GameState.pieceAtPosition game fromPos with
    | Some piece ->
        not (immobilizerAdjacent game (Player.oppositeTo piece.Player) fromPos)
            && pieceMovement game fromPos |> Set.contains toPos
    | None -> false

module private Capturing =
    let private opposingPieceAtPosition (game : State) (pos : Position) =
        GameState.pieceAtPosition game pos
        |> Option.filter (fun p -> p.Player = Player.oppositeTo game.Player)

    //For an explanation of Ultima pieces and how they capture, see:
    //https://www.chessvariants.com/other.dir/ultimapieces.html

    let king'
        (capturablePieceAtPosition : Position -> Piece<PieceType> option)
        (_game : State)
        (_fromPos : Position)
        (toPos : Position)
        = seq {
            //The king is unchanged from normal Chess and captures by displacement
            match capturablePieceAtPosition toPos with
            | Some targetPiece -> Capture (targetPiece.Id, toPos)
            | None -> ()
        }

    let king (game : State) (fromPos : Position) (toPos : Position) =
        king' (opposingPieceAtPosition game) game fromPos toPos

    let pawn'
        (capturablePieceAtPosition : Position -> Piece<PieceType> option)
        (game : State)
        (_fromPos : Position)
        (toPos : Position)
        = seq {
            //Pawns capture by "pinching":
            //An enemy piece is captured if the pawn ends its move
            //with that piece sandwiched orthogonally between it and another allied piece.
            //The "pinching" does not need to be in the direction of movement.
            for delta in [(-1, 0); (1, 0); (0, -1); (0, 1)] do
                let targetPos = Pair.add toPos.Coords delta |> Position.TryCreate
                let afterTargetPos =
                    targetPos
                    |> Result.bind (fun pos -> Pair.add pos.Coords delta |> Position.TryCreate)
                match (targetPos, afterTargetPos) with
                | (Ok targetPos, Ok afterTargetPos) ->
                    match (capturablePieceAtPosition targetPos, GameState.pieceAtPosition game afterTargetPos) with
                    | (Some targetPiece, Some {Player = Equals game.Player}) -> Capture (targetPiece.Id, targetPos)
                    | _ -> ()
                | _ -> ()
        }

    let pawn (game : State) (fromPos : Position) (toPos : Position) =
        pawn' (opposingPieceAtPosition game) game fromPos toPos

    let coordinator'
        (capturablePieceAtPosition : Position -> Piece<PieceType> option)
        (game : State)
        (_fromPos : Position)
        (toPos : Position)
        = seq {
            //Coordinators capture by "coordinating" with their King:
            //After a Coordinator moves, it captures any enemy piece in its rank and the King's file,
            //or vice-versa

            let (Position (kingRank, kingFile)) =
                game
                |> GameState.findPieceByPlayerAndType Position game.Player King
                |> Option.get
                |> snd

            let targetA = toPos.WithCoords'(file = kingFile)
            match capturablePieceAtPosition targetA with
            | Some targetPiece -> Capture (targetPiece.Id, targetA)
            | None -> ()

            let targetB = toPos.WithCoords'(rank = kingRank)
            match capturablePieceAtPosition targetB with
            | Some targetPiece -> Capture (targetPiece.Id, targetB)
            | None -> ()
        }

    let coordinator (game : State) (fromPos : Position) (toPos : Position) =
        coordinator' (opposingPieceAtPosition game) game fromPos toPos

    let longLeaper'
        (capturablePieceAtPosition : Position -> Piece<PieceType> option)
        (_game : State)
        (Position (fromRank, fromFile) as fromPos)
        (toPos : Position)
        = seq {
            //The Long Leaper captures by jumping over enemy pieces, similar to in Checkers.
            //It can make multiple hops, but cannot make a turn (can only capture in a straight line).
            let delta = Pair.sub toPos.Coords fromPos.Coords
            //Assuming straight-line movement, so the abs of both deltas is either equal or zero
            let distance = delta |> Pair.map abs ||> max
            let (rankStep, fileStep) = delta |> Pair.map sign
            for n in 1 .. distance - 1 do
                let targetPos = Position(fromRank + rankStep * n, fromFile + fileStep * n)
                match capturablePieceAtPosition targetPos with
                | Some targetPiece -> Capture (targetPiece.Id, targetPos)
                | None -> ()
        }

    let longLeaper (game : State) (fromPos : Position) (toPos : Position) =
        longLeaper' (opposingPieceAtPosition game) game fromPos toPos

    let withdrawer'
        (capturablePieceAtPosition : Position -> Piece<PieceType> option)
        (_game : State)
        (Position (fromRank, fromFile) as fromPos)
        (toPos : Position)
        = seq {
            //The Withdrawer captures a piece by moving directly away from it
            let (rankStep, fileStep) = Pair.sub toPos.Coords fromPos.Coords |> Pair.map sign
            let targetPos = Position.TryCreate(fromRank - rankStep, fromFile - fileStep)
            match targetPos with
            | Ok targetPos ->
                match capturablePieceAtPosition targetPos with
                | Some targetPiece -> Capture (targetPiece.Id, targetPos)
                | None -> ()
            | Error _ -> ()
        }

    let withdrawer (game : State) (fromPos : Position) (toPos : Position) =
        withdrawer' (opposingPieceAtPosition game) game fromPos toPos

    let chameleon (game : State) (fromPos : Position) (toPos : Position) = seq {
        //Chameleons capture a piece by using that piece's manner of capturing *and movement*.
        //This means that they can only capture kings with a one-space move, pawns orthogonally,
        //and cannot capture Immobilizers or other Chameleons at all.

        let capturablePieceOfType pieceType =
            opposingPieceAtPosition game >> Option.filter (fun p -> p.Type = pieceType)

        let (dRank, dFile) = Pair.sub toPos.Coords fromPos.Coords
        let moveIsOrthogonal = dRank = 0 || dFile = 0
        let moveIsAdjacent = (abs dRank) <= 1 && (abs dFile) <= 1

        if moveIsAdjacent then yield! king' (capturablePieceOfType King) game fromPos toPos
        if moveIsOrthogonal then yield! pawn' (capturablePieceOfType Pawn) game fromPos toPos
        yield! coordinator' (capturablePieceOfType Coordinator) game fromPos toPos
        yield! longLeaper' (capturablePieceOfType LongLeaper) game fromPos toPos
        yield! withdrawer' (capturablePieceOfType Withdrawer) game fromPos toPos
    }

/// Returns all the Capture MoveComponents
/// resulting from a move of the given piece ID between the given start and end positions
let private capturesForMove (game : State) (pieceId : string) (fromPos : Position) (toPos : Position) =
    match game.Pieces[pieceId].Type with
    | King -> Capturing.king game fromPos toPos
    | Pawn -> Capturing.pawn game fromPos toPos
    | Coordinator -> Capturing.coordinator game fromPos toPos
    | LongLeaper -> Capturing.longLeaper game fromPos toPos
    | Withdrawer -> Capturing.withdrawer game fromPos toPos
    | Chameleon -> Capturing.chameleon game fromPos toPos
    | Immobilizer -> Seq.empty

/// Applies a Move to an Ultima GameState and returns the resulting state
let applyMove (game : State) (move : Move) =
    let newState = GameState.applyMove game move

    //Check win condition
    let kingCaptured =
        move.Components
        |> Seq.exists (function Capture (id, _) when game.Pieces[id].Type = King -> true | _ -> false)
    if kingCaptured then {newState with Key = GameStateKey(); Winner = Some move.Player} else newState

/// Given a starting Ultima GameState, makes a move between the given start and end positions
let makeMove (game : State) (fromPos : Position) (toPos : Position) =
    match GameState.pieceAtPosition game fromPos with
    | Some piece ->
        if piece.Player <> game.Player then failwith $"%A{game.Player} cannot move %A{piece.Player}'s piece"
    | None -> failwith $"There is no piece to move at position %A{fromPos}"

    if not (pieceCanMoveToSpace game fromPos toPos) then failwith $"Move from %A{fromPos} to %A{toPos} is invalid"

    let move = GameState.createMove game fromPos (fun pieceId -> [|
        Relocate (pieceId, fromPos, toPos)
        yield! capturesForMove game pieceId fromPos toPos
    |])
    MoveWithInputs (move, fromPos, toPos, [])

/// Given a starting Ultima GameState, performs a self-capture of an immobilized piece at the given position
/// and returns the resulting state
let selfCaptureImmobilizedPiece (game : State) (pos : Position) =
    match GameState.pieceAtPosition game pos with
    | Some piece ->
        if piece.Player <> game.Player then
            failwith $"%A{piece.Player} cannot self-capture piece on %A{game.Player}'s turn"
        if not (immobilizerAdjacent game (Player.oppositeTo piece.Player) pos) then
            failwith "Cannot self-capture piece that is not immobilized"

        GameState.createMove game pos (fun pieceId -> [|Capture (pieceId, pos)|])
        |> applyMove game
    | None -> failwith $"No piece at position %A{pos}"

let undoMove (game : State) = GameState.undoMove (startingState ()) game

let redoMove = GameState.redoMove
