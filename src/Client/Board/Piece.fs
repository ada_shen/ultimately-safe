module UltimatelySafe.Board.Piece

open Fable.Core
open Fable.Core.JsInterop
open Feliz.JSX.Solid

open UltimatelySafe.ClientUtils.JsTypes
open UltimatelySafe.Components.Ext.SolidDnd
open UltimatelySafe.Shared.Game

let private classNames : CssModules.Board.Piece = importDefault "./Piece.module.scss"

[<AutoOpen>]
type BoardPiece =
    /// <summary>
    ///     <para>
    ///         Renders a piece on a game board.
    ///         Handles drag functionality.
    ///     </para>
    ///     <para>
    ///         Exposes CSS classes under <c>UsBoardPiece</c>:<br/>
    ///         * <c>draggable</c> - Applied to the <c>root</c> element when <paramref name="draggable"/> is true
    ///     </para>
    ///     <para>
    ///         Accepts the following CSS variables:<br/>
    ///         * <c>--piece-scale</c> - Multiplier for the size of the piece
    ///     </para>
    /// </summary>
    /// <param name="piece">The piece to render</param>
    /// <param name="draggable">Whether the player should be able to click and drag the piece</param>
    /// <param name="position">The coordinates of this piece on the board</param>
    /// <param name="renderPieceGraphic">
    ///     Function for getting the graphic to display for a piece.
    ///     Defaults to displaying the piece's standard abbreviation as text.
    /// </param>
    [<JSX.Component>]
    static member BoardPiece<'Type when 'Type :> IPieceType>(
        piece : Piece<'Type>,
        draggable : bool,
        renderPieceGraphic : Player -> 'Type -> JSX.Element,
        position : BoardPosition
    ) =
        //Rename to avoid conflict with the directive below
        let isDraggable() = draggable

        let draggable = createDraggable(!^piece.Id, Record [
            ("pieceType", box piece.Type)
            ("player", box piece.Player)
            ("position", box position)
        ])

        Html.div [
            Attr.title piece.Type.Name
            Attr.classList [
                "UsBoardPiece-root"
                classNames.piece
                match piece.Player with
                | White -> classNames.white
                | Black -> classNames.black
                if isDraggable() then "UsBoardPiece-draggable"
            ]
            Html.children [
                //There's no API to disable dragging, so we use pointer-events: none to disable it.
                //We still want the piece name tooltip to display though, so use an inner div for the drag handle.
                Html.div [
                    //The Feliz API doesn't support directives as far as I can tell,
                    //but the Solid docs say directives are basically a sugar over refs,
                    //so I imagine this should work fine
                    Solid.ref (fun el -> draggable.Invoke(el))
                    Attr.style [
                        Css.width (length.perc 100)
                        Css.height (length.perc 100)
                        //Surprisingly pointerEvents is not in the CSS typings...
                        if not (isDraggable()) then ("pointer-events", "none")
                    ]
                    Attr.classList [if isDraggable() then classNames.draggable]
                    Html.children [
                        //Wrap the graphic in another div so we can apply a CSS transform
                        //without it getting clobbered by the drag translation
                        Html.div [
                            Attr.className classNames.graphic
                            Html.children [
                                renderPieceGraphic piece.Player piece.Type
                            ]
                        ]
                    ]
                ]
            ]
        ]

    /// <summary>An alternative piece component that can be used for non-interactive purposes</summary>
    /// <param name="player">The player to render a piece for</param>
    /// <param name="pieceType">The type of piece to render</param>
    /// <param name="renderPieceGraphic">A function returning the piece graphic (image or placeholder text)</param>
    [<JSX.Component>]
    static member DisplayPiece<'Type when 'Type :> IPieceType>(
        player : Player,
        pieceType : 'Type,
        renderPieceGraphic : Player -> 'Type -> JSX.Element
    ) =
        Html.div [
            Attr.title pieceType.Name
            Attr.classList [
                "UsDisplayPiece-root"
                classNames.piece
                match player with
                | White -> classNames.white
                | Black -> classNames.black
            ]
            Html.children [
                Html.div [
                    Attr.className classNames.graphic
                    Html.children [
                        renderPieceGraphic player pieceType
                    ]
                ]
            ]
        ]
