module UltimatelySafe.Board.Space

open Browser.Types
open Fable.Core
open Fable.Core.JS
open Fable.Core.JsInterop
open Feliz.JSX.Solid

open UltimatelySafe.ClientUtils.JsTypes
open UltimatelySafe.Components.Ext.SolidDnd
open UltimatelySafe.Shared.Game

let private classNames : CssModules.Board.Space = importDefault "./Space.module.scss"

[<AutoOpen>]
type Space =
    /// <summary>
    ///     <para>
    ///         Renders a single space on a game board.
    ///         Handles low-level drag-and-drop functionality but possesses minimal state.
    ///     </para>
    ///     <para>
    ///         Exposes CSS classes under <c>UsBoardSpace</c>:<br/>
    ///         * <c>clickable</c> - Applied to the <c>root</c> element when <paramref name="clickable"/> is true<br/>
    ///         * <c>droppable</c> - Applied to the <c>root</c> element when <paramref name="droppable"/> is true<br/>
    ///         * <c>activeDroppable</c> - Applied to the <c>root</c> element when <paramref name="droppable"/> is true
    ///           and a draggable element is being hovered over the space<br/>
    ///         * <c>content</c> - The container used to center the space's children
    ///     </para>
    ///     <para>
    ///         Accepts the following CSS variables:<br/>
    ///         * <c>--space-color</c> - Sets the background color of the space
    ///     </para>
    /// </summary>
    /// <param name="id">The ID of this space on the board. E.g., "A1"</param>
    /// <param name="position">The coordinates of this space on the board</param>
    /// <param name="clickable">Sets whether the space can be clicked</param>
    /// <param name="droppable">
    ///     Sets whether the space can accept a dropped piece.
    ///     NOTE: This is purely cosmetic,
    ///     as accepting a drop is the responsibility of the parent of the <c>DragDropProvider</c>.
    /// </param>
    /// <param name="children">The contents to render in the space. Will be centered by default.</param>
    /// <param name="classList">Additional CSS classes to apply to the component</param>
    /// <param name="color">Dynamically sets the value of the <c>--space-color</c> CSS variable</param>
    /// <param name="onClick">
    ///     Event handler called when the space is clicked.
    ///     Only called when <paramref name="clickable"/> is true.
    ///     Called with the space's position and the event object.
    ///     NOTE: This is implemented using the "pointerup" event.
    /// </param>
    [<JSX.Component>]
    static member Space<'Position when 'Position :> BoardPosition>(
        id : string,
        position : 'Position,
        clickable : bool,
        droppable : bool,
        ?children : JSX.Element,
        ?classList : string list,
        ?color : string,
        ?onClick : 'Position -> PointerEvent -> unit
    ) =
        //Alias to prevent conflict with directive
        let isDroppable() = droppable

        let droppable = createDroppable(!^id, Record [("position", position)])

        //We want to support clicking on spaces in addition to drag-and-drop, but there's a problem:
        //solid-dnd uses a CSS transform to drag elements.
        //This means that the cursor never technically leaves the parent.
        //As a result, even the longest drag results in a "click" event when you release it, which is not what we want.
        //So we need to register separate pointerUp/Down events and use both a timeout and a coordinate check.
        //to see if we should count a "click".
        let mutable clicking = false
        let mutable clickTimeout : int option = None

        let onPointerDown (ev : PointerEvent) =
            match clickTimeout with
            | Some id -> clearTimeout id
            | None -> ()

            //If the click is directly on the space and not its contents, then the other checks aren't necessary
            if clickable && ev.target <> ev.currentTarget then
                clicking <- true
                clickTimeout <- Some (setTimeout (fun () ->
                    clicking <- false
                    clickTimeout <- None
                ) 250)

        let onPointerUp (ev : PointerEvent) =
            let directClick = ev.target = ev.currentTarget
            let pointerInSpace =
                let targetRect = (ev.currentTarget :?> HTMLDivElement).getBoundingClientRect()
                ev.clientX >= targetRect.left
                    && ev.clientX < targetRect.right
                    && ev.clientY >= targetRect.top
                    && ev.clientY < targetRect.bottom
            if clickable && (directClick || clicking && pointerInSpace) then
                match onClick with
                | Some handler -> handler position ev
                | None -> ()

            clicking <- false

        Html.div [
            Solid.ref (fun el -> droppable.Invoke(el))
            Attr.classList ((defaultArg classList []) @ [
                "UsBoardSpace-root"
                classNames.space
                if clickable then "UsBoardSpace-clickable"
                if isDroppable() then
                    "UsBoardSpace-droppable"
                    if droppable.isActiveDroppable then "UsBoardSpace-activeDroppable"
            ])
            Attr.style [
                match color with
                | Some color -> ("--space-color", color)
                | None -> ()
            ]
            Ev.onPointerDown onPointerDown
            Ev.onPointerUp onPointerUp
            Html.children [
                defaultArg children Html.none
            ]
        ]
