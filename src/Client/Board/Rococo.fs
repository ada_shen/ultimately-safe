module UltimatelySafe.Board.Rococo

open Browser.Types
open Fable.Core
open Fable.Core.JsInterop
open Feliz.JSX.Solid

open UltimatelySafe.Board.Base
open UltimatelySafe.Board.Pieces
open UltimatelySafe.ClientUtils.Focusable
open UltimatelySafe.ClientUtils.SolidExtensions
open UltimatelySafe.Shared.Game
open UltimatelySafe.Shared.Utils.ActivePatterns

let private classNames : CssModules.Board.Rococo = importDefault "./Rococo.module.scss"

type private SpecialMoveType = MutualCapture

let private baseRanks = [|
    for i in 0 .. Rococo.boardSize - 2 do (i, string i)
    (Rococo.boardSize - 1, "00")
|]
let private baseFiles = [|
    (0, "X")
    for i in 1 .. Rococo.boardSize - 2 do (i, string (char (int 'A' + i - 1)))
    (Rococo.boardSize - 1, "Y")
|]

[<AutoOpen>]
type RococoBoard =
    [<JSX.Component>]
    static member RococoBoard(
        gameState : Rococo.State,
        makeMove : Rococo.Position -> Rococo.Position -> MovementResult<Rococo.InputRequest>,
        applyMove: MoveWithInputs -> unit,
        selfCaptureImmobilizedPiece : Rococo.Position -> unit,
        mutualCapture : Rococo.Position -> Rococo.Position -> unit,
        requestCapturedPiece : (Rococo.PieceType * int -> bool) -> (Rococo.PieceType option -> unit) -> unit,
        perspective : Player,
        singlePlayer : bool,
        ?hideBorder : bool,
        ?classList : string list,
        ?freeMovement : bool,
        ?focusRef : Focusable ref,
        ?onSpaceFocused : Rococo.Position -> Piece<Rococo.PieceType> option -> unit
    ) =
        let shouldHideBorder() = defaultArg hideBorder false
        let freeMovementMode() = defaultArg freeMovement false

        let pieceCanMoveToSpace fromPos toPos =
            if freeMovementMode() then GameState.spaceIsEmpty gameState toPos
            else Rococo.pieceCanMoveToSpace gameState fromPos toPos

        let board = Solid.makeRef<BoardHandle<Rococo.PieceType, Rococo.Position, SpecialMoveType>>()

        match focusRef with
        | Some focusRef ->
            focusRef.Value <- {new Focusable with override _.Focus () = board.Value.Focus ()}
        | None -> ()

        let ranks() =
            match perspective with
            | White -> baseRanks |> Array.rev
            | Black -> baseRanks

        let shiftRank shift (pos : Rococo.Position) =
            match (perspective, shift) with
            | (White, Up delta) | (Black, Down delta) ->
                pos.WithCoords'(rank = min (Rococo.boardSize - 1) (pos.Rank + delta))
            | (White, Down delta) | (Black, Up delta) -> pos.WithCoords'(rank = max 0 (pos.Rank - delta))

        let files() =
            match perspective with
            | White -> baseFiles
            | Black -> baseFiles |> Array.rev

        let shiftFile shift (pos : Rococo.Position) =
            match (perspective, shift) with
            | (White, Left delta) | (Black, Right delta) -> pos.WithCoords'(file = max 0 (pos.File - delta))
            | (White, Right delta) | (Black, Left delta) ->
                pos.WithCoords'(file = min (Rococo.boardSize - 1) (pos.File + delta))

        let performMove fromPos toPos =
            match makeMove fromPos toPos with
            | Complete moveWithInputs -> applyMove moveWithInputs
            | Incomplete (Rococo.Promotion choosePromotion) ->
                requestCapturedPiece
                    (fun (pieceType, _) -> pieceType <> Rococo.CannonPawn)
                    (choosePromotion >> applyMove)

        let (|CanMutualCapture|_|) pos piece =
            match piece with
            | ({Type = Rococo.Swapper} as piece)
            | ({Type = Rococo.Chameleon} as piece)
              when pos
              |> Rococo.adjacentPieces gameState
              |> Seq.exists (fun adjPiece ->
                  adjPiece.Player = Player.oppositeTo piece.Player
                      && (piece.Type = Rococo.Swapper || adjPiece.Type = Rococo.Swapper)
              )
              -> Some piece
            | _ -> None

        let onSelfCapture pos _ =
            selfCaptureImmobilizedPiece pos
            board.Value.CancelMove ()

        let onToggleMutualCapture (pos, piece : Piece<Rococo.PieceType>) _ =
            match board.Value.MovementState with
            | SpecialMove MutualCapture -> board.Value.StartNormalMove pos
            | _ ->
                let adjacentEnemyPieces =
                    pos
                    |> Rococo.adjacentSpaces
                    |> Seq.filter (fun pos ->
                        match GameState.pieceAtPosition gameState pos with
                        | Some {Player = Equals (Player.oppositeTo piece.Player)} -> true
                        | _ -> false
                    )
                    |> Set
                board.Value.StartSpecialMove pos adjacentEnemyPieces MutualCapture

        let performSpecialMove _ fromPos toPos = mutualCapture fromPos toPos

        let onBoardKeyDown (ev : KeyboardEvent) =
            match ev.key with
            | "x" ->
                let focusedPiece = GameState.pieceAtPosition gameState board.Value.FocusedSpace
                if board.Value.PieceIsMoving focusedPiece then
                    if Rococo.immobilizerAdjacent
                        gameState
                        (Player.oppositeTo gameState.Player)
                        board.Value.FocusedSpace
                    then
                        onSelfCapture board.Value.FocusedSpace ()
                    else
                        match focusedPiece with
                        | Some (CanMutualCapture board.Value.FocusedSpace piece) ->
                            onToggleMutualCapture (board.Value.FocusedSpace, piece) ()
                        | _ -> ()
            | _ -> ()

        let borderLabel (label : string) =
            Html.span [ //span instead of div so the nth-of-type selectors for the checkerboard work
                Attr.className classNames.borderSpace
                Html.children [
                    Html.span [
                        Attr.className classNames.label
                        Html.children [
                            Html.text label
                        ]
                    ]
                ]
            ]

        let topBottomBorder() =
            Html.span [
                Attr.className classNames.row
                Html.children [
                    Html.div [Attr.className classNames.borderSpace] //Blank corners for border color
                    for (_, file) in files() do
                        borderLabel file
                    Html.div [Attr.className classNames.borderSpace]
                ]
            ]

        Board(
            handleRef = board,
            gameState = gameState,
            perspective = perspective,
            singlePlayer = singlePlayer,
            freeMovement = freeMovementMode(),
            initialFocusedSpace =
                match perspective with
                | White -> Rococo.Position(1, 1)
                | Black -> Rococo.Position(8, 8)
            ,
            shiftRank = shiftRank,
            shiftFile = shiftFile,
            pieceCanMoveToSpace = pieceCanMoveToSpace,
            performMove = performMove,
            onUnhandledKeyDown = onBoardKeyDown,
            performSpecialMove = performSpecialMove,
            ?onSpaceFocused = onSpaceFocused,
            classList = (defaultArg classList []) @ [
                "UsRococoBoard-root"
                classNames.rococoBoard
                if shouldHideBorder() then classNames.noBorder
            ],
            renderPieceGraphic = renderRococoPiece,
            children = (fun renderSpace -> Html.fragment [
                if not (shouldHideBorder()) then
                    topBottomBorder()
                for (rankIndex, rank) in ranks() do
                    Html.div [
                        Attr.className classNames.row
                        Html.children [
                            if not (shouldHideBorder()) then
                                borderLabel rank
                            for (fileIndex, file) in files() do
                                let pos = Rococo.Position(rankIndex, fileIndex)
                                renderSpace.Decorated(pos, $"{file}{rank}", fun maybePiece movement ->
                                    match maybePiece() with
                                    | Some piece
                                      when Rococo.immobilizerAdjacent gameState (Player.oppositeTo piece.Player) pos ->
                                        Html.fragment [
                                            Html.div [
                                                Attr.className classNames.immobilizedIcon
                                                Attr.title "Immobilized"
                                                Html.children [
                                                    Html.text "❄"
                                                ]
                                            ]
                                            if movement().IsMoving then
                                                Html.button [
                                                    Attr.typeButton
                                                    Attr.className classNames.selfCaptureButton
                                                    Attr.title "Self-capture immobilized piece"
                                                    Ev.onPointerUp (Solid.bindEv(onSelfCapture, pos))
                                                    Html.children [
                                                        Html.text "×"
                                                    ]
                                                ]
                                        ]
                                    | Some (CanMutualCapture pos piece) ->
                                        //Use a memo to prevent the button from re-rendering
                                        //when switching between normal and special movement on the same piece.
                                        //The re-render causes click events to be lost.
                                        let isMoving = Solid.createMemo(fun () -> movement().IsMoving)
                                        if isMoving() then
                                            Html.button [
                                                Attr.typeButton
                                                Attr.className classNames.mutualCaptureButton
                                                Attr.title "Perform mutual capture"
                                                Attr.ariaPressed (movement().IsSpecial)
                                                Ev.onPointerUp (Solid.bindEv(onToggleMutualCapture, (pos, piece)))
                                                Html.children [
                                                    Html.text "💣"
                                                ]
                                            ]
                                        else Html.none
                                    | _ -> Html.none
                                )
                            if not (shouldHideBorder()) then
                                borderLabel rank
                        ]
                    ]
                if not (shouldHideBorder()) then
                    topBottomBorder()
            ])
        )
