module UltimatelySafe.Board.Ultima

open Browser.Types
open Fable.Core
open Fable.Core.JsInterop
open Feliz.JSX.Solid

open UltimatelySafe.Board.Base
open UltimatelySafe.Board.Pieces
open UltimatelySafe.ClientUtils.Focusable
open UltimatelySafe.ClientUtils.SolidExtensions
open UltimatelySafe.Shared.Game
open UltimatelySafe.Shared.Utils.Functions

let private classNames : CssModules.Board.Ultima = importDefault "./Ultima.module.scss"

let private baseRanks = [|for i in 0 .. Ultima.boardSize - 1 -> (i, string (i + 1))|]
let private baseFiles = [|for i in 0 .. Ultima.boardSize - 1 -> (i, string (char (int 'A' + i)))|]

[<AutoOpen>]
type UltimaBoard =
    /// <summary>
    ///     Renders the board for a game of Ultima.<br/>
    ///     See: https://www.chessvariants.com/other.dir/ultima.html<br/>
    ///     Root CSS Class: <c>UsUltimaBoard</c>.
    /// </summary>
    /// <param name="gameState">The current state of the game to render</param>
    /// <param name="makeMove">A function that constructs a move from start and end coordinates</param>
    /// <param name="applyMove">A side-effectful function that applies a constructed move to the game state</param>
    /// <param name="selfCaptureImmobilizedPiece">
    ///     A side-effectful function for performing a self-capture of an immobilized piece
    /// </param>
    /// <param name="perspective">Which player is viewing the board</param>
    /// <param name="singlePlayer">
    ///     If true, the board is only interactive on the <paramref name="perspective"/> player's turn
    /// </param>
    /// <param name="hideBorder">Set to true to hide the border with rank/file labels</param>
    /// <param name="classList">Optional list of CSS classes to apply</param>
    /// <param name="freeMovement">Enable free movement of pieces for testing/demonstration purposes</param>
    /// <param name="focusRef">Ref allowing focusing of the board</param>
    /// <param name="onSpaceFocused">
    ///     Event handler called when a board space receives focus.
    ///     Called with the position and any piece displayed there.
    /// </param>
    [<JSX.Component>]
    static member UltimaBoard(
        gameState : Ultima.State,
        makeMove : Ultima.Position -> Ultima.Position -> MoveWithInputs,
        applyMove : MoveWithInputs -> unit,
        selfCaptureImmobilizedPiece : Ultima.Position -> unit,
        perspective : Player,
        singlePlayer : bool,
        ?hideBorder : bool,
        ?classList : string list,
        ?freeMovement : bool,
        ?focusRef : Focusable ref,
        ?onSpaceFocused : Ultima.Position -> Piece<Ultima.PieceType> option -> unit
    ) =
        let shouldHideBorder() = defaultArg hideBorder false
        let freeMovementMode() = defaultArg freeMovement false

        let pieceCanMoveToSpace fromPos toPos =
            if freeMovementMode() then GameState.spaceIsEmpty gameState toPos
            else Ultima.pieceCanMoveToSpace gameState fromPos toPos

        let board = Solid.makeRef<BoardHandle<Ultima.PieceType, Ultima.Position, _>>()

        match focusRef with
        | Some focusRef ->
            focusRef.Value <- {new Focusable with override _.Focus () = board.Value.Focus ()}
        | None -> ()

        let ranks() =
            match perspective with
            | White -> baseRanks |> Array.rev
            | Black -> baseRanks

        let shiftRank shift (pos : Ultima.Position) =
            match (perspective, shift) with
            | (White, Up delta) | (Black, Down delta) ->
                pos.WithCoords'(rank = min (Ultima.boardSize - 1) (pos.Rank + delta))
            | (White, Down delta) | (Black, Up delta) -> pos.WithCoords'(rank = max 0 (pos.Rank - delta))

        let files() =
            match perspective with
            | White -> baseFiles
            | Black -> baseFiles |> Array.rev

        let shiftFile shift (pos : Ultima.Position) =
            match (perspective, shift) with
            | (White, Left delta) | (Black, Right delta) -> pos.WithCoords'(file = max 0 (pos.File - delta))
            | (White, Right delta) | (Black, Left delta) ->
                pos.WithCoords'(file = min (Ultima.boardSize - 1) (pos.File + delta))

        let onSelfCapture pos _ =
            selfCaptureImmobilizedPiece pos
            board.Value.CancelMove ()

        let onBoardKeyDown (ev : KeyboardEvent) =
            match ev.key with
            | "x" ->
                let focusedPiece = GameState.pieceAtPosition gameState board.Value.FocusedSpace
                if board.Value.PieceIsMoving focusedPiece
                    && Ultima.immobilizerAdjacent
                        gameState
                        (Player.oppositeTo gameState.Player)
                        board.Value.FocusedSpace
                then
                    onSelfCapture board.Value.FocusedSpace ()
            | _ -> ()

        let borderLabel (label : string) =
            Html.span [ //span instead of div so the nth-of-type selectors for the checkerboard work
                Attr.className classNames.borderSpace
                Html.children [
                    Html.span [
                        Attr.className classNames.label
                        Html.children [
                            Html.text label
                        ]
                    ]
                ]
            ]

        let topBottomBorder() =
            Html.span [
                Attr.className classNames.row
                Html.children [
                    Html.div [Attr.className classNames.borderSpace] //Blank corners for border color
                    for (_, file) in files() do
                        borderLabel file
                    Html.div [Attr.className classNames.borderSpace]
                ]
            ]

        Board(
            handleRef = board,
            gameState = gameState,
            perspective = perspective,
            singlePlayer = singlePlayer,
            freeMovement = freeMovementMode(),
            initialFocusedSpace =
                match perspective with
                | White -> Ultima.Position(0, 0)
                | Black -> Ultima.Position(7, 7)
            ,
            shiftRank = shiftRank,
            shiftFile = shiftFile,
            pieceCanMoveToSpace = pieceCanMoveToSpace,
            performMove = (makeMove >>+ applyMove),
            onUnhandledKeyDown = onBoardKeyDown,
            ?onSpaceFocused = onSpaceFocused,
            classList = (defaultArg classList []) @ [
                "UsUltimaBoard-root"
                classNames.ultimaBoard
                if shouldHideBorder() then classNames.noBorder
            ],
            renderPieceGraphic = renderUltimaPiece,
            children = (fun renderSpace -> Html.fragment [
                if not (shouldHideBorder()) then
                    topBottomBorder()
                for (rankIndex, rank) in ranks() do
                    Html.div [
                        Attr.className classNames.row
                        Html.children [
                            if not (shouldHideBorder()) then
                                borderLabel rank
                            for (fileIndex, file) in files() do
                                let pos = Ultima.Position(rankIndex, fileIndex)
                                renderSpace.Decorated(pos, $"{file}{rank}", fun maybePiece movement ->
                                    match maybePiece() with
                                    | Some piece
                                      when Ultima.immobilizerAdjacent gameState (Player.oppositeTo piece.Player) pos ->
                                        Html.fragment [
                                            Html.div [
                                                Attr.className classNames.immobilizedIcon
                                                Attr.title "Immobilized"
                                                Html.children [
                                                    Html.text "❄"
                                                ]
                                            ]
                                            if movement().IsMoving then
                                                Html.button [
                                                    Attr.typeButton
                                                    Attr.className classNames.selfCaptureButton
                                                    Attr.title "Self-capture immobilized piece"
                                                    Ev.onPointerUp (Solid.bindEv(onSelfCapture, pos))
                                                    Html.children [
                                                        Html.text "×"
                                                    ]
                                                ]
                                        ]
                                    | _ -> Html.none
                                )
                            if not (shouldHideBorder()) then
                                borderLabel rank
                        ]
                    ]
                if not (shouldHideBorder()) then
                    topBottomBorder()
            ])
        )
