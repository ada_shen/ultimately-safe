module UltimatelySafe.Board.Base

open Browser.Dom
open Browser.Types
open Fable.Core
open Fable.Core.JsInterop
open Feliz.JSX.Solid

open UltimatelySafe.Board.Piece
open UltimatelySafe.Board.Space
open UltimatelySafe.ClientUtils.Focusable
open UltimatelySafe.ClientUtils.SolidExtensions
open UltimatelySafe.Components.Ext.SolidDnd
open UltimatelySafe.Shared.Game
open UltimatelySafe.Shared.Utils.ActivePatterns

let private classNames : CssModules.Board.Base = importDefault "./Base.module.scss"

/// A manner of moving a piece on the board
type private MovementType<'Position, 'SpecialMoveType when 'Position :> BoardPosition and 'Position : comparison> =
    /// Discrete selection via click or keyboard
    | Select

    /// Click + drag
    | Drag

    /// A special/custom move type initiated by the parent component. Functions like Select.
    | Special of targets : Set<'Position> * moveType : 'SpecialMoveType

/// <summary>Marker interface for either <see cref="HorizontalShift"/> or <see cref="VerticalShift"/></summary>
type Shift = interface end

/// Movement to either the left or right
type HorizontalShift = Left of int | Right of int with interface Shift

/// Movement either up or down
type VerticalShift = Up of int | Down of int with interface Shift

type MovementState<'SpecialMoveType> =
    | NotMoving
    | NormalMove
    | SpecialMove of 'SpecialMoveType

    member this.IsSpecial =
        match this with
        | SpecialMove _ -> true
        | _ -> false

    member this.IsMoving =
        match this with
        | NormalMove | SpecialMove _ -> true
        | NotMoving -> false

/// Imperative handle for the <see cref="Board"/> component
type BoardHandle<
    'PieceType, 'Position, 'SpecialMoveType
        when 'PieceType :> IPieceType and 'Position :> BoardPosition and 'Position : comparison
> =
    inherit Focusable

    /// The root element of the board
    abstract member Root : HTMLDivElement

    /// The position of the focused space within the board,
    /// or the space that will receive focus when next the board does
    abstract member FocusedSpace : 'Position with get, set

    abstract member PieceIsMoving : piece : Piece<'PieceType> option -> bool

    /// Cancels any current move in progress (deselects any selected piece)
    abstract member CancelMove : unit -> unit

    /// <summary>Starts a special/custom move</summary>
    /// <param name="fromPos">The position of the moving piece</param>
    /// <param name="targets">The set of possible target spaces for the special move</param>
    /// <param name="moveType">A value indicating to the caller the type of move being made</param>
    abstract member StartSpecialMove :
        fromPos : 'Position
        -> targets : Set<'Position>
        -> moveType : 'SpecialMoveType
        -> unit

    /// Starts a normal selection-based move from the given position
    abstract member StartNormalMove : fromPos : 'Position -> unit

    /// The current movement state of the board
    abstract member MovementState : MovementState<'SpecialMoveType>

/// Interface to hold two methods for rendering spaces in the board, with or without additional decorations
type RenderSpace<'PieceType, 'Position, 'SpecialMoveType when 'PieceType :> IPieceType and 'Position :> BoardPosition> =
    /// <summary>Renders a space on the board with no additional decoration</summary>
    /// <param name="position">The position of the space to render</param>
    /// <param name="spaceId">The ID of the space to render (typically file + rank)</param>
    abstract member Undecorated : position : 'Position * spaceId : string -> JSX.Element

    /// <summary>
    ///     Renders a space on the board. Accepts a callback for decorating the space with additional markup.
    /// </summary>
    /// <param name="position">The position of the space to render</param>
    /// <param name="spaceId">The ID of the space to render (typically file + rank)</param>
    /// <param name="decorateSpace">
    ///     A function returning additional markup to add to the space (likely absolutely positioned).
    ///     Is given the piece currently in the space (if there is one)
    ///     and whether or not that piece has been selected for movement.
    /// </param>
    abstract member Decorated :
        position : 'Position
        * spaceId : string
        * decorateSpace :
            ((unit -> Piece<'PieceType> option)
            -> (unit -> MovementState<'SpecialMoveType>)
            -> JSX.Element)
        -> JSX.Element

[<AutoOpen>]
type Board =
    /// <summary>
    ///     Base component for rendering a game board.
    ///     Handles interaction via mouse, keyboard, and drag+drop, but contains no game logic.
    /// </summary>
    /// <param name="gameState">The current state of the game to render</param>
    /// <param name="perspective">Which player is viewing the board</param>
    /// <param name="singlePlayer">
    ///     If true, the board is only interactive on the <paramref name="perspective"/> player's turn
    /// </param>
    /// <param name="initialFocusedSpace">The space to receive keyboard focus when the board first renders</param>
    /// <param name="shiftRank">A function for applying a vertical shift to a rank index on this board</param>
    /// <param name="shiftFile">A function for applying a horizontal shift to a file index on this board</param>
    /// <param name="pieceCanMoveToSpace">
    ///     A predicate returning whether a move is valid between a given start and end point.
    ///     Used to determine where pieces can be dropped when moving.
    /// </param>
    /// <param name="performMove">
    ///     A side-effectful function for performing a move between a start and end position
    /// </param>
    /// <param name="children">
    ///     A function for rendering the contents of the board.
    ///     Responsible for determining the board's layout and appearance.
    ///     Is passed an object with methods for rendering the individual spaces.
    /// </param>
    /// <param name="classList">Optional list of CSS classes to apply</param>
    /// <param name="renderPieceGraphic">Function for getting the graphic to display for a piece</param>
    /// <param name="handleRef">A ref cell to which to assign the imperative handle for this component</param>
    /// <param name="onSpaceFocused">
    ///     Event handler called when a board space receives focus.
    ///     Called with the position and any piece displayed there.
    /// </param>
    /// <param name="onUnhandledKeyDown">A keydown event handler for keys not handled by the base component</param>
    /// <param name="performSpecialMove">Callback function for completing a special/custom move</param>
    /// <param name="freeMovement">Enable free movement of pieces for testing/demonstration purposes</param>
    [<JSX.Component>]
    static member Board<
        'PieceType, 'Position, 'SpecialMoveType
            when 'PieceType :> IPieceType
            and 'SpecialMoveType : equality
            and 'Position :> BoardPosition
            and 'Position : comparison
    >(
        gameState : GameState<'PieceType>,
        perspective : Player,
        singlePlayer : bool,
        initialFocusedSpace : 'Position,
        shiftRank : VerticalShift -> 'Position -> 'Position,
        shiftFile : HorizontalShift -> 'Position -> 'Position,
        pieceCanMoveToSpace : 'Position -> 'Position -> bool,
        performMove : 'Position -> 'Position -> unit,
        children : RenderSpace<'PieceType, 'Position, 'SpecialMoveType> -> JSX.Element,
        renderPieceGraphic : Player -> 'PieceType -> JSX.Element,
        ?classList : string list,
        ?handleRef : BoardHandle<'PieceType, 'Position, 'SpecialMoveType> ref,
        ?onSpaceFocused : 'Position -> Piece<'PieceType> option -> unit,
        ?onUnhandledKeyDown : KeyboardEvent -> unit,
        ?performSpecialMove : 'SpecialMoveType -> 'Position -> 'Position -> unit,
        ?freeMovement : bool
    ) =
        let freeMovementMode() = defaultArg freeMovement false

        let gameCompleted() = gameState.Winner.IsSome

        let root = Solid.makeRef<HTMLDivElement>()

        let shiftPosition (shift : Shift) pos =
            match shift with
            | :? HorizontalShift as hShift -> shiftFile hShift pos
            | :? VerticalShift as vShift -> shiftRank vShift pos
            | _ -> failwith "Unknown Shift subtype"

        let (focusedSpace, setFocusedSpace) = Solid.createSignal(initialFocusedSpace)
        let spaceIsFocused = Solid.createSelector(focusedSpace, (=))

        let (showFocus, setShowFocus) = Solid.createSignal(false)

        //If a piece is being moved (by dragging or otherwise), contains its ID and starting position
        let (movingPiece, setMovingPiece) =
            Solid.createSignal<(string * 'Position * MovementType<'Position, 'SpecialMoveType>) option>(None)
        let pieceIsMoving = Solid.createSelector(movingPiece, fun (piece : Piece<'PieceType> option) movingPiece ->
            match (piece, movingPiece) with
            | (Some piece, Some (movingId, _, _)) -> piece.Id = movingId
            | _ -> false
        )
        let movementType piece =
            //Use pieceIsMoving to avoid unnecessary re-renders
            if pieceIsMoving(piece) then
                match movingPiece() with
                | Some (_, _, Special (_, specialMoveType)) -> SpecialMove specialMoveType
                | _ -> NormalMove
            else
                NotMoving

        //In certain situations, such as pawn promotion, a move is not completely made when the player drops a piece.
        //In these cases, it's jarring for the piece to snap back to its original position until the move is completed.
        //To alleviate this, use a ReactiveMap to maintain temporary overrides to game/board state until the state is
        //next updated. This can be used to persist the movement that was made.
        //Structure: board position -> overridden space state.
        let (tempStateOverrides, setTempStateOverrides) = Solid.createSignal(Map.empty<'Position, string option>)
        Solid.createEffect(Solid.on(
            //Detect that a move has been made by watching the NextMoveIndex
            (fun () -> gameState.NextMoveIndex),
            (fun _ _ -> setTempStateOverrides(Map.empty)),
            {|defer = true|}
        ))
        let overrideForSpace pos = tempStateOverrides() |> Map.tryFind pos
        let setSpaceOverride pos spaceOverride =
            setTempStateOverrides(tempStateOverrides() |> Map.add pos spaceOverride)

        let performMove' (fromPos : 'Position) (toPos : 'Position) =
            //This is simplistic and not perfect, but should be fine for just making the UI less jarring:
            //When the player moves a piece, simply null out the original position and move that piece ID to toPos
            setSpaceOverride toPos gameState.Board[fromPos.Rank].[fromPos.File]
            setSpaceOverride fromPos None
            performMove fromPos toPos

        let pieceAtPosition pos =
            match overrideForSpace pos with
            | Some spaceOverride ->
                //If we have an override, we need to handle the possibility that the ID doesn't exist.
                //This is because, when a move is applied, the re-render will happen before the effect clears the Map.
                //Working around this here was the best solution I could come up with.
                //I would love a cleaner way, though...
                spaceOverride |> Option.bind (fun id -> gameState.Pieces |> Map.tryFind id)
            | None -> GameState.pieceAtPosition gameState pos

        let pieceOwnedByPlayer (piece : Piece<'PieceType> option) =
            match piece with
            | Some p -> p.Player = gameState.Player
            | None -> false

        let pieceCanBeMoved piece =
            freeMovementMode()
                || not (gameCompleted())
                   //Don't allow picking up another piece while in the middle of a move
                   && (tempStateOverrides().IsEmpty)
                   && pieceOwnedByPlayer piece
                   && (not singlePlayer || gameState.Player = perspective)

        let focusSpace pos =
            setFocusedSpace(pos)
            match onSpaceFocused with
            | Some handler -> handler pos (pieceAtPosition pos)
            | None -> ()

        let onDragStart (ev : DragEvent) =
            let position = ev.draggable.data["position"] |> unbox<'Position>
            focusSpace position
            setMovingPiece(Some (unbox ev.draggable.id, position, Drag))

        let onDragEnd (ev : DragEvent) =
            match ev.droppable with
            | Some droppable ->
                let toPos = droppable.data["position"] |> unbox<'Position>
                match movingPiece() with
                | Some (_, _, Special _) -> () //You can't use drag-and-drop for special moves
                | Some (_, fromPos, _) when pieceCanMoveToSpace fromPos toPos ->
                    focusSpace toPos
                    performMove' fromPos toPos
                | _ -> ()
            | None -> ()

            setMovingPiece(None)

        let onSelectSpace position =
            match movingPiece() with
            | Some (_, fromPos, movementType) ->
                match movementType with
                | Special (targets, moveType) ->
                    if targets |> Set.contains position then
                        match performSpecialMove with
                        | Some handler -> handler moveType fromPos position
                        | None -> ()
                | _ ->
                    if pieceCanMoveToSpace fromPos position then
                        performMove' fromPos position

                setMovingPiece(None)
            | None ->
                let piece = pieceAtPosition position
                if pieceCanBeMoved piece then
                    setMovingPiece(Some (piece.Value.Id, position, Select))

        let onSpaceClick position (ev : PointerEvent) =
            focusSpace position

            //If the click target is a button (e.g., the self-capture button) then don't treat it as a selection
            if (ev.target :?> HTMLElement).closest("button").IsNone then
                onSelectSpace position

            ev.stopPropagation()

        let onWindowClick (ev : MouseEvent) =
            //The space click handler will handle clearing things out if the user clicks a non-interactive space.
            //Avoid doing so here to prevent conflicts with interactive elements within spaces.
            if (ev.target :?> HTMLElement).closest(".UsBoardSpace-root").IsNone then
                setMovingPiece(None)

        Solid.createEffect(fun () ->
            //Need to use pointerup because that's what the Space component uses
            window.addEventListener("pointerup", unbox onWindowClick)
            Solid.onCleanup(fun () -> window.removeEventListener("pointerup", unbox onWindowClick))
        )

        let onBoardKeyDown (ev : KeyboardEvent) =
            match ev.key with
            | Prefix "Arrow" direction ->
                setShowFocus(true)
                ev.preventDefault() //Prevent scrolling
                match direction with
                | "Up" -> focusSpace (focusedSpace() |> shiftPosition (Up 1))
                | "Down" -> focusSpace (focusedSpace() |> shiftPosition (Down 1))
                | "Left" -> focusSpace (focusedSpace() |> shiftPosition (Left 1))
                | "Right" -> focusSpace (focusedSpace() |> shiftPosition (Right 1))
                | _ -> ()
            | "Enter" | " " ->
                onSelectSpace (focusedSpace())
                ev.preventDefault()
            | _ ->
                match onUnhandledKeyDown with
                | Some handler -> handler ev
                | None -> ()

        let onWindowKeyDown (ev : KeyboardEvent) =
            match ev.key with
            | "Tab" -> setShowFocus(true)
            | "Escape" -> setMovingPiece(None)
            | _ -> ()

        Solid.createEffect(fun () ->
            window.addEventListener("keydown", unbox onWindowKeyDown)
            Solid.onCleanup(fun () -> window.removeEventListener("keydown", unbox onWindowKeyDown))
        )

        match handleRef with
        | Some handleRef ->
            handleRef.Value <- {new BoardHandle<'PieceType, 'Position, 'SpecialMoveType> with
                override _.Root = root.Value

                override this.Focus () = this.Root.focus()

                override _.FocusedSpace
                    with get () = focusedSpace()
                    and set value = focusSpace value

                override _.PieceIsMoving piece = pieceIsMoving(piece)

                override _.CancelMove () = setMovingPiece(None)

                override _.StartSpecialMove fromPos targets moveType =
                    match GameState.pieceAtPosition gameState fromPos with
                    | Some piece -> setMovingPiece(Some (piece.Id, fromPos, Special (targets, moveType)))
                    | None -> failwith $"No piece at position %A{fromPos}"

                override _.StartNormalMove fromPos =
                    match GameState.pieceAtPosition gameState fromPos with
                    | Some piece -> setMovingPiece(Some (piece.Id, fromPos, Select))
                    | None -> failwith $"No piece at position %A{fromPos}"

                override _.MovementState =
                    match movingPiece() with
                    | Some (_, _, Special (_, specialMoveType)) -> SpecialMove specialMoveType
                    | Some _ -> NormalMove
                    | None -> NotMoving
            }
        | None -> ()

        DragDropProvider(
            onDragStart = onDragStart,
            onDragEnd = onDragEnd,
            children = (
                DragDropSensors(
                    Html.div [
                        Solid.ref root
                        Attr.classList ((defaultArg classList []) @ [
                            "UsBoard-root"
                            classNames.board
                            if showFocus() then classNames.showFocus
                            match movingPiece() with
                            | Some (_, _, movementType) ->
                                classNames.moving
                                match movementType with
                                | Select | Special _ -> classNames.selecting
                                | _ -> ()
                            | None -> ()
                        ])
                        Attr.tabIndex 0
                        Ev.onFocus (fun _ ->
                            match onSpaceFocused with
                            | Some handler ->
                                //If showFocus is false, then we're coming from a click,
                                //in which case we shouldn't fire here as the pointerup handler will also
                                //fire the event handler, after updating focusedSpace.
                                //Can't use a `when` clause for this as for some reason this causes the compiler to emit
                                //handler(focusedSpace())(pieceAtPosition(focusedSpace())) to JS, which is incorrect.
                                //TODO: Look for/file GH issue about this behavior (try to find minimal repro)
                                if showFocus() then
                                    handler (focusedSpace()) (pieceAtPosition (focusedSpace()))
                            | _ -> ()
                        )
                        Ev.onPointerDown (fun _ -> setShowFocus(false))
                        Ev.onKeyDown onBoardKeyDown
                        Html.children [
                            children {new RenderSpace<'PieceType, 'Position, 'SpecialMoveType> with
                                override this.Undecorated(position, spaceId) =
                                    this.Decorated(position, spaceId, (fun _ _ -> Html.none))

                                override _.Decorated(position, spaceId, decorateSpace) =
                                    let piece() = pieceAtPosition position
                                    let movable() = pieceCanBeMoved (piece())
                                    let droppable = Solid.createMemo(fun () ->
                                        not (gameCompleted())
                                            && match movingPiece() with
                                               | Some (_, _, Special (targets, _)) -> targets |> Set.contains position
                                               | Some (_, startingPos, _) -> pieceCanMoveToSpace startingPos position
                                               | None -> false
                                    )
                                    Space(
                                        id = spaceId,
                                        position = position,
                                        clickable = not (gameCompleted()),
                                        onClick = onSpaceClick,
                                        droppable = droppable(),
                                        classList = [
                                            if movable() then classNames.movableSpace
                                            if pieceIsMoving(piece()) then classNames.activeSpace
                                            if spaceIsFocused(position) then classNames.focusedSpace
                                        ],
                                        children = Html.fragment [
                                            match piece() with
                                            | Some piece ->
                                                BoardPiece(
                                                    piece = piece,
                                                    draggable = movable(),
                                                    position = position,
                                                    renderPieceGraphic = renderPieceGraphic
                                                )
                                            | None -> ()
                                            Html.fragment [
                                                decorateSpace piece (movementType << piece)
                                            ]
                                        ]
                                    )
                            }
                        ]
                    ]
                )
            )
        )
