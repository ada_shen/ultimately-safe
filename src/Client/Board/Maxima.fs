module UltimatelySafe.Board.Maxima

open Browser.Types
open Fable.Core
open Fable.Core.JsInterop
open Feliz.JSX.Solid

open UltimatelySafe.Board.Base
open UltimatelySafe.Board.Pieces
open UltimatelySafe.ClientUtils.Focusable
open UltimatelySafe.ClientUtils.SolidExtensions
open UltimatelySafe.Shared.Game
open UltimatelySafe.Shared.Utils.Functions

let private classNames : CssModules.Board.Maxima = importDefault "./Maxima.module.scss"

let private baseRanks = [|for i in 0 .. Maxima.maxRankValue -> (i, string i)|]
let private baseFiles = [|for i in 0 .. Maxima.maxFileValue -> (i, string (char (int 'A' + i)))|]

[<AutoOpen>]
type MaximaBoard =
    /// <summary>
    ///     Renders the board for a game of Maxima.<br/>
    ///     See: https://www.chessvariants.com/dpieces.dir/maxima/maxima.html<br/>
    ///     Root CSS Class: <c>UsMaximaBoard</c>.
    /// </summary>
    /// <param name="gameState">The current state of the game to render</param>
    /// <param name="makeMove">A function that constructs a move from start and end coordinates</param>
    /// <param name="applyMove">A side-effectful function that applies a constructed move to the game state</param>
    /// <param name="selfCaptureImmobilizedPiece">
    ///     A side-effectful function for performing a self-capture of an immobilized piece
    /// </param>
    /// <param name="perspective">Which player is viewing the board</param>
    /// <param name="singlePlayer">
    ///     If true, the board is only interactive on the <paramref name="perspective"/> player's turn
    /// </param>
    /// <param name="hideBorder">Set to true to hide the border with rank/file labels</param>
    /// <param name="classList">Optional list of CSS classes to apply</param>
    /// <param name="freeMovement">Enable free movement of pieces for testing/demonstration purposes</param>
    /// <param name="focusRef">Ref allowing focusing of the board</param>
    /// <param name="onSpaceFocused">
    ///     Event handler called when a board space receives focus.
    ///     Called with the position and any piece displayed there.
    /// </param>
    [<JSX.Component>]
    static member MaximaBoard(
        gameState : Maxima.State,
        makeMove : Maxima.Position -> Maxima.Position -> MoveWithInputs,
        applyMove : MoveWithInputs -> unit,
        selfCaptureImmobilizedPiece : Maxima.Position -> unit,
        perspective : Player,
        singlePlayer : bool,
        ?hideBorder : bool,
        ?classList : string list,
        ?freeMovement : bool,
        ?focusRef : Focusable ref,
        ?onSpaceFocused : Maxima.Position -> Piece<Maxima.PieceType> option -> unit
    ) =
        let shouldHideBorder() = defaultArg hideBorder false
        let freeMovementMode() = defaultArg freeMovement false

        let pieceCanMoveToSpace fromPos toPos =
            if freeMovementMode() then GameState.spaceIsEmpty gameState toPos
            else Maxima.pieceCanMoveToSpace gameState fromPos toPos

        let board = Solid.makeRef<BoardHandle<Maxima.PieceType, Maxima.Position, _>>()

        match focusRef with
        | Some focusRef ->
            focusRef.Value <- {new Focusable with override _.Focus () = board.Value.Focus ()}
        | None -> ()

        let ranks() =
            match perspective with
            | White -> baseRanks |> Array.rev
            | Black -> baseRanks

        let shiftRank shift (pos : Maxima.Position) =
            match (perspective, shift) with
            | (White, Up delta) | (Black, Down delta) ->
                pos.WithCoords'(rank = min (Maxima.lastRank pos.File) (pos.Rank + delta))
            | (White, Down delta) | (Black, Up delta) ->
                pos.WithCoords'(rank = max (Maxima.firstRank pos.File) (pos.Rank - delta))

        let files() =
            match perspective with
            | White -> baseFiles
            | Black -> baseFiles |> Array.rev

        let shiftFile shift (pos : Maxima.Position) =
            match (perspective, shift) with
            | (White, Left delta) | (Black, Right delta) ->
                pos.WithCoords'(file = max (Maxima.firstFile pos.Rank) (pos.File - delta))
            | (White, Right delta) | (Black, Left delta) ->
                pos.WithCoords'(file = min (Maxima.lastFile pos.Rank) (pos.File + delta))

        let onSelfCapture pos _ =
            selfCaptureImmobilizedPiece pos
            board.Value.CancelMove ()

        let onBoardKeyDown (ev : KeyboardEvent) =
            match ev.key with
            | "x" ->
                let focusedPiece = GameState.pieceAtPosition gameState board.Value.FocusedSpace
                if board.Value.PieceIsMoving focusedPiece
                    && Maxima.pieceIsImmobilized gameState board.Value.FocusedSpace
                then
                    onSelfCapture board.Value.FocusedSpace ()
            | _ -> ()

        let borderLabel (label : string) =
            Html.span [ //span instead of div so the nth-of-type selectors for the checkerboard work
                Attr.className classNames.borderSpace
                Html.children [
                    Html.span [
                        Attr.className classNames.label
                        Html.children [
                            Html.text label
                        ]
                    ]
                ]
            ]

        let topBottomBorder() =
            Html.span [
                Attr.className classNames.row
                Html.children [
                    Html.div [Attr.className classNames.borderSpace] //Blank corners for border color
                    for (_, file) in files() do
                        borderLabel file
                    Html.div [Attr.className classNames.borderSpace]
                ]
            ]

        Board(
            handleRef = board,
            gameState = gameState,
            perspective = perspective,
            singlePlayer = singlePlayer,
            freeMovement = freeMovementMode(),
            initialFocusedSpace =
                match perspective with
                | White -> Maxima.Position(1, 0)
                | Black -> Maxima.Position(9, 7)
            ,
            shiftRank = shiftRank,
            shiftFile = shiftFile,
            pieceCanMoveToSpace = pieceCanMoveToSpace,
            performMove = (makeMove >>+ applyMove),
            onUnhandledKeyDown = onBoardKeyDown,
            ?onSpaceFocused = onSpaceFocused,
            classList = [
                yield! defaultArg classList []
                "UsMaximaBoard-root"
                classNames.maximaBoard
                if shouldHideBorder() then classNames.noBorder
            ],
            renderPieceGraphic = renderMaximaPiece,
            children = (fun renderSpace -> Html.fragment [
                if not (shouldHideBorder()) then
                    topBottomBorder()
                for (rankIndex, rank) in ranks() do
                    Html.div [
                        Attr.classList [
                            classNames.row
                            if rankIndex = 0 || rankIndex = Maxima.maxRankValue then classNames.goalRow
                        ]
                        Html.children [
                            if not (shouldHideBorder()) then
                                borderLabel rank
                            for (fileIndex, file) in files() do
                                match Maxima.Position.TryCreate(rankIndex, fileIndex) with
                                | Error _ ->
                                    //Use blank border spaces for the invalid spaces around the goals
                                    Html.span [Attr.className classNames.borderSpace]
                                | Ok pos ->
                                    renderSpace.Decorated(pos, $"{file}{rank}", fun maybePiece movement ->
                                        match maybePiece() with
                                        | Some _
                                          when Maxima.pieceIsImmobilized gameState pos ->
                                            Html.fragment [
                                                Html.div [
                                                    Attr.className classNames.immobilizedIcon
                                                    Attr.title "Immobilized"
                                                    Html.children [
                                                        Html.text "❄"
                                                    ]
                                                ]
                                                if movement().IsMoving then
                                                    Html.button [
                                                        Attr.typeButton
                                                        Attr.className classNames.selfCaptureButton
                                                        Attr.title "Self-capture immobilized piece"
                                                        Ev.onPointerUp (Solid.bindEv(onSelfCapture, pos))
                                                        Html.children [
                                                            Html.text "×"
                                                        ]
                                                    ]
                                            ]
                                        | _ -> Html.none
                                    )
                            if not (shouldHideBorder()) then
                                borderLabel rank
                        ]
                    ]
                if not (shouldHideBorder()) then
                    topBottomBorder()
            ])
        )
