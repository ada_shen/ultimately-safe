module UltimatelySafe.Board.Pieces

open Fable.Core

open UltimatelySafe.Shared.Game

let renderUltimaPiece (player : Player) (pieceType : Ultima.PieceType) =
    //JSX is untyped (and sadly not syntax highlighted in JetBrains Rider),
    //but given the very meta nature of importing SVG files as JSX components,
    //I think using it for this purpose is the most pragmatic choice
    JSX.jsx $"""
        import ChessBlackBishop from "./Pieces/Chess/Black/Bishop.svg";
        import ChessBlackKing from "./Pieces/Chess/Black/King.svg";
        import ChessBlackKnight from "./Pieces/Chess/Black/Knight.svg";
        import ChessBlackPawn from "./Pieces/Chess/Black/Pawn.svg";
        import ChessBlackQueen from "./Pieces/Chess/Black/Queen.svg";
        import ChessBlackRook from "./Pieces/Chess/Black/Rook.svg";
        import ChessWhiteBishop from "./Pieces/Chess/White/Bishop.svg";
        import ChessWhiteKing from "./Pieces/Chess/White/King.svg";
        import ChessWhiteKnight from "./Pieces/Chess/White/Knight.svg";
        import ChessWhitePawn from "./Pieces/Chess/White/Pawn.svg";
        import ChessWhiteQueen from "./Pieces/Chess/White/Queen.svg";
        import ChessWhiteRook from "./Pieces/Chess/White/Rook.svg";

        <>
            {match player with
             | Black ->
                 match pieceType with
                 | Ultima.Chameleon -> JSX.jsx "<ChessBlackBishop/>"
                 | Ultima.Coordinator -> JSX.jsx "<ChessBlackRook/>"
                 | Ultima.Immobilizer ->
                     //The Rook isn't completely centered in its viewBox,
                     //so add a translate to re-align it after flipping
                     JSX.jsx "<ChessBlackRook style={{transform: 'rotate(180deg) translateY(-7%)'}}/>"
                 | Ultima.King -> JSX.jsx "<ChessBlackKing/>"
                 | Ultima.Pawn -> JSX.jsx "<ChessBlackPawn/>"
                 | Ultima.Withdrawer -> JSX.jsx "<ChessBlackQueen/>"
                 | Ultima.LongLeaper -> JSX.jsx "<ChessBlackKnight/>"
             | White ->
                 match pieceType with
                 | Ultima.Chameleon -> JSX.jsx "<ChessWhiteBishop/>"
                 | Ultima.Coordinator -> JSX.jsx "<ChessWhiteRook/>"
                 | Ultima.Immobilizer ->
                     JSX.jsx "<ChessWhiteRook style={{transform: 'rotate(180deg) translateY(-7%)'}}/>"
                 | Ultima.King -> JSX.jsx "<ChessWhiteKing/>"
                 | Ultima.Pawn -> JSX.jsx "<ChessWhitePawn/>"
                 | Ultima.Withdrawer -> JSX.jsx "<ChessWhiteQueen/>"
                 | Ultima.LongLeaper -> JSX.jsx "<ChessWhiteKnight/>"
            }
        </>
    """

let renderRococoPiece (player : Player) (pieceType : Rococo.PieceType) =
    JSX.jsx $"""
        import RococoBlackAdvancer from "./Pieces/Rococo/Black/Advancer.svg";
        import RococoBlackCannonPawn from "./Pieces/Rococo/Black/CannonPawn.svg";
        import RococoBlackChameleon from "./Pieces/Rococo/Black/Chameleon.svg";
        import RococoBlackImmobilizer from "./Pieces/Rococo/Black/Immobilizer.svg";
        import RococoBlackKing from "./Pieces/Rococo/Black/King.svg";
        import RococoBlackLongLeaper from "./Pieces/Rococo/Black/LongLeaper.svg";
        import RococoBlackSwapper from "./Pieces/Rococo/Black/Swapper.svg";
        import RococoBlackWithdrawer from "./Pieces/Rococo/Black/Withdrawer.svg";
        import RococoWhiteAdvancer from "./Pieces/Rococo/White/Advancer.svg";
        import RococoWhiteCannonPawn from "./Pieces/Rococo/White/CannonPawn.svg";
        import RococoWhiteChameleon from "./Pieces/Rococo/White/Chameleon.svg";
        import RococoWhiteImmobilizer from "./Pieces/Rococo/White/Immobilizer.svg";
        import RococoWhiteKing from "./Pieces/Rococo/White/King.svg";
        import RococoWhiteLongLeaper from "./Pieces/Rococo/White/LongLeaper.svg";
        import RococoWhiteSwapper from "./Pieces/Rococo/White/Swapper.svg";
        import RococoWhiteWithdrawer from "./Pieces/Rococo/White/Withdrawer.svg";

        <>
            {match player with
             | Black ->
                 match pieceType with
                 | Rococo.Advancer -> JSX.jsx "<RococoBlackAdvancer/>"
                 | Rococo.LongLeaper -> JSX.jsx "<RococoBlackLongLeaper/>"
                 | Rococo.Swapper -> JSX.jsx "<RococoBlackSwapper/>"
                 | Rococo.Withdrawer -> JSX.jsx "<RococoBlackWithdrawer/>"
                 | Rococo.Immobilizer -> JSX.jsx "<RococoBlackImmobilizer/>"
                 | Rococo.Chameleon -> JSX.jsx "<RococoBlackChameleon/>"
                 | Rococo.CannonPawn -> JSX.jsx "<RococoBlackCannonPawn/>"
                 | Rococo.King -> JSX.jsx "<RococoBlackKing/>"
             | White ->
                 match pieceType with
                 | Rococo.Advancer -> JSX.jsx "<RococoWhiteAdvancer/>"
                 | Rococo.LongLeaper -> JSX.jsx "<RococoWhiteLongLeaper/>"
                 | Rococo.Swapper -> JSX.jsx "<RococoWhiteSwapper/>"
                 | Rococo.Withdrawer -> JSX.jsx "<RococoWhiteWithdrawer/>"
                 | Rococo.Immobilizer -> JSX.jsx "<RococoWhiteImmobilizer/>"
                 | Rococo.Chameleon -> JSX.jsx "<RococoWhiteChameleon/>"
                 | Rococo.CannonPawn -> JSX.jsx "<RococoWhiteCannonPawn/>"
                 | Rococo.King -> JSX.jsx "<RococoWhiteKing/>"
            }
        </>
    """

let renderMaximaPiece (player : Player) (pieceType : Maxima.PieceType) =
    JSX.jsx $"""
        import MaximaBlackChameleon from "./Pieces/Maxima/Black/Chameleon.svg";
        import MaximaBlackCoordinator from "./Pieces/Maxima/Black/Coordinator.svg";
        import MaximaBlackGuard from "./Pieces/Maxima/Black/Guard.svg";
        import MaximaBlackImmobilizer from "./Pieces/Maxima/Black/Immobilizer.svg";
        import MaximaBlackKing from "./Pieces/Maxima/Black/King.svg";
        import MaximaBlackLongLeaper from "./Pieces/Maxima/Black/LongLeaper.svg";
        import MaximaBlackMage from "./Pieces/Maxima/Black/Mage.svg";
        import MaximaBlackPawn from "./Pieces/Maxima/Black/Pawn.svg";
        import MaximaBlackWithdrawer from "./Pieces/Maxima/Black/Withdrawer.svg";
        import MaximaWhiteChameleon from "./Pieces/Maxima/White/Chameleon.svg";
        import MaximaWhiteCoordinator from "./Pieces/Maxima/White/Coordinator.svg";
        import MaximaWhiteGuard from "./Pieces/Maxima/White/Guard.svg";
        import MaximaWhiteImmobilizer from "./Pieces/Maxima/White/Immobilizer.svg";
        import MaximaWhiteKing from "./Pieces/Maxima/White/King.svg";
        import MaximaWhiteLongLeaper from "./Pieces/Maxima/White/LongLeaper.svg";
        import MaximaWhiteMage from "./Pieces/Maxima/White/Mage.svg";
        import MaximaWhitePawn from "./Pieces/Maxima/White/Pawn.svg";
        import MaximaWhiteWithdrawer from "./Pieces/Maxima/White/Withdrawer.svg";

        <>
            {match player with
             | Black ->
                 match pieceType with
                 | Maxima.Chameleon -> JSX.jsx "<MaximaBlackChameleon/>"
                 | Maxima.Coordinator -> JSX.jsx "<MaximaBlackCoordinator/>"
                 | Maxima.Guard -> JSX.jsx "<MaximaBlackGuard/>"
                 | Maxima.Immobilizer -> JSX.jsx "<MaximaBlackImmobilizer/>"
                 | Maxima.King -> JSX.jsx "<MaximaBlackKing/>"
                 | Maxima.LongLeaper -> JSX.jsx "<MaximaBlackLongLeaper/>"
                 | Maxima.Mage -> JSX.jsx "<MaximaBlackMage/>"
                 | Maxima.Pawn -> JSX.jsx "<MaximaBlackPawn/>"
                 | Maxima.Withdrawer -> JSX.jsx "<MaximaBlackWithdrawer/>"
             | White ->
                 match pieceType with
                 | Maxima.Chameleon -> JSX.jsx "<MaximaWhiteChameleon/>"
                 | Maxima.Coordinator -> JSX.jsx "<MaximaWhiteCoordinator/>"
                 | Maxima.Guard -> JSX.jsx "<MaximaWhiteGuard/>"
                 | Maxima.Immobilizer -> JSX.jsx "<MaximaWhiteImmobilizer/>"
                 | Maxima.King -> JSX.jsx "<MaximaWhiteKing/>"
                 | Maxima.LongLeaper -> JSX.jsx "<MaximaWhiteLongLeaper/>"
                 | Maxima.Mage -> JSX.jsx "<MaximaWhiteMage/>"
                 | Maxima.Pawn -> JSX.jsx "<MaximaWhitePawn/>"
                 | Maxima.Withdrawer -> JSX.jsx "<MaximaWhiteWithdrawer/>"
            }
        </>
    """
