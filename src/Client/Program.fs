module UltimatelySafe.Program

open Browser

open Fable.Core.JsInterop
open Feliz.JSX.Solid

open UltimatelySafe.Components.Ext.SolidRouter
open UltimatelySafe.Home
open UltimatelySafe.LocalGame
open UltimatelySafe.OnlineRoom

importSideEffects "./App.scss"

Solid.render((fun () ->
    Solid.Router(
        ``base`` = "/app",
        children = Html.fragment [
            Solid.Route(path = "/local", ``component`` = LocalGame)
            Solid.Route(path = $"/room/:{RoomName}", ``component`` = OnlineRoom)
            Solid.Route(path = "/", ``component`` = Home)
            Solid.Route(path = "*", ``component`` = (fun () -> Solid.Navigate(href = "/")))
        ]
    )
), document.getElementById("solid-app"))
