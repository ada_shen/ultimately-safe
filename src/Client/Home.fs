module UltimatelySafe.Home

open Fable.Core
open Fable.Core.JsInterop
open Feliz.JSX.Solid

open UltimatelySafe.JoinRoom

let private classNames : CssModules.Home = importDefault "./Home.module.scss"

[<AutoOpen>]
type Home =
    [<JSX.Component>]
    static member Home() =
        Html.main [
            Attr.className classNames.homePage
            Html.children [
                JoinRoom()
            ]
        ]
