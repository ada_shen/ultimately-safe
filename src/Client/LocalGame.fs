module UltimatelySafe.LocalGame

open Fable.Core
open Feliz.JSX.Solid

open UltimatelySafe.Game.Maxima
open UltimatelySafe.Game.Rococo
open UltimatelySafe.Game.Ultima
open UltimatelySafe.Shared.Game

/// View component for playing a local game
[<JSX.Component>]
let LocalGame() =
    let perspective = White

    let (gameType, setGameType) = Solid.createSignal(Ultima)

    Html.fragment [
        match gameType() with
        | Ultima ->
            UltimaGame(perspective = perspective, singlePlayer = false, changeGameType = setGameType)
        | Rococo ->
            RococoGame(perspective = perspective, singlePlayer = false, changeGameType = setGameType)
        | Maxima ->
            MaximaGame(perspective = perspective, singlePlayer = false, changeGameType = setGameType)
    ]
