module UltimatelySafe.Components.Ext.SolidDnd

open Browser.Types
open Fable.Core

open UltimatelySafe.ClientUtils.JsTypes

type Id = U2<string, int>

//I believe all of these types are meant to be treated as immutable and simply not declared as such in TypeScript.
//So using anonymous record types to save on keystrokes until I discover otherwise.

//I may have gone slightly overboard with writing bindings for properties I'm not going to end up using,
//but these were my first Fable bindings and it was good exercise, lol

type Coordinates = {|x : float; y : float|}

type Point = {|x : float; y : float|}

type Transform = {|x : float; y : float|}

//This one is left as an interface since it's a class that's not directly exported, rather than a TS interface
type Layout =
    abstract x : float
    abstract y : float
    abstract width : float
    abstract height : float
    abstract rect : {|x : float; y : float; width : float; height : float|}
    abstract left : float
    abstract right : float
    abstract top : float
    abstract bottom : float
    abstract center : Point
    abstract corners : {|topLeft : Point; topRight : Point; bottomRight : Point; bottomLeft : Point|}

//The TypeScript type is generic over an event name. No way to mimic that in F#.
type SensorActivator = Event * Id -> unit

type Sensor = {|
    id : Id
    //In TS this is a mapping from event names to typed handlers.
    //Not only can I not truly replicate that, but I'm not gonna go write a giant union of event names, either.
    activators : Record<string, SensorActivator>
    coordinates : {|
        origin : Coordinates
        current : Coordinates
        delta : Coordinates
    |}
|}

type TransformerCallback = Transform -> Transform

type Transformer = {|
    id : Id
    order : float
    callback : TransformerCallback
|}

type Item = {|
    id : Id
    node : HTMLElement
    layout : Layout
    data : Record<string, obj>
    transformers : Record<Id, Transformer>
    transform : Transform
    transformed : Layout
|}

type Draggable = Item

type Droppable = Item

type Overlay = Item

type DragEvent = {|
    draggable : Draggable
    droppable : Droppable option
    overlay : Overlay option
|}

type DragEventHandler = DragEvent -> unit

type CollisionDetector = Draggable * Droppable[] * {|activeDroppableId : Id option|} -> Droppable option

type Listeners = Record<string, Event -> unit>

type DraggableDirective =
    [<Emit("$0($1...)")>]
    abstract Invoke : element : HTMLElement * ?accessor : (unit -> {|skipTransform : bool|}) -> unit
    abstract ref<'T when 'T :> HTMLElement> : 'T option -> 'T
    abstract isActiveDraggable : bool
    abstract dragActivators : Listeners
    abstract transform : Transform

type DroppableDirective =
    [<Emit("$0($1...)")>]
    abstract Invoke : element : HTMLElement * ?accessor : (unit -> {|skipTransform : bool|}) -> unit
    abstract ref<'T when 'T :> HTMLElement> : 'T option -> 'T
    abstract isActiveDroppable : bool
    abstract transform : Transform

[<AutoOpen>]
type SolidDnd =
    [<ImportMember(from = "@thisbeyond/solid-dnd")>]
    [<JSX.Component>]
    static member DragDropProvider(
        children : JSX.Element,
        ?onDragStart : DragEventHandler,
        ?onDragMove : DragEventHandler,
        ?onDragOver : DragEventHandler,
        ?onDragEnd : DragEventHandler,
        ?collisionDetector : CollisionDetector
    ) : JSX.Element = jsNative

    [<ImportMember(from = "@thisbeyond/solid-dnd")>]
    [<JSX.Component>]
    static member DragDropSensors(?children : JSX.Element) : JSX.Element = jsNative

    [<ImportMember(from = "@thisbeyond/solid-dnd")>]
    static member createDraggable(id : Id, ?data : Record<string, obj>) : DraggableDirective = jsNative

    [<ImportMember(from = "@thisbeyond/solid-dnd")>]
    static member createDroppable(id : Id, ?data : Record<string, obj>) : DroppableDirective = jsNative

    //I'm only writing a binding for this so I can check for the context, not use it,
    //so I'm not bothering to actually write bindings for the huge DragDropContext type.
    [<ImportMember(from = "@thisbeyond/solid-dnd")>]
    static member useDragDropContext() : obj = jsNative
