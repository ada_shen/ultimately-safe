module UltimatelySafe.Components.Ext.SolidRouter

//See https://github.com/dotnet/fsharp/issues/8221
#nowarn "1189"

open Browser.Types
open Fable.Core
open System
open System.Text.RegularExpressions

open UltimatelySafe.ClientUtils.JsTypes

type NavigateOptions = {|
    resolve : bool
    replace : bool
    scroll : bool
    state : obj
|}

//Is there a better way to bind a multi-argument JS function type than using an awkward C# delegate type?
// `string * NavigateOptions -> unit` produces `navigate(["path", opts])`
// and `string -> NavigateOptions -> unit` produces `navigate("path")(opts)`.
type NavigateFunction = Func<string, NavigateOptions, unit>

let defaultNavigateOptions = {|resolve = true; replace = false; scroll = true; state = undefined|}

[<Erase>]
type MatchFilter =
    | Enum of string[]
    | Pattern of Regex
    | Predicate of (string -> bool)

[<StringEnum>]
type RouteLoadIntent =
    | Initial
    | Navigate
    | Native
    | Preload

type RouteLoadParams<'RouteParam> =
    [<Emit("$0.params")>]
    abstract member routeParams : Record<'RouteParam, string>
    abstract member location : Location
    abstract member intent : RouteLoadIntent

type RouteLoadFunction<'RouteParam, 'T> = RouteLoadParams<'RouteParam> -> 'T

type PathMatch<'RouteParam> =
    [<Emit("$0.params")>]
    abstract member routeParams : Record<'RouteParam, string>
    abstract member path : string

type BeforeLeaveEventArgs =
    abstract member from : Location
    //NOTE: The docs say that this can also be a number, but reading the source code it never seems like it actually is
    abstract member ``to`` : string
    abstract member options: NavigateOptions
    abstract member preventDefault : unit -> unit
    abstract member defaultPrevented : bool
    abstract member retry : ?force : bool -> unit

type Solid with
    [<ImportMember(from = "@solidjs/router")>]
    [<JSX.Component>]
    static member Router(
        //Not binding config-based routing as I don't plan to use it
        children : JSX.Element,
        ?root : JSX.Element -> JSX.Element,
        ?``base`` : string,
        ?actionBase : string,
        ?preload : bool,
        ?explicitLinks : bool
    ) : JSX.Element = jsNative

    [<ImportMember(from = "@solidjs/router")>]
    [<JSX.Component>]
    static member A(
        href : string,
        ?noScroll : bool,
        ?replace : bool,
        ?state : obj,
        ?inactiveClass : string,
        ?activeClass : string,
        ?``end`` : bool,
        ?onClick : MouseEvent -> unit,
        ?children : JSX.Element
    ) : JSX.Element = jsNative

    [<ImportMember(from = "@solidjs/router")>]
    [<JSX.Component>]
    static member Navigate(href : string) : JSX.Element = jsNative

    [<ImportMember(from = "@solidjs/router")>]
    [<JSX.Component>]
    static member Navigate(
        href : {|navigate : NavigateFunction; location : Location|} -> string
    ) : JSX.Element = jsNative

    [<ImportMember(from = "@solidjs/router")>]
    [<JSX.Component>]
    static member Route<'RouteParam, 'Data>(
        path : string,
        ?``component`` : 'Data -> JSX.Element,
        ?matchFilters : Record<'RouteParam, MatchFilter>,
        ?children : JSX.Element,
        ?load : RouteLoadFunction<'RouteParam, 'Data>
    ) : JSX.Element = jsNative

    [<ImportMember(from = "@solidjs/router")>]
    static member useParams<'K>() : Record<'K, string> = jsNative

    [<ImportMember(from = "@solidjs/router")>]
    static member useNavigate() : NavigateFunction = jsNative

    [<ImportMember(from = "@solidjs/router")>]
    static member useLocation() : Location = jsNative

    [<ImportMember(from = "@solidjs/router")>]
    static member useSearchParams() : Record<string, string> * (obj -> unit) = jsNative

    [<ImportMember(from = "@solidjs/router")>]
    static member useIsRouting() : unit -> bool = jsNative

    [<ImportMember(from = "@solidjs/router")>]
    static member useMatch<'RouteParam>(
        path : unit -> string,
        ?matchFilters : Record<'RouteParam, MatchFilter>
    ) : PathMatch<'RouteParam> = jsNative

    [<ImportMember(from = "@solidjs/router")>]
    static member useBeforeLeave(cb : BeforeLeaveEventArgs -> unit) : unit = jsNative
