module UltimatelySafe.Game.Base

open Elmish
open Elmish.Solid
open Fable.Core
open Fable.Core.JS
open Fable.Core.JsInterop
open Feliz.JSX.Solid

open UltimatelySafe.ClientUtils.Focusable
open UltimatelySafe.ClientUtils.SolidExtensions
open UltimatelySafe.Components.Ext.SolidRouter
open UltimatelySafe.Game.CapturedPieceList
open UltimatelySafe.Shared.Game
open UltimatelySafe.Shared.Utils
open UltimatelySafe.Shared.Utils.Functions

let private classNames : CssModules.Game.Base = importDefault "./Base.module.scss"

type CapturedPieceRequest<'PieceType when 'PieceType :> IPieceType> =
    {
        Predicate : 'PieceType * int -> bool
        Callback : 'PieceType option -> unit
    }

type GameBaseState<'PieceType when 'PieceType :> IPieceType> =
    {
        CapturedPieceRequest : CapturedPieceRequest<'PieceType> option
        HideBorder : bool
        FreeMovement : bool
        ShowInfoForPieceType : 'PieceType option
    }

type private Msg<'PieceType when 'PieceType :> IPieceType> =
    | RequestCapturedPiece of CapturedPieceRequest<'PieceType>
    | ClearCapturedPieceRequest
    | ToggleBorder
    | ToggleFreeMovement
    | ShowInfoForPieceType of 'PieceType option

let private init () =
    {
        CapturedPieceRequest = None
        HideBorder = false
        FreeMovement = false
        ShowInfoForPieceType = None
    }

let private update<'PieceType when 'PieceType :> IPieceType>
    (msg : Msg<'PieceType>)
    (state : GameBaseState<'PieceType>)
    =
    match msg with
    | RequestCapturedPiece capturedPieceRequest -> {state with CapturedPieceRequest = Some capturedPieceRequest}
    | ClearCapturedPieceRequest -> {state with CapturedPieceRequest = None}
    | ToggleBorder -> {state with HideBorder = not state.HideBorder}
    | ToggleFreeMovement -> {state with FreeMovement = not state.FreeMovement}
    | ShowInfoForPieceType pieceType -> {state with ShowInfoForPieceType = pieceType}

type private CaptureListPosition = Top | Bottom

type RenderBoardProps<'PieceType when 'PieceType :> IPieceType> =
    {
        FocusRef : Focusable ref
        ClassName : string
        OnSpaceFocused : BoardPosition -> Piece<'PieceType> option -> unit
        RequestCapturedPiece : ('PieceType * int -> bool) -> ('PieceType option -> unit) -> unit
    }

[<AutoOpen>]
type GameBase =
    [<JSX.Component>]
    static member GameBase<'PieceType when 'PieceType :> IPieceType and 'PieceType : equality>(
        perspective : Player,
        gameState : GameState<'PieceType>,
        onNewGame : GameType -> unit,
        undoMove : unit -> unit,
        redoMove : unit -> unit,
        renderBoard : GameBaseState<'PieceType> -> RenderBoardProps<'PieceType> -> JSX.Element,
        renderPieceInfo : 'PieceType -> JSX.Element,
        renderPieceGraphic : Player -> 'PieceType -> JSX.Element,
        ?classList : string list
    ) =
        let topCaptureList = Solid.makeRef<CapturedPieceListHandle>()
        let bottomCaptureList = Solid.makeRef<CapturedPieceListHandle>()
        let board = Solid.makeRef<Focusable>()

        let (state, dispatch) = Solid.createElmishStore(Program.mkSimple init update (fun _ _ -> ()))

        let requestCapturedPiece predicate callback =
            dispatch (RequestCapturedPiece {Predicate = predicate; Callback = callback})
            //Wait a tick for the capture list to become focusable
            setTimeout (fun () ->
                if gameState.Player = perspective then
                    topCaptureList.Value.Focus ()
                else
                    bottomCaptureList.Value.Focus ()
            ) 0 |> ignore

        let captureCounts player =
            gameState.CapturedPieces
            |> AssocArr.values
            |> Seq.filter (fun piece -> piece.Player = player)
            |> Seq.countBy (fun piece -> piece.Type)
            |> Seq.toArray

        let renderCapturedPieces position =
            let player =
                match position with
                | Top -> perspective
                | Bottom -> Player.oppositeTo perspective

            let selectMode() = state.CapturedPieceRequest.IsSome && player = gameState.Player

            let onSelectCapturedPiece selection =
                match state.CapturedPieceRequest with
                | Some {Callback = callback} ->
                    callback selection
                    dispatch ClearCapturedPieceRequest
                    board.Value.Focus ()
                | None -> ()

            Html.div [
                Attr.classList [
                    "UsGameBase-capturesContainer"
                    classNames.capturesContainer
                    match position with
                    | Top ->
                        "UsGameBase-capturesContainerTop"
                        classNames.top
                    | Bottom ->
                        "UsGameBase-capturesContainerBottom"
                        classNames.bottom
                ]
                Html.children [
                    CapturedPieceList(
                        player = player,
                        counts = captureCounts player,
                        selectMode = selectMode(),
                        onSelect = onSelectCapturedPiece,
                        onButtonFocused = (dispatch << ShowInfoForPieceType << Some),
                        ?selectPredicate = (state.CapturedPieceRequest |> Option.map (fun req -> req.Predicate)),
                        renderPieceGraphic = renderPieceGraphic,
                        handleRef = (
                            match position with
                            | Top -> topCaptureList
                            | Bottom -> bottomCaptureList
                        )
                    )
                ]
            ]

        Html.div [
            Attr.classList [
                yield! defaultArg classList []
                "UsGameBase-root"
                classNames.gameBase
            ]
            Html.children [
                Html.div [
                    Attr.className classNames.toolbar
                    Html.children [
                        Solid.A(href = "/", children = Html.text "Home")
                        Html.fieldSet [
                            Attr.style [Css.padding length.zero]
                            Html.children [
                                Html.legend "New Game"
                                Html.button [
                                    Attr.typeButton
                                    Ev.onClick (fun _ -> onNewGame Ultima)
                                    Html.children [
                                        Html.text "Ultima"
                                    ]
                                ]
                                Html.button [
                                    Attr.typeButton
                                    Ev.onClick (fun _ -> onNewGame Rococo)
                                    Html.children [
                                        Html.text "Rococo"
                                    ]
                                ]
                                Html.button [
                                    Attr.typeButton
                                    Ev.onClick (fun _ -> onNewGame Maxima)
                                    Html.children [
                                        Html.text "Maxima"
                                    ]
                                ]
                            ]
                        ]
                        Html.label [
                            //TODO: issue/PR for the fact that the children-only overload for Html.label produces <kbd>
                            Html.children [
                                Html.input [
                                    Attr.typeCheckbox
                                    Attr.isChecked state.HideBorder
                                    Ev.onClick (fun _ -> dispatch ToggleBorder)
                                ]
                                Html.text " Hide border"
                            ]
                        ]
                        Html.label [
                            Html.children [
                                Html.input [
                                    Attr.typeCheckbox
                                    Attr.isChecked state.FreeMovement
                                    Ev.onClick (fun _ -> dispatch ToggleFreeMovement)
                                ]
                                Html.text " Free movement"
                            ]
                        ]
                    ]
                ]

                Html.p [
                    Attr.classList [
                        classNames.turn
                        match gameState.Winner |> Option.defaultValue gameState.Player with
                        | White -> classNames.white
                        | Black -> classNames.black
                    ]
                    Html.children [
                        match gameState.Winner with
                        | Some winner -> Html.text (sprintf "%A wins!" winner)
                        | None -> Html.text (sprintf "Turn: %A" gameState.Player)
                    ]
                ]

                renderCapturedPieces Top
                renderBoard state {
                    FocusRef = board
                    ClassName = classNames.board
                    OnSpaceFocused = (fun _ maybePiece ->
                        match maybePiece with
                        | Some piece -> dispatch ^<| ShowInfoForPieceType ^<| Some piece.Type
                        | None -> dispatch (ShowInfoForPieceType None)
                    )
                    RequestCapturedPiece = requestCapturedPiece
                }
                renderCapturedPieces Bottom

                Html.aside [
                    Attr.className classNames.sidebar
                    Html.children [
                        //TODO: Eventually there will be a whole move history here...
                        Html.button [
                            Attr.typeButton
                            Attr.disabled (gameState.NextMoveIndex = 0)
                            Ev.onClick (fun _ -> undoMove ())
                            Html.children [
                                Html.text "Undo Move"
                            ]
                        ]
                        Html.button [
                            Attr.typeButton
                            Attr.disabled (gameState.NextMoveIndex = gameState.Moves.Length)
                            Ev.onClick (fun _ -> redoMove ())
                            Html.children [
                                Html.text "Redo Move"
                            ]
                        ]

                        Html.section [
                            Attr.className classNames.pieceInfoContainer
                            Attr.ariaLabel "Piece Info"
                            Html.children [
                                match state.ShowInfoForPieceType with
                                | Some pieceType ->
                                    renderPieceInfo pieceType
                                | None -> ()
                            ]
                        ]
                    ]
                ]
            ]
        ]
