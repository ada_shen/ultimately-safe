module UltimatelySafe.Game.CapturedPieceList

open Browser.Types
open Fable.Core
open Fable.Core.JsInterop
open Feliz.JSX.Solid

open UltimatelySafe.Board.Piece
open UltimatelySafe.ClientUtils.Focusable
open UltimatelySafe.ClientUtils.SolidExtensions
open UltimatelySafe.Shared.Game.Common

let private classNames : CssModules.Game.CapturedPieceList = importDefault "./CapturedPieceList.module.scss"

type CapturedPieceListHandle =
    inherit Focusable

[<AutoOpen>]
type CapturedPieceList =
    [<JSX.Component>]
    static member private CapturedPiece<'PieceType when 'PieceType :> IPieceType>(
        player : Player,
        pieceType : 'PieceType,
        count : int,
        buttonMode : bool,
        buttonDisabled : bool,
        renderPieceGraphic : Player -> 'PieceType -> JSX.Element,
        onButtonClick : 'PieceType -> unit,
        ?onButtonFocused : 'PieceType -> unit
    ) =
        let renderPiece () =
            DisplayPiece(player = player, pieceType = pieceType, renderPieceGraphic = renderPieceGraphic)

        Html.button [
            Attr.classList [
                classNames.capturedPiece
                if buttonMode then classNames.buttonMode
                if count = 1 then classNames.single else classNames.multiple
            ]
            //Avoid setting disabled when not in button mode to avoid style weirdness
            Attr.disabled (buttonMode && buttonDisabled)
            Ev.onClick (fun _ -> if buttonMode then onButtonClick pieceType)
            Ev.onFocus (fun _ ->
                match onButtonFocused with
                | Some handler -> handler pieceType
                | None -> ()
            )
            Html.children [
                Html.div [
                    Attr.className classNames.capturedPieceContent
                    Html.children [
                        renderPiece ()
                        //If there's multiple pieces of this type in the list then render two pieces for a stack effect
                        if count > 1 then
                            renderPiece ()
                        Html.div [
                            Attr.className classNames.count
                            Html.children [
                                Html.text count
                            ]
                        ]
                    ]
                ]
            ]
        ]

    [<JSX.Component>]
    static member CapturedPieceList<'PieceType when 'PieceType :> IPieceType>(
        player : Player,
        counts : ('PieceType * int)[],
        selectMode : bool,
        onSelect : 'PieceType option -> unit,
        renderPieceGraphic : Player -> 'PieceType -> JSX.Element,
        ?onButtonFocused : 'PieceType -> unit,
        ?selectPredicate : 'PieceType * int -> bool,
        ?classList : string list,
        ?handleRef : CapturedPieceListHandle ref
    ) =
        let pieceTypeSelectable typeAndCount = typeAndCount |> defaultArg selectPredicate (fun _ -> true)

        let cancelButton = Solid.makeRef<HTMLButtonElement>()

        match handleRef with
        | Some handleRef ->
            handleRef.Value <- {new CapturedPieceListHandle with
                override _.Focus () = cancelButton.Value.focus()
            }
        | None -> ()

        Html.ul [
            Attr.roleList
            Attr.classList [
                yield! defaultArg classList []
                classNames.capturedPieceList
                if selectMode then classNames.selectMode
            ]
            Html.children [
                if selectMode then
                    Html.button [
                        Solid.ref cancelButton
                        Attr.className classNames.cancelButton
                        Ev.onClick (fun _ -> onSelect None)
                        Html.children [
                            Html.text "✕"
                        ]
                    ]
                Solid.For(counts, fun (pieceType, count) _ ->
                    CapturedPiece(
                        player = player,
                        pieceType = pieceType,
                        count = count,
                        buttonMode = selectMode,
                        buttonDisabled = not (pieceTypeSelectable (pieceType, count)),
                        renderPieceGraphic = renderPieceGraphic,
                        onButtonClick = (onSelect << Some),
                        ?onButtonFocused = onButtonFocused
                    )
                )
            ]
        ]
