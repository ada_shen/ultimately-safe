module UltimatelySafe.Game.Ultima

open Elmish
open Elmish.Solid
open Fable.Core
open Feliz.JSX.Solid

open UltimatelySafe.Board.Piece
open UltimatelySafe.Board.Pieces
open UltimatelySafe.Board.Ultima
open UltimatelySafe.Game.Base
open UltimatelySafe.Game.PieceInfo
open UltimatelySafe.Shared.Game

type private Msg =
    | NewGame
    | ApplyMove of MoveWithInputs
    | UndoMove
    | RedoMove
    | SelfCaptureImmobilizedPiece of Ultima.Position

let private init () = Ultima.startingState ()

let private update (msg : Msg) (gameState : Ultima.State) =
    match msg with
    | NewGame -> Ultima.startingState ()
    | ApplyMove (MoveWithInputs (move, _, _, _)) -> Ultima.applyMove gameState move
    | UndoMove -> Ultima.undoMove gameState
    | RedoMove -> Ultima.redoMove gameState
    | SelfCaptureImmobilizedPiece pos -> Ultima.selfCaptureImmobilizedPiece gameState pos

let private pieceDescription (pieceType : Ultima.PieceType) =
    match pieceType with
    | Ultima.King ->
        //Another good use for the JSX helper: text formatting
        JSX.jsx $"""
            <p>
                The King in Ultima moves and captures identically to the King in traditional Chess.
                It can move one space in any direction and captures by displacement (landing on an enemy piece).
                The King cannot land on friendly pieces. There is no castling in Ultima.
                The game is won by capturing the enemy King.
            </p>
        """
    | Ultima.Withdrawer ->
        JSX.jsx $"""
            <p>
                The Withdrawer can move any number of spaces in any direction.
                It cannot move on or through other pieces.
                To capture, the Withdrawer must start its move adjacent to an enemy piece,
                and then move any number of squares <em>directly away</em> from that piece.
                For example, to capture a piece one square above it, the Withdrawer must move straight down.
            </p>
        """
    | Ultima.Chameleon ->
        JSX.jsx $"""
            <>
                <p>
                    The Chameleon can move any number of spaces in any direction.
                    The Chameleon interacts with other pieces by mimicking them.
                </p>
                <ul>
                    <li>The Chameleon can leap over one or more Long Leapers to capture them.</li>
                    <li>The Chameleon can coordinate with its King to capture the enemy Coordinator.</li>
                    <li>The Chameleon can capture Withdrawers by withdrawing from them.</li>
                    <li>
                        A Chameleon standing next to an enemy Immobilizer causes both pieces to be frozen
                        (either piece can of course choose to self-destruct).
                    </li>
                    <li>
                        The Chameleon can capture Pawns by pinching them,
                        <em>but only with an orthogonal move, as Pawns cannot move diagonally.</em>
                    </li>
                    <li>
                        The Chameleon can capture the enemy King by displacement,
                        <em>but only from one square away.</em>
                    </li>
                    <li>Chameleons cannot capture other Chameleons.</li>
                </ul>
                <p>If a move satisfies the conditions for capturing pieces of multiple types, all captures occur.</p>
            </>
        """
    | Ultima.LongLeaper ->
        JSX.jsx $"""
            <p>
                The Long Leaper can move any number of spaces in any direction.
                The Long Leaper can "leap" over one or more enemy pieces in a straight line to capture them.
                Leaping is allowed when there is at least one empty square after each piece in the sequence.
                There can be multiple squares between leaped pieces and you can land more than one square
                after the last piece you capture.The Long Leaper cannot jump over friendly pieces,
                or any piece that does not have an empty square behind it.
            </p>
        """
    | Ultima.Coordinator ->
        JSX.jsx $"""
            <p>
                The Coordinator can move any number of spaces in any direction.
                It cannot move on or through other pieces.
                When the coordinator moves, it captures any enemy pieces located at the intersections
                of the rank and file at which it landed, and the rank and file of its King.
                For example, if a Coordinator lands at C3 and its king is at A1, then it would capture any enemy pieces
                at either C1 or A3.
            </p>
        """
    | Ultima.Immobilizer ->
        JSX.jsx $"""
            <p>
                The Immobilizer can move any number of spaces in any direction.
                It cannot move on or through other pieces.
                Any enemy pieces that start their turn adjacent to the Immobilizer cannot move.
                However, an immobilized piece may choose to capture itself (or "self-destruct") as its move.
                If two opposing Immobilizers meet, then neither may move again until one is captured.
            </p>
        """
    | Ultima.Pawn ->
        JSX.jsx $"""
            <p>
                The Pawns in Ultima are also known as Pincing Pawns.
                These Pincing Pawns can move any number of spaces orthogonally.
                They cannot move on or through other pieces.
                When a Pawn ends its move, it captures any enemy pieces that are orthogonally adjacent,
                with another allied piece immediately beyond. In other words, Pawns capture by "pinching"
                enemy pieces between themselves and other allied pieces.
                For example, if a pawn has an enemy piece on B4 and an allied piece on C4,
                it can capture that enemy piece by moving to A4.
            </p>
        """

[<AutoOpen>]
type UltimaGame =
    [<JSX.Component>]
    static member UltimaGame(perspective : Player, singlePlayer : bool, changeGameType : GameType -> unit) =
        let (gameState, dispatch) = Solid.createElmishStore(Program.mkSimple init update (fun _ _ -> ()))

        let makeFreeMove fromPos toPos =
            let move = GameState.createMove gameState fromPos (fun pieceId -> [|Relocate (pieceId, fromPos, toPos)|])
            MoveWithInputs (move, fromPos, toPos, [])

        let onNewGame gameType =
            match gameType with
            | Ultima -> dispatch NewGame
            | otherType -> changeGameType otherType

        GameBase(
            perspective = perspective,
            gameState = gameState,
            onNewGame = onNewGame,
            undoMove = (fun () -> dispatch UndoMove),
            redoMove = (fun () -> dispatch RedoMove),
            renderPieceGraphic = renderUltimaPiece,
            renderBoard = (fun baseState boardProps ->
                UltimaBoard(
                    focusRef = boardProps.FocusRef,
                    gameState = gameState,
                    makeMove = (if baseState.FreeMovement then makeFreeMove else Ultima.makeMove gameState),
                    applyMove = (dispatch << ApplyMove),
                    selfCaptureImmobilizedPiece = (dispatch << SelfCaptureImmobilizedPiece),
                    perspective = perspective,
                    singlePlayer = singlePlayer,
                    classList = [boardProps.ClassName],
                    hideBorder = baseState.HideBorder,
                    freeMovement = baseState.FreeMovement,
                    onSpaceFocused = (fun pos maybePiece -> boardProps.OnSpaceFocused pos maybePiece)
                )
            ),
            renderPieceInfo = (fun pieceType ->
                let pieceType' = pieceType :> IPieceType
                PieceInfo(
                    display = DisplayPiece(
                        player = gameState.Player,
                        pieceType = pieceType,
                        renderPieceGraphic = renderUltimaPiece
                    ),
                    title = $"{pieceType'.Name} ({pieceType'.Abbreviation})",
                    body = Html.fragment [
                        pieceDescription pieceType
                        JSX.jsx $"""
                            <p>
                                <a
                                    href="https://www.chessvariants.com/other.dir/ultimapieces.html"
                                    target="_blank"
                                    rel="noreferrer"
                                >
                                    More info on Ultima pieces (opens in new tab)
                                </a>
                            </p>
                        """
                    ]
                )
            )
        )
