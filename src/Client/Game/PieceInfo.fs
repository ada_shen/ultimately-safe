module UltimatelySafe.Game.PieceInfo

open Fable.Core
open Fable.Core.JsInterop
open Feliz.JSX.Solid

let private classNames : CssModules.Game.PieceInfo = importDefault "./PieceInfo.module.scss"

[<AutoOpen>]
type PieceInfo =
    /// <summary>Simple presentational skeleton for displaying info about a piece</summary>
    /// <param name="display">
    ///     Generally an image of the piece. PRESENTATIONAL. Will be aria-hidden. Placed in a square container.
    /// </param>
    /// <param name="title">Generally the name and abbreviation of the piece. Used as the article header.</param>
    /// <param name="body">The main content of the description</param>
    [<JSX.Component>]
    static member PieceInfo(display : JSX.Element, title : string, body : JSX.Element) =
        Html.article [
            Attr.classList ["UsPieceInfo-root"; classNames.pieceInfo]
            Html.children [
                Html.div [
                    Attr.classList ["UsPieceInfo-display"; classNames.display]
                    //Whatever gets put in this div is essentially a presentational image.
                    //Setting aria-hidden here because the use of xml-files-as-jsx-components
                    //makes doing it for the actual image kinda difficult via Fable.
                    Attr.ariaHidden true
                    Html.children [
                        display
                    ]
                ]
                Html.header [
                    Html.children [
                        Html.text title
                    ]
                ]
                body
            ]
        ]
