module UltimatelySafe.Game.Maxima

open Elmish
open Elmish.Solid
open Fable.Core
open Fable.Core.JsInterop
open Feliz.JSX.Solid

open UltimatelySafe.Board.Piece
open UltimatelySafe.Board.Pieces
open UltimatelySafe.Board.Maxima
open UltimatelySafe.Game.Base
open UltimatelySafe.Game.PieceInfo
open UltimatelySafe.Shared.Game

let private classNames : CssModules.Game.Maxima = importDefault "./Maxima.module.scss"

type private Msg =
    | NewGame
    | ApplyMove of MoveWithInputs
    | UndoMove
    | RedoMove
    | SelfCaptureImmobilizedPiece of Maxima.Position

let private init () = Maxima.startingState ()

let private update (msg : Msg) (gameState : Maxima.State) =
    match msg with
    | NewGame -> Maxima.startingState ()
    | ApplyMove (MoveWithInputs (move, _, _, _)) -> Maxima.applyMove gameState move
    | UndoMove -> Maxima.undoMove gameState
    | RedoMove -> Maxima.redoMove gameState
    | SelfCaptureImmobilizedPiece pos -> Maxima.selfCaptureImmobilizedPiece gameState pos

let private pieceDescription (pieceType : Maxima.PieceType) =
    match pieceType with
    | Maxima.Pawn ->
        JSX.jsx $"""
            <p>
                The Pawns in Maxima are also known as Pincing Pawns.
                These Pincing Pawns can move any number of spaces orthogonally.
                They cannot move on or through other pieces.
                When a Pawn ends its move, it captures any enemy pieces that are orthogonally adjacent,
                with another allied piece immediately beyond. In other words, Pawns capture by "pinching"
                enemy pieces between themselves and other allied pieces.
                For example, if a pawn has an enemy piece on B4 and an allied piece on C4,
                it can capture that enemy piece by moving to A4.
            </p>
        """
    | Maxima.Guard ->
        JSX.jsx $"""
            <p>
                The Guard in Ultima moves and captures identically to the King in traditional Chess.
                It can move one space in any direction and captures by displacement (landing on an enemy piece).
                The Guard cannot land on friendly pieces.
            </p>
        """
    | Maxima.LongLeaper ->
        JSX.jsx $"""
            <p>
                The Long Leaper can move any number of spaces in any direction.
                The Long Leaper can "leap" over one or more enemy pieces in a straight line to capture them.
                Leaping is allowed when there is at least one empty square after each piece in the sequence.
                There can be multiple squares between leaped pieces and you can land more than one square
                after the last piece you capture.The Long Leaper cannot jump over friendly pieces,
                or any piece that does not have an empty square behind it.
            </p>
        """
    | Maxima.Mage ->
        JSX.jsx $"""
            <p>
                The Mage has a multi-step style of movement in which it first moves one square diagonally,
                and then zero or more squares orthogonally. Unlike a Chess Knight, no "jumping" occurs,
                meaning that a piece blocking the initial diagonal move prevents the Mage from moving to
                any of the spaces beyond it.
                The Mage captures by displacement and <em>is immune to the Immobilizer.</em>
            </p>
        """
    | Maxima.Chameleon ->
        JSX.jsx $"""
            <>
                <p>
                    The Chameleon can move any number of spaces in any direction.
                    The Chameleon interacts with other pieces by mimicking them.
                </p>
                <ul>
                    <li>The Chameleon can leap over one or more Long Leapers to capture them.</li>
                    <li>The Chameleon can coordinate with its King to capture the enemy Coordinator.</li>
                    <li>The Chameleon can capture Withdrawers by withdrawing from them.</li>
                    <li>
                        A Chameleon standing next to an enemy Immobilizer causes both pieces to be frozen
                        (either piece can of course choose to self-destruct).
                    </li>
                    <li>
                        The Chameleon can capture Pawns by pinching them,
                        <em>but only with an orthogonal move, as Pawns cannot move diagonally.</em>
                    </li>
                    <li>
                        The Chameleon can capture Guards by displacement, <em>but only from one square away.</em>
                    </li>
                    <li>
                        The Chameleon can also capture Mages and Kings by displacement from one square away,
                        rather than by mimicking their more unique movement.
                    </li>
                    <li>Chameleons cannot capture other Chameleons.</li>
                </ul>
                <p>If a move satisfies the conditions for capturing pieces of multiple types, all captures occur.</p>
            </>
        """
    | Maxima.Immobilizer ->
        JSX.jsx $"""
            <p>
                The Immobilizer can move any number of spaces in any direction.
                It cannot move on or through other pieces.
                Any enemy pieces that start their turn adjacent to the Immobilizer cannot move.
                However, an immobilized piece may choose to capture itself (or "self-destruct") as its move.
                If two opposing Immobilizers meet, then neither may move again until one is captured.
            </p>
        """
    | Maxima.Coordinator ->
        JSX.jsx $"""
            <p>
                The Coordinator can move any number of spaces in any direction.
                It cannot move on or through other pieces.
                When the coordinator moves, it captures any enemy pieces located at the intersections
                of the rank and file at which it landed, and the rank and file of its King.
                For example, if a Coordinator lands at C3 and its king is at A1, then it would capture any enemy pieces
                at either C1 or A3.
            </p>
        """
    | Maxima.Withdrawer ->
        JSX.jsx $"""
            <p>
                The Withdrawer can move any number of spaces in any direction.
                It cannot move on or through other pieces.
                To capture, the Withdrawer must start its move adjacent to an enemy piece,
                and then move any number of squares <em>directly away</em> from that piece.
                For example, to capture a piece one square above it, the Withdrawer must move straight down.
            </p>
        """
    | Maxima.King ->
        JSX.jsx $"""
            <p>
                The King in Maxima moves and captures like a Chess Knight: by "jumping" in an "L" shape
                (one space in one direction followed by two spaces in another) and capturing enemy pieces by
                landing on them. In addition, the Maxima King can "wrap" around the far left and right edges
                of the board. For example, a King on the A file can move to the G or H file.
            </p>
        """

[<AutoOpen>]
type MaximaGame =
    [<JSX.Component>]
    static member MaximaGame(perspective : Player, singlePlayer : bool, changeGameType : GameType -> unit) =
        let (gameState, dispatch) = Solid.createElmishStore(Program.mkSimple init update (fun _ _ -> ()))

        let makeFreeMove fromPos toPos =
            let move = GameState.createMove gameState fromPos (fun pieceId -> [|Relocate (pieceId, fromPos, toPos)|])
            MoveWithInputs (move, fromPos, toPos, [])

        let onNewGame gameType =
            match gameType with
            | Maxima -> dispatch NewGame
            | otherType -> changeGameType otherType

        GameBase(
            classList = [classNames.maximaGame],
            perspective = perspective,
            gameState = gameState,
            onNewGame = onNewGame,
            undoMove = (fun () -> dispatch UndoMove),
            redoMove = (fun () -> dispatch RedoMove),
            renderPieceGraphic = renderMaximaPiece,
            renderBoard = (fun baseState boardProps ->
                MaximaBoard(
                    focusRef = boardProps.FocusRef,
                    gameState = gameState,
                    makeMove = (if baseState.FreeMovement then makeFreeMove else Maxima.makeMove gameState),
                    applyMove = (dispatch << ApplyMove),
                    selfCaptureImmobilizedPiece = (dispatch << SelfCaptureImmobilizedPiece),
                    perspective = perspective,
                    singlePlayer = singlePlayer,
                    classList = [boardProps.ClassName],
                    hideBorder = baseState.HideBorder,
                    freeMovement = baseState.FreeMovement,
                    onSpaceFocused = (fun pos maybePiece -> boardProps.OnSpaceFocused pos maybePiece)
                )
            ),
            renderPieceInfo = (fun pieceType ->
                let pieceType' = pieceType :> IPieceType
                PieceInfo(
                    display = DisplayPiece(
                        player = gameState.Player,
                        pieceType = pieceType,
                        renderPieceGraphic = renderMaximaPiece
                    ),
                    title = $"{pieceType'.Name} ({pieceType'.Abbreviation})",
                    body = Html.fragment [
                        pieceDescription pieceType
                        JSX.jsx $"""
                            <p>
                                <a
                                    href="https://www.chessvariants.com/dpieces.dir/maxima/maxima.html"
                                    target="_blank"
                                    rel="noreferrer"
                                >
                                    More info on Maxima pieces (opens in new tab)
                                </a>
                            </p>
                        """
                    ]
                )
            )
        )
