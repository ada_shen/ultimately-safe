module UltimatelySafe.Game.Rococo

open Elmish
open Elmish.Solid
open Fable.Core
open Feliz.JSX.Solid

open UltimatelySafe.Board.Piece
open UltimatelySafe.Board.Pieces
open UltimatelySafe.Board.Rococo
open UltimatelySafe.Game.Base
open UltimatelySafe.Game.PieceInfo
open UltimatelySafe.Shared.Game
open UltimatelySafe.Shared.Utils.Functions
open UltimatelySafe.Shared.Utils.Tuples

type private Msg =
    | NewGame
    | ApplyMove of MoveWithInputs
    | UndoMove
    | RedoMove
    | SelfCaptureImmobilizedPiece of Rococo.Position
    | MutualCapture of Rococo.Position * Rococo.Position

let private init () = Rococo.startingState ()

let private update (msg : Msg) (gameState : Rococo.State) =
    match msg with
    | NewGame -> Rococo.startingState ()
    | ApplyMove (MoveWithInputs (move, _, _, _)) -> Rococo.applyMove gameState move
    | UndoMove -> Rococo.undoMove gameState
    | RedoMove -> Rococo.redoMove gameState
    | SelfCaptureImmobilizedPiece pos -> Rococo.selfCaptureImmobilizedPiece gameState pos
    | MutualCapture (fromPos, toPos) -> Rococo.mutualCapture gameState fromPos toPos

let private pieceDescription (pieceType : Rococo.PieceType) =
    match pieceType with
    | Rococo.Advancer ->
        JSX.jsx $"""
            <>
                <p>
                    The Advancer can move any number of spaces in any direction.
                    It cannot move on or through other pieces.
                    The Advancer captures an enemy piece by stopping one square before it.
                    For example, if an Advancer moves from A3 to B4, it will capture an enemy piece at C5.
                </p>
                <p>
                    The Advancer can only enter the border squares by being swapped there.
                    Once on the border, it can make capturing moves along it.
                </p>
            </>
        """
    | Rococo.LongLeaper ->
        JSX.jsx $"""
            <>
                <p>
                    The Long Leaper can move any number of spaces in any direction.
                    The Long Leaper can "leap" over one or more enemy pieces in a straight line to capture them.
                    Leaping is allowed when there is at least one empty square after each piece in the sequence.
                    There can be multiple squares between leaped pieces and you can land more than one square
                    after the last piece you capture.The Long Leaper cannot jump over friendly pieces,
                    or any piece that does not have an empty square behind it.
                </p>
                <p>
                    The Long Leaper can enter the border squares by capturing.
                    When landing in the border to capture, the Long Leaper must end its move on the first square
                    after the last captured piece.
                </p>
            </>
        """
    | Rococo.Swapper ->
        JSX.jsx $"""
            <>
                <p>
                    The Swapper can move any number of spaces in any direction.
                    The Swapper can land on any piece, either friendly or enemy.
                    When it does so, that piece is "swapped" to the Swapper's previous position.
                    Instead of making a normal move, the Swapper can also choose to perform a "mutual capture",
                    in which it chooses a single adjacent enemy piece and captures both that piece <em>and itself.</em>
                </p>
                <p>
                    The Swapper may swap with a piece on a border square.
                    It may not enter the border via a normal move.
                </p>
            </>
        """
    | Rococo.Withdrawer ->
        JSX.jsx $"""
            <>
                <p>
                    The Withdrawer can move any number of spaces in any direction.
                    It cannot move on or through other pieces.
                    To capture, the Withdrawer must start its move adjacent to an enemy piece,
                    and then move any number of squares <em>directly away</em> from that piece.
                    For example, to capture a piece one square above it, the Withdrawer must move straight down.
                </p>
                <p>
                    The Withdrawer may land in a border square only via a one-square-long capturing move.
                    (E.g., it may land on X3 to capture a piece on B3 or X1 but not C3 or X0.)
                </p>
            </>
        """
    | Rococo.Immobilizer ->
        JSX.jsx $"""
            <>
                <p>
                    The Immobilizer can move any number of spaces in any direction.
                    It cannot move on or through other pieces.
                    Any enemy pieces that start their turn adjacent to the Immobilizer cannot move.
                    However, an immobilized piece may choose to capture itself (or "self-destruct") as its move.
                    If two opposing Immobilizers meet, then neither may move again until one is captured.
                </p>
                <p>
                    The Immobilizer can only enter the border squares by being swapped there.
                    An Immobilizer positioned on a border square can only move away from the border.
                </p>
            </>
        """
    | Rococo.Chameleon ->
        JSX.jsx $"""
            <>
                <p>
                    The Chameleon can move any number of spaces in any direction.
                    The Chameleon interacts with other pieces by mimicking them.
                </p>
                <ul>
                    <li>The Chameleon can leap over one or more Long Leapers to capture them.</li>
                    <li>The Chameleon can capture Withdrawers by withdrawing from them.</li>
                    <li>The Chameleon can capture Advancers by advancing up to them.</li>
                    <li>
                        The Chameleon can swap with Swappers of either color,
                        and may capture an adjacent enemy swapper via mutual destruction.
                    </li>
                    <li>
                        A Chameleon standing next to an enemy Immobilizer causes both pieces to be frozen
                        (either piece can of course choose to self-destruct).
                    </li>
                    <li>The Chameleon can capture Cannon Pawns by vaulting over an adjacent piece.</li>
                    <li>
                        The Chameleon can capture the enemy King by displacement,
                        <em>but only from one square away.</em>
                    </li>
                    <li>Chameleons cannot capture other Chameleons.</li>
                </ul>
                <p>If a move satisfies the conditions for capturing pieces of multiple types, all captures occur.</p>
                <p>Each type of mimicked capture follows its original piece's rules for landing in border squares.</p>
            </>
        """
    | Rococo.CannonPawn ->
        JSX.jsx $"""
            <>
                <p>
                    The pawns in Rococo are known as Cannon Pawns.
                    Cannon Pawns can <strong>either</strong> move one square in any direction,
                    <strong>or</strong> "vault" over an adjacent piece, landing in the square immediately beyond.
                    When performing a non-vaulting move, the Cannon Pawn cannot land in an occupied square.
                    When vaulting, the Cannon Pawn may land on an enemy piece, capturing it.
                </p>
                <p>A Cannon Pawn may land in a border square only via a capturing move.</p>
            </>
        """
    | Rococo.King ->
        JSX.jsx $"""
            <>
                <p>
                    The King in Rococo moves and captures identically to the King in traditional Chess.
                    It can move one space in any direction and captures by displacement (landing on an enemy piece).
                    The King cannot land on friendly pieces. There is no castling in Rococo.
                    The game is won by capturing the enemy King.
                </p>
                <p>The King may land in a border square only via a capturing move.</p>
            </>
        """

[<AutoOpen>]
type RococoGame =
    [<JSX.Component>]
    static member RococoGame(perspective : Player, singlePlayer : bool, changeGameType : GameType -> unit) =
        let (gameState, dispatch) = Solid.createElmishStore(Program.mkSimple init update (fun _ _ -> ()))

        let makeFreeMove fromPos toPos =
            let move = GameState.createMove gameState fromPos (fun pieceId -> [|Relocate (pieceId, fromPos, toPos)|])
            Complete <| MoveWithInputs (move, fromPos, toPos, [])

        let onNewGame gameType =
            match gameType with
            | Rococo -> dispatch NewGame
            | otherType -> changeGameType otherType

        GameBase(
            perspective = perspective,
            gameState = gameState,
            onNewGame = onNewGame,
            undoMove = (fun () -> dispatch UndoMove),
            redoMove = (fun () -> dispatch RedoMove),
            renderPieceGraphic = renderRococoPiece,
            renderBoard = (fun baseState boardProps ->
                RococoBoard(
                    focusRef = boardProps.FocusRef,
                    gameState = gameState,
                    makeMove = (if baseState.FreeMovement then makeFreeMove else Rococo.makeMove gameState),
                    applyMove = (dispatch << ApplyMove),
                    selfCaptureImmobilizedPiece = (dispatch << SelfCaptureImmobilizedPiece),
                    mutualCapture = (dispatch << MutualCapture <<+ pair),
                    requestCapturedPiece = boardProps.RequestCapturedPiece,
                    onSpaceFocused = (fun pos maybePiece -> boardProps.OnSpaceFocused pos maybePiece),
                    perspective = perspective,
                    singlePlayer = singlePlayer,
                    classList = [boardProps.ClassName],
                    hideBorder = baseState.HideBorder,
                    freeMovement = baseState.FreeMovement
                )
            ),
            renderPieceInfo = (fun pieceType ->
                let pieceType' = pieceType :> IPieceType
                PieceInfo(
                    display = DisplayPiece(
                        player = gameState.Player,
                        pieceType = pieceType,
                        renderPieceGraphic = renderRococoPiece
                    ),
                    title = $"{pieceType'.Name} ({pieceType'.Abbreviation})",
                    body = Html.fragment [
                        pieceDescription pieceType
                        JSX.jsx $"""
                            <p>
                                <a
                                    href="https://www.chessvariants.com/other.dir/rococo.dir/rococo-moves.html"
                                    target="_blank"
                                    rel="noreferrer"
                                >
                                    More info on Rococo pieces (opens in new tab)
                                </a>
                            </p>
                        """
                    ]
                )
            )
        )
