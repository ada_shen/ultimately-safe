module UltimatelySafe.JoinRoom

open Browser.Dom
open Browser.Types
open Fable.Core
open Fable.Core.JsInterop
open Feliz.JSX.Solid

open UltimatelySafe.ApiClient
open UltimatelySafe.Components.Ext.SolidRouter

let private classNames : CssModules.JoinRoom = importDefault "./JoinRoom.module.scss"

[<AutoOpen>]
type JoinRoom =
    [<JSX.Component>]
    static member JoinRoom(?classList : string list) =
        let navigate = Solid.useNavigate()

        let (roomName, setRoomName) = Solid.createSignal("")

        let onJoinRoom (ev : Event) =
            ev.preventDefault()
            async {
                match! apiClient.joinRoom (roomName()) with
                | Ok occupantCount ->
                    let occupantMessage =
                        $"""You are the {if occupantCount = 1 then "first" else "second"} user to join."""
                    window.alert($"Joining room {roomName()}! {occupantMessage}")
                    navigate.Invoke($"/room/{roomName()}", defaultNavigateOptions)
                | Error message ->
                    window.alert(message)
            } |> Async.Start

        Html.div [
            Attr.classList [
                yield! defaultArg classList []
                classNames.joinRoom
                "UsJoinRoom-root"
            ]
            Html.children [
                Solid.A(href = "/local", children = Html.text "Play Locally")
                Html.hr []
                Html.form [
                    Ev.onSubmit onJoinRoom
                    Html.children [
                        Html.h2 "Play Online"
                        Html.label [
                            Html.children [
                                Html.text "Room Name&nbsp;"
                                Html.input [
                                    Attr.typeText
                                    Attr.required true
                                    Attr.value (roomName())
                                    Ev.onTextInput setRoomName
                                ]
                            ]
                        ]
                        Html.button [
                            Attr.typeSubmit
                            Html.children [
                                Html.text "Join Room"
                            ]
                        ]
                    ]
                ]
            ]
        ]
