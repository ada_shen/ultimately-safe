module UltimatelySafe.ClientUtils.Focusable

/// Abstract interface for focusing a UI element
type Focusable =
    /// Focuses the element
    abstract member Focus : unit -> unit
