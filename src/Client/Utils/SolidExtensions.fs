module UltimatelySafe.ClientUtils.SolidExtensions

open Fable.Core

//Bindings for Solid APIs missing from the official bindings
type Solid with
    [<ImportMember("solid-js")>]
    static member createSelector<'T>(source : unit -> 'T) : 'T -> bool = jsNative

    [<ImportMember("solid-js")>]
    static member createSelector<'T, 'U>(source : unit -> 'T, comparator : 'U -> 'T -> bool) : 'U -> bool = jsNative

    /// Bit of a hack to support Solid's event handler data binding
    /// (See: https://www.solidjs.com/docs/latest/api#on___ )
    [<Emit("[$0, $1]")>]
    static member bindEv<'Ev, 'U>(handler : 'U -> 'Ev -> unit, data : 'U) : 'Ev -> unit = jsNative

    /// <summary>Similar to <see cref="createRef"/> but without the requirement that the type extend Element</summary>
    static member makeRef<'T>() = ref (Unchecked.defaultof<'T>)

    //This function has a LOT of potential overloads, especially since F# doesn't have variadic tuples,
    //so I'm just defining them as I use them.
    /// Used to explicitly define dependencies for effects and other computations
    [<ImportMember("solid-js")>]
    static member on<'T>(dep : unit -> 'T, fn : 'T -> 'T -> unit, ?options : {|defer : bool|}) : unit -> unit = jsNative

    [<ImportMember("solid-js")>]
    static member createComputed(fn : unit -> unit) : unit = jsNative

    [<ImportMember("solid-js")>]
    static member createComputed<'T>(fn : 'T option -> 'T option) : unit = jsNative

    [<ImportMember("solid-js")>]
    static member createComputed<'T>(fn : 'T -> 'T, initVal : 'T) : unit = jsNative
