module UltimatelySafe.ClientUtils.JsTypes

open System.Collections
open System.Collections.Generic

open Fable.Core
open Fable.Core.JsInterop

[<Emit("undefined")>]
let undefined : obj = jsNative

//My instinct would be to have MutableRecord extend Record,
//but I'm not sure I can make that work in F#? I'd still need the setter for initialization
//and I don't think you can define a private member and then make it public in a subclass...
//So copy-paste it is!

/// <summary>
///     Binding for a JavaScript "Record" object (an object used as a dictionary).
///     Does not attempt to validate that the given key type can be used as an object property.
/// </summary>
/// <param name="init">A <c>seq</c> of tuples used to initialize the collection</param>
type Record<'K, 'V>(init : ('K * 'V) seq, ?initDynamic : ('K * (unit -> 'V)) seq) as this =
    do
        for (k, v) in init do this[k] <- v

        match initDynamic with
        | Some pairs -> for (k, setter) in pairs do this.SetDynamic k setter
        | None -> ()

    /// Returns true if the given key is defined as a property of the Record
    [<Emit("$0.hasOwnProperty($1)")>] //Not using Object.hasOwn so as to avoid needing a polyfill
    member _.Has (key : 'K) : bool = jsNative

    /// Attempts to access the given key in the Record. Returns an option.
    member this.Get (key : 'K) : 'V option =
        //Can't just use [<Emit("$0[$1]")>] here, since we might have a Record of options
        //and the options will get assigned as unwrapped nulls
        if this.Has key then Some (this?(key)) else None

    /// Assigns a key/value pair to the Record
    //Using Object.defineProperty to avoid conflicting with SetDynamic
    [<Emit("Object.defineProperty($0, $1, {configurable: true, enumerable: true, writable: true, value: $2})")>]
    member private _.Set (key : 'K) (value : 'V) : unit = jsNative

    /// Assigns a dynamic property whose value is computed on each access
    [<Emit("Object.defineProperty($0, $1, {configurable: true, enumerable: true, get: $2})")>]
    member private _.SetDynamic (key : 'K) (getter : unit -> 'V) : unit = jsNative

    /// Allows indexing into the Record. Attempting to retrieve a non-existent key will raise an exception.
    member this.Item
        with get (key : 'K) =
            this.Get key
            |> Option.defaultWith (fun () -> raise <| KeyNotFoundException($"Key %A{key} not found in Record"))
        and private set (key : 'K) (value : 'V) = this.Set key value

    [<Emit("Object.entries($0)")>]
    member _.Entries : ('K * 'V)[] = jsNative

    [<Emit("Object.keys($0)")>]
    member _.Keys : 'K[] = jsNative

    [<Emit("Object.values($0)")>]
    member _.Values : 'V[] = jsNative

    interface ('K * 'V) seq with
        override this.GetEnumerator() : IEnumerator = this.Entries.GetEnumerator()
        override this.GetEnumerator() : IEnumerator<'K * 'V> = (this.Entries :> ('K * 'V) seq).GetEnumerator()

/// <summary>
///     Binding for a mutable JavaScript "Record" object (an object used as a dictionary).
///     Does not attempt to validate that the given key type can be used as an object property.
/// </summary>
/// <param name="init">A <c>seq</c> of tuples used to initialize the collection</param>
type MutableRecord<'K, 'V>(init : ('K * 'V) seq, ?initDynamic : ('K * (unit -> 'V)) seq) as this =
    do
        for (k, v) in init do this[k] <- v

        match initDynamic with
        | Some pairs -> for (k, setter) in pairs do this.SetDynamic k setter
        | None -> ()

    /// Returns true if the given key is defined as a property of the Record
    [<Emit("$0.hasOwnProperty($1)")>] //Not using Object.hasOwn so as to avoid needing a polyfill
    member _.Has (key : 'K) : bool = jsNative

    /// Attempts to access the given key in the Record. Returns an option.
    member this.Get (key : 'K) : 'V option =
        //Can't just use [<Emit("$0[$1]")>] here, since we might have a Record of options
        //and the options will get assigned as unwrapped nulls
        if this.Has key then Some (this?(key)) else None

    /// Assigns a key/value pair to the Record
    //Using Object.defineProperty to avoid conflicting with SetDynamic
    [<Emit("Object.defineProperty($0, $1, {configurable: true, enumerable: true, writable: true, value: $2})")>]
    member _.Set (key : 'K) (value : 'V) : unit = jsNative

    /// Assigns a dynamic property whose value is computed on each access
    [<Emit("Object.defineProperty($0, $1, {configurable: true, enumerable: true, get: $2})")>]
    member _.SetDynamic (key : 'K) (getter : unit -> 'V) : unit = jsNative

    /// Allows indexing into the Record. Attempting to retrieve a non-existent key will raise an exception.
    member this.Item
        with get (key : 'K) =
            this.Get key
            |> Option.defaultWith (fun () -> raise <| KeyNotFoundException($"Key %A{key} not found in Record"))
        and set (key : 'K) (value : 'V) = this.Set key value

    [<Emit("delete $0[$1]")>]
    member _.Delete (key : 'K) : unit = jsNative

    [<Emit("Object.entries($0)")>]
    member _.Entries : ('K * 'V)[] = jsNative

    [<Emit("Object.keys($0)")>]
    member _.Keys : 'K[] = jsNative

    [<Emit("Object.values($0)")>]
    member _.Values : 'V[] = jsNative

    interface ('K * 'V) seq with
        override this.GetEnumerator() : IEnumerator = this.Entries.GetEnumerator()
        override this.GetEnumerator() : IEnumerator<'K * 'V> = (this.Entries :> ('K * 'V) seq).GetEnumerator()
