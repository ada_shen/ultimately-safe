//------------------------------------------------------------------------------
//        This code was generated by fable-css-modules.
//        Changes to this file will be lost when the code is regenerated.
//------------------------------------------------------------------------------

[<RequireQualifiedAccess>]
module internal CssModules

open Fable.Core

type Home =

    /// <summary>
    /// Binding for <c>home-page</c> class
    /// </summary>
    [<Emit("$0[\"home-page\"]")>]
    abstract homePage : string

type JoinRoom =

    /// <summary>
    /// Binding for <c>join-room</c> class
    /// </summary>
    [<Emit("$0[\"join-room\"]")>]
    abstract joinRoom : string

module Board =

    type Base =

        /// <summary>
        /// Binding for <c>board</c> class
        /// </summary>
        [<Emit("$0[\"board\"]")>]
        abstract board : string

        /// <summary>
        /// Binding for <c>movable-space</c> class
        /// </summary>
        [<Emit("$0[\"movable-space\"]")>]
        abstract movableSpace : string

        /// <summary>
        /// Binding for <c>moving</c> class
        /// </summary>
        [<Emit("$0[\"moving\"]")>]
        abstract moving : string

        /// <summary>
        /// Binding for <c>selecting</c> class
        /// </summary>
        [<Emit("$0[\"selecting\"]")>]
        abstract selecting : string

        /// <summary>
        /// Binding for <c>active-space</c> class
        /// </summary>
        [<Emit("$0[\"active-space\"]")>]
        abstract activeSpace : string

        /// <summary>
        /// Binding for <c>show-focus</c> class
        /// </summary>
        [<Emit("$0[\"show-focus\"]")>]
        abstract showFocus : string

        /// <summary>
        /// Binding for <c>focused-space</c> class
        /// </summary>
        [<Emit("$0[\"focused-space\"]")>]
        abstract focusedSpace : string

    type Maxima =

        /// <summary>
        /// Binding for <c>maxima-board</c> class
        /// </summary>
        [<Emit("$0[\"maxima-board\"]")>]
        abstract maximaBoard : string

        /// <summary>
        /// Binding for <c>no-border</c> class
        /// </summary>
        [<Emit("$0[\"no-border\"]")>]
        abstract noBorder : string

        /// <summary>
        /// Binding for <c>row</c> class
        /// </summary>
        [<Emit("$0[\"row\"]")>]
        abstract row : string

        /// <summary>
        /// Binding for <c>border-space</c> class
        /// </summary>
        [<Emit("$0[\"border-space\"]")>]
        abstract borderSpace : string

        /// <summary>
        /// Binding for <c>label</c> class
        /// </summary>
        [<Emit("$0[\"label\"]")>]
        abstract label : string

        /// <summary>
        /// Binding for <c>goal-row</c> class
        /// </summary>
        [<Emit("$0[\"goal-row\"]")>]
        abstract goalRow : string

        /// <summary>
        /// Binding for <c>immobilized-icon</c> class
        /// </summary>
        [<Emit("$0[\"immobilized-icon\"]")>]
        abstract immobilizedIcon : string

        /// <summary>
        /// Binding for <c>self-capture-button</c> class
        /// </summary>
        [<Emit("$0[\"self-capture-button\"]")>]
        abstract selfCaptureButton : string

    type Piece =

        /// <summary>
        /// Binding for <c>piece</c> class
        /// </summary>
        [<Emit("$0[\"piece\"]")>]
        abstract piece : string

        /// <summary>
        /// Binding for <c>white</c> class
        /// </summary>
        [<Emit("$0[\"white\"]")>]
        abstract white : string

        /// <summary>
        /// Binding for <c>black</c> class
        /// </summary>
        [<Emit("$0[\"black\"]")>]
        abstract black : string

        /// <summary>
        /// Binding for <c>draggable</c> class
        /// </summary>
        [<Emit("$0[\"draggable\"]")>]
        abstract draggable : string

        /// <summary>
        /// Binding for <c>graphic</c> class
        /// </summary>
        [<Emit("$0[\"graphic\"]")>]
        abstract graphic : string

    type Rococo =

        /// <summary>
        /// Binding for <c>rococo-board</c> class
        /// </summary>
        [<Emit("$0[\"rococo-board\"]")>]
        abstract rococoBoard : string

        /// <summary>
        /// Binding for <c>no-border</c> class
        /// </summary>
        [<Emit("$0[\"no-border\"]")>]
        abstract noBorder : string

        /// <summary>
        /// Binding for <c>row</c> class
        /// </summary>
        [<Emit("$0[\"row\"]")>]
        abstract row : string

        /// <summary>
        /// Binding for <c>border-space</c> class
        /// </summary>
        [<Emit("$0[\"border-space\"]")>]
        abstract borderSpace : string

        /// <summary>
        /// Binding for <c>label</c> class
        /// </summary>
        [<Emit("$0[\"label\"]")>]
        abstract label : string

        /// <summary>
        /// Binding for <c>space-icon</c> class
        /// </summary>
        [<Emit("$0[\"space-icon\"]")>]
        abstract spaceIcon : string

        /// <summary>
        /// Binding for <c>immobilized-icon</c> class
        /// </summary>
        [<Emit("$0[\"immobilized-icon\"]")>]
        abstract immobilizedIcon : string

        /// <summary>
        /// Binding for <c>space-button</c> class
        /// </summary>
        [<Emit("$0[\"space-button\"]")>]
        abstract spaceButton : string

        /// <summary>
        /// Binding for <c>mutual-capture-button</c> class
        /// </summary>
        [<Emit("$0[\"mutual-capture-button\"]")>]
        abstract mutualCaptureButton : string

        /// <summary>
        /// Binding for <c>self-capture-button</c> class
        /// </summary>
        [<Emit("$0[\"self-capture-button\"]")>]
        abstract selfCaptureButton : string

    type Space =

        /// <summary>
        /// Binding for <c>space</c> class
        /// </summary>
        [<Emit("$0[\"space\"]")>]
        abstract space : string

    type Ultima =

        /// <summary>
        /// Binding for <c>ultima-board</c> class
        /// </summary>
        [<Emit("$0[\"ultima-board\"]")>]
        abstract ultimaBoard : string

        /// <summary>
        /// Binding for <c>no-border</c> class
        /// </summary>
        [<Emit("$0[\"no-border\"]")>]
        abstract noBorder : string

        /// <summary>
        /// Binding for <c>row</c> class
        /// </summary>
        [<Emit("$0[\"row\"]")>]
        abstract row : string

        /// <summary>
        /// Binding for <c>border-space</c> class
        /// </summary>
        [<Emit("$0[\"border-space\"]")>]
        abstract borderSpace : string

        /// <summary>
        /// Binding for <c>label</c> class
        /// </summary>
        [<Emit("$0[\"label\"]")>]
        abstract label : string

        /// <summary>
        /// Binding for <c>immobilized-icon</c> class
        /// </summary>
        [<Emit("$0[\"immobilized-icon\"]")>]
        abstract immobilizedIcon : string

        /// <summary>
        /// Binding for <c>self-capture-button</c> class
        /// </summary>
        [<Emit("$0[\"self-capture-button\"]")>]
        abstract selfCaptureButton : string

module Game =

    type Base =

        /// <summary>
        /// Binding for <c>game-base</c> class
        /// </summary>
        [<Emit("$0[\"game-base\"]")>]
        abstract gameBase : string

        /// <summary>
        /// Binding for <c>toolbar</c> class
        /// </summary>
        [<Emit("$0[\"toolbar\"]")>]
        abstract toolbar : string

        /// <summary>
        /// Binding for <c>turn</c> class
        /// </summary>
        [<Emit("$0[\"turn\"]")>]
        abstract turn : string

        /// <summary>
        /// Binding for <c>white</c> class
        /// </summary>
        [<Emit("$0[\"white\"]")>]
        abstract white : string

        /// <summary>
        /// Binding for <c>black</c> class
        /// </summary>
        [<Emit("$0[\"black\"]")>]
        abstract black : string

        /// <summary>
        /// Binding for <c>sidebar</c> class
        /// </summary>
        [<Emit("$0[\"sidebar\"]")>]
        abstract sidebar : string

        /// <summary>
        /// Binding for <c>piece-info-container</c> class
        /// </summary>
        [<Emit("$0[\"piece-info-container\"]")>]
        abstract pieceInfoContainer : string

        /// <summary>
        /// Binding for <c>captures-container</c> class
        /// </summary>
        [<Emit("$0[\"captures-container\"]")>]
        abstract capturesContainer : string

        /// <summary>
        /// Binding for <c>top</c> class
        /// </summary>
        [<Emit("$0[\"top\"]")>]
        abstract top : string

        /// <summary>
        /// Binding for <c>bottom</c> class
        /// </summary>
        [<Emit("$0[\"bottom\"]")>]
        abstract bottom : string

        /// <summary>
        /// Binding for <c>board</c> class
        /// </summary>
        [<Emit("$0[\"board\"]")>]
        abstract board : string

    type CapturedPieceList =

        /// <summary>
        /// Binding for <c>captured-piece</c> class
        /// </summary>
        [<Emit("$0[\"captured-piece\"]")>]
        abstract capturedPiece : string

        /// <summary>
        /// Binding for <c>button-mode</c> class
        /// </summary>
        [<Emit("$0[\"button-mode\"]")>]
        abstract buttonMode : string

        /// <summary>
        /// Binding for <c>captured-piece-content</c> class
        /// </summary>
        [<Emit("$0[\"captured-piece-content\"]")>]
        abstract capturedPieceContent : string

        /// <summary>
        /// Binding for <c>count</c> class
        /// </summary>
        [<Emit("$0[\"count\"]")>]
        abstract count : string

        /// <summary>
        /// Binding for <c>single</c> class
        /// </summary>
        [<Emit("$0[\"single\"]")>]
        abstract single : string

        /// <summary>
        /// Binding for <c>multiple</c> class
        /// </summary>
        [<Emit("$0[\"multiple\"]")>]
        abstract multiple : string

        /// <summary>
        /// Binding for <c>cancel-button</c> class
        /// </summary>
        [<Emit("$0[\"cancel-button\"]")>]
        abstract cancelButton : string

        /// <summary>
        /// Binding for <c>captured-piece-list</c> class
        /// </summary>
        [<Emit("$0[\"captured-piece-list\"]")>]
        abstract capturedPieceList : string

        /// <summary>
        /// Binding for <c>select-mode</c> class
        /// </summary>
        [<Emit("$0[\"select-mode\"]")>]
        abstract selectMode : string

    type Maxima =

        /// <summary>
        /// Binding for <c>maxima-game</c> class
        /// </summary>
        [<Emit("$0[\"maxima-game\"]")>]
        abstract maximaGame : string

    type PieceInfo =

        /// <summary>
        /// Binding for <c>piece-info</c> class
        /// </summary>
        [<Emit("$0[\"piece-info\"]")>]
        abstract pieceInfo : string

        /// <summary>
        /// Binding for <c>display</c> class
        /// </summary>
        [<Emit("$0[\"display\"]")>]
        abstract display : string

