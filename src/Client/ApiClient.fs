module UltimatelySafe.ApiClient

open Fable.Remoting.Client

open UltimatelySafe.Shared.Api

let apiClient =
    Remoting.createApi ()
    |> Remoting.withRouteBuilder routeBuilder
    |> Remoting.buildProxy<Api>
