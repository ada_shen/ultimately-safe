module UltimatelySafe.OnlineRoom

open Fable.Core
open Feliz.JSX.Solid

open UltimatelySafe.ApiClient
open UltimatelySafe.Components.Ext.SolidRouter

[<StringEnum>]
type OnlineRoomRouteParams = RoomName

[<AutoOpen>]
type OnlineRoom =
    [<JSX.Component>]
    static member OnlineRoom() =
        let routeParams = Solid.useParams<OnlineRoomRouteParams>()
        let roomName = routeParams[RoomName]

        Solid.onCleanup(fun () ->
            apiClient.leaveRoom roomName |> Async.Ignore |> Async.Start
        )

        Html.main [
            Html.children [
                Html.h1 roomName
                Solid.A(href = "/", children = Html.text "Home")
            ]
        ]
