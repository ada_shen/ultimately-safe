module UltimatelySafe.Server

open Fable.Remoting.Server
open Fable.Remoting.Giraffe
open Saturn

open UltimatelySafe.Shared.Api

//TODO: Choose an actual storage backend
module Storage =
    let mutable private rooms = Map.empty<string, int>

    let getOccupants (roomName : string) = rooms |> Map.tryFind roomName |> Option.defaultValue 0

    let joinRoom (roomName : string) =
        let occupants = getOccupants roomName
        if occupants < 2 then
            let newOccupants = occupants + 1
            rooms <- rooms |> Map.add roomName newOccupants
            Ok newOccupants
        else
            Error "Room is full"

    let leaveRoom (roomName : string) =
        let occupants = getOccupants roomName
        if occupants <= 0 then
            Error "Room is empty"
        else
            let newOccupants = occupants - 1
            rooms <- rooms |> Map.add roomName newOccupants
            Ok newOccupants

let apiImpl = {
    joinRoom = fun (roomName : string) -> async { return Storage.joinRoom roomName }
    leaveRoom = fun (roomName : string) -> async { return Storage.leaveRoom roomName }
}

let router =
    Remoting.createApi ()
    |> Remoting.withRouteBuilder routeBuilder
    |> Remoting.fromValue apiImpl
    |> Remoting.buildHttpHandler

let app = application {
    url "http://*:8085"
    use_router router
    memory_cache
    use_static "public"
    use_gzip
}

[<EntryPoint>]
let main (_ : string[]) =
    run app
    0
