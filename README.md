# ultimately-safe

An implementation of the chess-like game [Ultima](https://www.chessvariants.com/other.dir/ultima.html)
and the related games [Maxima](https://www.chessvariants.com/dpieces.dir/maxima/maxima.html)
and [Rococo](https://www.chessvariants.com/other.dir/rococo.html).

Written in full-stack F# using the [SAFE Stack](https://safe-stack.github.io/) and [SolidJS](https://www.solidjs.com/).
[SCSS](https://sass-lang.com/) [Modules](https://github.com/css-modules/css-modules) are used for styles.
Database/persistence layer (if any) yet to be decided.

Plan is to implement all three games entirely client-side/local and then start writing the backend.

Current status: All three client-side games are complete. Backend work pending.

Yes the project name is bad/cheesy but it's the best thing I could come up with. No I am not ashamed.

## Install pre-requisites

You'll need to install the following pre-requisites in order to build SAFE applications

* [.NET Core SDK](https://www.microsoft.com/net/download) 8.0 or higher
* [Node 21](https://nodejs.org/en/download/)

## Starting the application

Before you run the project **for the first time only** you must install dotnet "local tools" with this command:

```bash
dotnet tool restore
```

Depending on your editor you'll likely also need to run:

```sh
dotnet paket restore
npm install
```

To concurrently run the server and the client components in watch mode use the following command:

```bash
dotnet run
```

Then open `http://localhost:8080` in your browser.

The build project in root directory contains a couple of different build targets. You can specify them after `--` (target name is case-insensitive).

To run concurrently server and client tests in watch mode (you can run this command in parallel to the previous one in new terminal):

```bash
dotnet run -- RunTests
```

Client tests are available under `http://localhost:8081` in your browser and server tests are running in watch mode in console.

When editing SCSS Module files, you can use the `fcm` tool to automatically generate/update F# types for your class names:

```sh
dotnet run FcmWatch
```

Finally, there are `Bundle` and `Azure` targets that you can use to package your app and deploy to Azure, respectively:

```bash
dotnet run -- Bundle
dotnet run -- Azure
```

## SAFE Stack Documentation

If you want to know more about the full Azure Stack and all of it's components (including Azure) visit the official [SAFE documentation](https://safe-stack.github.io/docs/).

You will find more documentation about the used F# components at the following places:

* [Saturn](https://saturnframework.org/)
* [Fable](https://fable.io/docs/)
* [Elmish](https://elmish.github.io/elmish/)
