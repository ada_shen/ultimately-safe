module Helpers

open System

open Fake.Core

let initializeContext () =
    let execContext = Context.FakeExecutionContext.Create false "build.fsx" []
    Context.setExecutionContext (Context.RuntimeContext.Fake execContext)

module private Proc =
    module Parallel =
        let private locker = obj()

        let private colors = [|
           ConsoleColor.Blue
           ConsoleColor.Yellow
           ConsoleColor.Magenta
           ConsoleColor.Cyan
           ConsoleColor.DarkBlue
           ConsoleColor.DarkYellow
           ConsoleColor.DarkMagenta
           ConsoleColor.DarkCyan
        |]

        let private print (color : ConsoleColor) (colored : string) (line : string) =
            lock locker (fun () ->
                let currentColor = Console.ForegroundColor
                Console.ForegroundColor <- color
                Console.Write colored
                Console.ForegroundColor <- currentColor
                Console.WriteLine line
            )

        let private onStdout (index : int) (name : string) (line : string) =
            let color = colors[index % colors.Length]
            if isNull line then
                print color $"{name}: --- END ---" ""
            elif String.isNotNullOrEmpty line then
                print color $"{name}: " line

        let private onStderr (name : string) (line : string) =
            let color = ConsoleColor.Red
            if not (isNull line) then
                print color $"{name}: " line

        let private redirect (index : int, (name : string, createProcess : CreateProcess<'T>)) =
            createProcess
            |> CreateProcess.redirectOutputIfNotRedirected
            |> CreateProcess.withOutputEvents (onStdout index name) (onStderr name)

        let private printStarting (indexed : (int * (string * CreateProcess<'T>)) seq) =
            for (index, (name, c)) in indexed do
                let color = colors[index % colors.Length]
                let wd = c.WorkingDirectory |> Option.defaultValue ""
                let exe = c.Command.Executable
                let args = c.Command.Arguments.ToStartInfo
                print color $"{name}: {wd}> {exe} {args}" ""

        let run (cs : (string * CreateProcess<'T>) seq) =
            cs
            |> Seq.toArray
            |> Array.indexed
            |> fun x -> printStarting x; x
            |> Array.map redirect
            |> Array.Parallel.map Proc.run

let createProcess (exe : FilePath) (arg : string) (dir : string) =
    CreateProcess.fromRawCommandLine exe arg
    |> CreateProcess.withWorkingDirectory dir
    |> CreateProcess.ensureExitCode

let dotnet = createProcess "dotnet"
let npm =
    let npmPath =
        match ProcessUtils.tryFindFileOnPath "npm" with
        | Some path -> path
        | None ->
            failwith "\
                npm was not found in path. Please install it and make sure it's available from your path. \
                See https://safe-stack.github.io/docs/quickstart/#install-pre-requisites for more info.\
            "

    createProcess npmPath

let run (proc : string -> string -> CreateProcess<'T>) (arg : string) (dir : string) =
    proc arg dir
    |> Proc.run
    |> ignore

let runParallel (processes : (string * CreateProcess<'T>) seq) =
    processes
    |> Proc.Parallel.run
    |> ignore

let runOrDefault (args : string[]) =
    try
        match args with
        | [|target|] -> Target.runOrDefault target
        | _ -> Target.runOrDefault "Run"
        0
    with e ->
        printfn "%A" e
        1
