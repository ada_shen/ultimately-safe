import {defineConfig} from "vite";
import solidPlugin from "vite-plugin-solid";
import solidSvgPlugin from "vite-plugin-solid-svg";

export default defineConfig({
    //Path to directory containing index HTML file
    root: "./src/Client",

    //Set to false to prevent Vite from clearing the terminal screen when logging certain messages
    clearScreen: false,

    build: {
        outDir: "../../deploy/public",

        assetsDir: "static",

        //Clean before building
        emptyOutDir: true,

        //The file size in bytes at which to use a separate file instead of URI inlining when importing assets
        assetsInlineLimit: 10_000,

        //Use inline sourcemaps in development, and separate files in production
        sourcemap: process.env.NODE_ENV === "development" ? "inline" : true
    },

    optimizeDeps: {
        //For whatever reason, solid-dnd breaks without "optimizeDeps" being applied to it.
        //See: https://github.com/thisbeyond/solid-dnd/issues/63
        //However, the `extensions` option shown in that issue breaks the SolidJS Router,
        //so use a narrower "include" instead (this seems to work).
        include: ["solid-dnd"]
    },

    css: {
        modules: {
            generateScopedName: process.env.NODE_ENV === "development" ?
                "[name]__[local]--[hash:base64:6]" : //In development mode, include the class names in the output
                "[hash:base64]"
        }
    },

    server: {
        port: 8080,

        //Disable HMR because it's spooky magic and I do not trust it
        hmr: false,

        proxy: {
            "/api": {
                target: `http://localhost:${process.env.SERVER_PROXY_PORT || "8085"}`,
                changeOrigin: true
            },

            "/socket": {
                target: `http://localhost:${process.env.SERVER_PROXY_PORT || "8085"}`,
                ws: true
            }
        }
    },

    plugins: [
        {
            name: "scoped-spa-rewriting",
            apply: "serve",
            configureServer(server) {
                //NOTE: I generally prefer to keep my SPA routes namespaced to /app/.
                //This way there is no ambiguity between the SPA and static assets.
                //However, Vite's dev server aggressively does SPA rewriting for all 404s.
                //I cannot find a way to disable it that does not break everything.
                //You're supposed to be able to set appType: "mpa", but that doesn't remove the behavior.
                //This appears to be a bug, but nevertheless I have given up.
                //I am continuing to *pretend* that's how it's set up and have still added a redirect below.
                //That's how it would be set up in production.
                //However, for now, all 404s will just serve the SPA instead in local dev.
                //I'll revisit this in the future if MPA mode is fixed.

                //Redirect / to /app
                server.middlewares.use((req, res, next) => {
                    if (req.url === "/") {
                        res.writeHead(301, {Location: "/app"});
                    }

                    next();
                });
            }
        },

        solidPlugin({
            hot: false
        }),

        //Enable importing SVG files as JSX components.
        solidSvgPlugin({
            //You'll get a JSX component by default, and a URL with `?url`.
            defaultAsComponent: true,

            svgo: {
                enabled: true,
                svgoConfig: {
                    plugins: [
                        {
                            name: "preset-default",
                            params: {
                                overrides: {
                                    removeViewBox: false
                                }
                            }
                        },
                        "removeDimensions"
                    ]
                }
            }
        })
    ]
});
