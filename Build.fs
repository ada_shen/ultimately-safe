open Fake.Core
open Fake.Core.TargetOperators
open Fake.IO

open Helpers

initializeContext ()

let sharedPath = Path.getFullName "src/Shared"
let serverPath = Path.getFullName "src/Server"
let clientPath = Path.getFullName "src/Client"
let deployPath = Path.getFullName "deploy"
let sharedTestsPath = Path.getFullName "tests/Shared"
let serverTestsPath = Path.getFullName "tests/Server"
let clientTestsPath = Path.getFullName "tests/Client"

let fableOptions = "-o output -e .jsx -s"

Target.create "Clean" (fun _ ->
    Shell.cleanDir deployPath
    run dotnet "fable clean --yes" clientPath //Delete *.fs.js files created by Fable
)

// TODO: Remove --legacy-peer-deps when no longer needed to use Vite 5
Target.create "InstallClient" (fun _ -> run npm "ci --legacy-peer-deps" ".")

Target.create "FcmWatch" (fun _ -> run npm "run fcm-watch" clientPath)

Target.create "Fable" (fun _ -> run dotnet $"fable {fableOptions}" clientPath)

Target.create "Bundle" (fun _ ->
    run npm "run fcm" clientPath
    runParallel [
        "server", dotnet $"publish -c Release -o '{deployPath}'" serverPath
        "client", dotnet $"fable {fableOptions} --run npm run build" clientPath
    ]
)

// Target.create "Azure" (fun _ ->
//     let web = webApp {
//         name "ultimately_safe"
//         zip_deploy "deploy"
//     }
//
//     let deployment = arm {
//         location Location.WestUS
//         add_resource web
//     }
//
//     deployment
//     |> Deploy.execute "ultimately_safe" Deploy.NoParameters
//     |> ignore
// )

Target.create "Run" (fun _ ->
    run dotnet "build" sharedPath
    runParallel [
        "server", dotnet "watch run" serverPath
        "client", dotnet $"fable watch {fableOptions} --run npm run start" clientPath
    ]
)

Target.create "RunTests" (fun _ ->
    run dotnet "build" sharedTestsPath
    runParallel [
        "server", dotnet "watch run" serverTestsPath
        "client", dotnet $"fable watch {fableOptions} --run npm run test:live" clientTestsPath
    ]
)

let dependencies = [
    "Clean"
        ==> "InstallClient"
        ==> "Bundle"
        // ==> "Azure"

    "Clean"
        ==> "InstallClient"
        ==> "Run"

    "InstallClient"
        ==> "RunTests"
]

[<EntryPoint>]
let main (args : string[]) = runOrDefault args
